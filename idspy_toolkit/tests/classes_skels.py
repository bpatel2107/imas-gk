from dataclasses import dataclass, field
from typing import Optional
import copy
from numpy import array, ndarray
import numpy.typing as npt


def default_field(obj):
    return field(default_factory=lambda: copy.deepcopy(obj))


class ClassOldStyle:
    time: str = "12345"


@dataclass#(slots=True)
class ClassEmpty:
    time: Optional[str] = None


@dataclass#(slots=True)
class Class1:
    time: str = "1.234"


@dataclass#(slots=True)
class Class3:
    time: float = 1.234
    space: int = 567
    name: str = "azerty"


@dataclass#(slots=True)
class ClassList:
    time: list[str] = field(default_factory=lambda: ["1.234", "5.678910", "abcdefghijk"])


@dataclass#(slots=True)
class ClassList2:
    time: list[str] = field(default_factory=lambda: [1.234, 5.678])


@dataclass#(slots=True)
class ClassListMix:
    time: list[str] = field(default_factory=lambda: [1.234, 5.678])
    space: int = 567
    name: str = "azerty"


@dataclass#(slots=True)
class ClassListNightmare:
    time: list[str] = field(default_factory=lambda: ["1.234", "5.678"])
    array_value: npt.NDArray = field(default_factory=lambda: array([[1, 2, 4], [1, 2, 3.5]]))
    list_array: list[npt.NDArray] = field(default_factory=lambda: [array([1, 2, 4]), array([1, 2, 3.5]), ])
    list_list: list[list[float]] = field(default_factory=lambda: [[1, 2], [3, 4]])
    list_list_list: list[list[list[float]]] = field(
        default_factory=lambda: [[[1, 2], [3, 4]], [[1, 2], [3, 4]], [[1, 2], [3, 4]], ])
    array_array: npt.NDArray = field(default_factory=lambda: array([array([1, 2, 3]), array([1, 2, 3.5]), ]))


@dataclass#(slots=True)
class ClassListMixDict:
    time: list[str] = default_field(["1.234", "5.678"])
    space: int = 567
    name: str = "azerty"
    jsonarg: dict = field(default_factory=lambda: {"a": 1, "b": 2.3, "c": "azertyuiop"})


@dataclass#(slots=True)
class ClassListMixDict2:
    time: list[str] = field(default_factory=lambda: ["1.234", "5.678"])
    space: int = 567
    name: str = "azerty"
    jsonarg: list[dict] = default_field([{"a": 1, "b": 2.3, "c": "azertyuiop"},
                                         {"a": 2, "b": 3.4, "c1": "azertyuiope"}])


@dataclass#(slots=True)
class ClassListMixDictIn:
    time1: list[str] = field(default_factory=lambda: (1.2345, 5.6789))
    space1: int = 5678
    name1: str = "azERty"
    jsonarg1: list[dict] = field(default_factory=lambda: [{"a": 11, "b": 22.33, "c": "azertyuiopQS"},
                                                          {"a": 22, "b": 33.44, "c1": "azertyuiopeQSD"}])


@dataclass#(slots=True, )
class ClassListNested:
    time: list[str] = field(default_factory=lambda: [1.234, 5.678])
    space: int = 567
    name: str = "azerty"
    jsonarg: list[dict] = field(default_factory=lambda: [{"a": 1, "b": 2.3, "c": "azertyuiop"},
                                                         {"a": 2, "b": 3.4, "c1": "azertyuiope"}])
    nestedclass: ClassListMixDictIn = field(default_factory=ClassListMixDictIn)


@dataclass#(slots=True)
class ClassListNestedList:
    time: list[str] = default_field([1.234, 5.678])
    space: int = 567
    name: str = "azerty"
    jsonarg: list[dict] = field(default_factory=lambda: [{"a": 1, "b": 2.3, "c": "azertyuiop"},
                                                         {"a": 2, "b": 3.4, "c1": "azertyuiope"}])
    nestedclass: list[ClassListMixDictIn] = field(
        default_factory=lambda: [ClassListMixDictIn(space1=8888, name1="IMAS"),
                                 ClassListMixDictIn(space1=9999, name1="IDS")]
    )


@dataclass
class subsubclass:
    member_subsubclass_aa: str = field(default=""
                                       )
    member_subsubclass_bb: int = field(default=999999999
                                       )
    member_subsubclass_cc: float = field(default=None
                                         )


@dataclass
class subclass:
    member_subclass: Optional[subsubclass] = field(
        default=None
    )


@dataclass
class BaseClass:
    list_member: list[subclass] = field(
        default_factory=list,
    )


@dataclass
class ArrayClass:
    val_array_0d: float = field(default_factory=lambda: array(42.42))  # array of shape 0
    val_0d: float = 999.999
    val_array_1d: ndarray[int, float] = field(default_factory=lambda: array([]))
