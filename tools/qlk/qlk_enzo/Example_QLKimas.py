#----------------------------------------------------------
# This is a quick tutorial on how to use the QLKimas class
#----------------------------------------------------------

import QLKimas as qlkimas
import QLKrun as qlkrun

qlkpath = '~/path/to/QuaLiKiz'
runpath = '~/path/to/your/runs/folder'
qlkpath = '~/Documents/These/QuaLiKiz'
runpath = qlkpath + '/runs'

# Create an instance of the QLKrun class, run the simulation, and store both inputs and outputs
qlk_instance = qlkrun.QLKrun()
qlk_instance.set_storage(qlkpath = qlkpath, runpath = runpath, runname = 'my_run_qlk')
qlk_instance.input['kthetarhos'] = 0.5
qlk_instance.write_input()
qlk_instance.run()
qlk_instance.read()

# Convert QLKrun object to an IDS object
IDS_qlk = qlkimas.qlk2ids(qlk_instance)

# Print an example output from the IDS object
print("Growth rate norm: ", IDS_qlk.linear.wavevector[-1].eigenmode[0].growth_rate_norm)

# Convert IDS object back to QLKrun object
test = qlkimas.ids2qlk(IDS_qlk)

# Set the storage path for the modified QLKrun object
test.set_storage(qlkpath = qlkpath, runpath = runpath, runname = 'my_run_qlk_from_ids')

# Write the modified input parameters to the input file
test.write_input()

# Run QLK using the modified input file
test.run()