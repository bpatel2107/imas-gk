'''
Provide an example of gk ids creation with outputs from QuaLiKiz and the reopening of this ids with the qlk class
'''

from QLKclass import QLK_class


qlk_path = '~/Documents/These/QuaLiKiz' 
run_path = '~/Documents/These/QuaLiKiz/runs'
run_name = 'my_test_run'
provider = 'Enzo'

qlk_object = QLK_class()
qlk_object.run_QLK(run_name, run_path, qlk_path)
qlk_object.load_runfolder(run_path + '/' + run_name)
IDS_QLK = qlk_object.to_IMAS_ids(provider)


run_name_2 = 'my_test_run_from_IDS'
qlk_object_2 = QLK_class()
qlk_object_2.load_IMAS_ids(ids = IDS_QLK)
qlk_object_2.run_QLK(run_name_2, run_path, qlk_path)
qlk_object_2.load_runfolder(run_path + '/' + run_name_2)
IDS_QLK_2 = qlk_object_2.to_IMAS_ids(provider)

print("growth rate from first IDS : ", IDS_QLK.linear.wavevector[-1].eigenmode[0].growth_rate_norm)
print("growth rate from second IDS : ", IDS_QLK_2.linear.wavevector[-1].eigenmode[0].growth_rate_norm)