import scipy.constants
import numpy as np
import inspect
from matplotlib import pyplot as plt
import pandas as pd
from idspy_dictionaries import ids_gyrokinetics_local as gkids
import idspy_toolkit as idspy
import datetime
import os

import shutil
import struct

from pathlib import Path
from os.path import relpath




np.seterr(all="raise",)

class constantsIMAS:
    """
    Initialise an IMAS constants class from scipy.constants for use in the conversion functions

    :param ds: optional parameter required for conversion routine. If not present only the default values from scipy will be returned.
    """
    qref = np.float64(scipy.constants.physical_constants['electron volt'][0])  # C
    mref = np.float64(scipy.constants.physical_constants['deuteron mass'][0])  # kg
    me = np.float64(scipy.constants.physical_constants['electron mass'][0])  # kg

    def __init__(self, Ro, Bo, Te, ne, QLK_Tref, QLK_nref):
            # return eV value (given in keV)
            self.Tref = Te * QLK_Tref
            # return m^-3 value (given in 1e19 m^-3)
            self.nref = np.float64(ne) * QLK_nref
            self.Rref = Ro
            self.Bref = Bo
            qTe = self.Tref * self.qref
            self.vth_ref = np.sqrt(np.float64(2) * qTe / self.mref)
            self.rho_ref = self.mref * self.vth_ref / (self.qref * self.Bref)
            self.vth_e = np.sqrt(np.float64(2) * qTe / self.me)
            self.rho_star = self.rho_ref / self.Rref
            self.recip_rho_star = self.Rref / self.rho_ref

    def print_constants(self):

        print('\nIMAS Constants:')
        # members of an object
        for i in inspect.getmembers(self):

            # to remove private and protected
            # functions
            if not i[0].startswith('_'):

                # To remove other methods that
                # doesnot start with a underscore
                if not inspect.ismethod(i[1]):
                    var = i[1]
                    if type(var) == float:
                        print(f'{i[0]}: {var}, {type(var)}')
                    elif var.shape == ():
                        print(f'{i[0]}: {var}, {type(var)}')
                    else:
                        print(f'{i[0]} shape: {var.shape}')


class constantsRATIO():
    """
    Initialise a ratio constants class with QLK:IMAS ratios for use in the conversion functions
    :param QLK: a QLK class
    :param IMAS: an IMAS constants class
    """
    Trat = np.float64(1)

    def __init__(self, QLK, IMAS, baseline=1):
        # baseline !=1 used in cases where the main ion has not yet been identified and the ratios wanted are only using
        # reference quantities and this will be reloaded later
        # self.qrat = QLK.qref / IMAS.qref
        self.qrat = 1
        self.mrat = QLK.mref / IMAS.mref
        self.nrat = QLK.nref / IMAS.nref
        self.Rrat = QLK.Rref / IMAS.Rref
        self.Brat = QLK.Bref / IMAS.Bref
        if baseline == 1:
            self.vth_rat = 1 #QLK.vth_i0 / IMAS.vth_ref
            self.rho_rat = 1 #QLK.rho_i0 / IMAS.rho_ref
            # rho_rat 2 seems more consistent in outputting a single values, avoiding errors due to cancelling
            self.rho_rat2 = 1 # np.sqrt(QLK.mi0 / (2 * IMAS.mref * self.qrat * self.Brat))


    def print_constants(self):

        print('\nConstant Ratios:')
        # members of an object
        for i in inspect.getmembers(self):

            # to remove private and protected
            # functions
            if not i[0].startswith('_'):

                # To remove other methods that
                # doesnot start with a underscore
                if not inspect.ismethod(i[1]):
                    var = i[1]
                    if type(var) == float:
                        print(f'{i[0]}: {var}, {type(var)}')
                    elif var.shape == ():
                        print(f'{i[0]}: {var}, {type(var)}')
                    else:
                        print(f'{i[0]} shape: {var.shape}')


class QLK_class:
    """
    A class containing all the inputs and outputs necessary to run QuaLiKiZ
    """
    def __init__(self):
        # initialise QLK constant values
        self.qref = np.float64(1.602176565e-19)  # C, qlk charge
        self.mref = np.float64(1.672621777e-27)  # kg, qlk proton mass
        self.me = np.float64(9.10938291e-31)  # kg, qlk electron mass
        self.Tref = np.float64(1e3)  # eV
        self.nref = np.float64(1e19)  # m^-3
        self.cref = np.sqrt(self.Tref) * np.float64(self.qref) / np.float64(self.mref)
        self.Rref = 3 # m
        self.Bref = 1 # T
        self.source = 'Initialisation QLK from qlk2ids'

        self.typee = 1 #adiabatic default
        self.rot_flag = 0 # no rotation default
        self.anis = 0 # no temperature anisotropy default
        self.danis = 0 # no temperature anisotropy gradient default

        # By default parameters
        self.dimx = 1  # Number of radial points or scans: set to 1 (no scan)
        self.dimn = 1  # Number of wavenumbers
        self.nions = 1  # Number of ions in the system
        self.phys_meth = 2  # Flag for additional computation (default 0.0)
        self.coll_flag = 0  # Flag for collisionality (default 0.0)
        self.write_primi = 1  # Flag for writing primitive outputs (default 1)
        self.rot_flag = 0  # Flag for rotation (default 0.0)
        self.verbose = 1  # Flag to set the verbosity level of the output
        self.numsols = 3  # Number of solutions requested
        self.relacc1 = 1e-3  # Relative accuracy in 1D integrals
        self.relacc2 = 2e-2  # Relative accuracy in 2D integrals
        self.absacc1 = 0  # Collisionality multiplier (for tests)
        self.absacc2 = 0  # Collisionality multiplier (for tests)
        self.maxruns = 1  # Number of iterations jumping directly to Newton between contour checks
        self.maxpts = 5e5  # Number of integrand evaluations performed in the 2D integral
        self.timeout = 600  # Upper time limit (s) beyond which solutions are not sought for a given wavenumber and radius
        self.ETGmult = 1  # ETG multiplier
        self.collmult = 1  # Collisionality multiplier

        # Geometry input
        self.kthetarhos = 0.2  # Wavenumber spectrum input: Vector (dimn)
        self.x = 0.5 / 3  # Normalized radial coordinate (midplane average)
        self.rho = 0.5 / 3  # Normalized toroidal flux coordinate
        self.rhomin = 0  # Normalized toroidal flux coordinate
        self.rhomax = 1  # Normalized toroidal flux coordinate
        self.Ro = 3  # Major radius. Radial profile due to Shafranov shift
        self.Rmin = 0.5  # Geometric minor radius. Assumed to be an average at the midplane at the LCFS. Currently a profile but probably should be shifted to a scalar
        self.Bo = 1  # Probably not very rigorous to use this sqrt(<B^2>) to calculate Larmor radius; fairly close to <Bphi> in practice though
        # self.R0 = self.R0  # Major geometric radius used for normalizations
        self.q = 2  # Vector (radial grid x(aa))
        self.smag = 1  # Vector (radial grid x(aa)); q is a flux surface quantity --> makes sense to consider s = self.rho / q dq/drho
        self.alpha = 0  # Vector (radial grid x(aa))

        # Rotation input
        self.Machtor = 0  # Vector (radial grid x(aa))
        self.Autor = 0  # Vector (radial grid x(aa))
        self.Machpar = 0  # Vector (radial grid x(aa))
        self.Aupar = 0  # Vector (radial grid x(aa))
        self.gammaE = 0  # Vector (radial grid x(aa))

        # Electron input
        self.Te = 1  # Vector (radial grid x(aa))
        self.ne = 1  # Vector (radial grid x(aa))
        self.Ate = 9  # Vector (radial grid x(aa))
        self.Ane = 3  # Vector (radial grid x(aa))
        self.typee = 1  # Kinetic or adiabatic
        self.anise = 1  # Tperp/Tpar at LFS
        self.danisdre = 0  # d/dr(Tperp/Tpar) at LFS

        # Ion inputs (can be for multiple species)
        self.Ai = [1]  # Ion mass
        self.Zi = [1]  # Ion charge
        self.Ti =  [1]  # Vector (radial grid x(aa))
        self.normni = [1]  # ni/ne Vector (radial grid x(aa))
        self.Ati = [9]  # Vector (radial grid x(aa))
        self.Ani = [3]  # Vector (radial grid x(aa)); Check calculation against Qualikiz's electroneutrality assumption
        self.typei = [1]  # Kinetic, adiabatic, tracer
        self.anisi = [1]  # Tperp/Tpar at LFS
        self.danisdri = [0]  # d/dr(Tperp/Tpar) at LFS
        self.separateflux = 1  # Separate flux output
        self.simple_mpi_only = 0  # Separate flux output
        self.integration_routine = 1  # NAG

        self.commit = 'qlskdfj'
        self.code_version = 'qmlkdjf'



    def load_xr(self, xr_df):
        '''
        loads a QLK class from an entry in an xarray dataset
        :param xr_df: xarray dataframe containing a single QLK run (i.e. dimx = 1).
        '''
        # load all the required variables as float 64 to circumvent the issues with loading as float64 initially and
        # avoid rounding errors especially for back conversion
        if xr_df.dimx.shape != ():
            print('dimx has more than 1 value. The class only supports a single value. Select an individual dimx by using the following: xr_df.isel(dimx=i) for an index i')
            return
        self.source = 'xarray'
        self.Rmin = xr_df.Rmin.values.astype('float64')
        self.x = xr_df.x.values.astype('float64')
        self.Ro = xr_df.Ro.values.astype('float64')
        self.Bo = xr_df.Bo.values.astype('float64')

        self.dimn = len(xr_df.dimn.values)
        self.numsols = len(xr_df.numsols.values)
        self.nions = len(xr_df.nions.values)

        self.q = xr_df.q.values.astype('float64')
        self.smag = xr_df.smag.values.astype('float64')
        self.alpha = xr_df.alpha.values.astype('float64')
        self.Machtor = xr_df.Machtor.values.astype('float64')
        self.Machpar = xr_df.Machpar.values.astype('float64')
        self.Autor = xr_df.Autor.values.astype('float64')
        self.Aupar = xr_df.Aupar.values.astype('float64')
        self.gammaE = xr_df.gammaE.values.astype('float64')
        self.normni = xr_df.normni.values.astype('float64')
        self.Ze = 1
        self.Zi = xr_df.Zi.values.astype('float64')
        self.ne = np.float64(xr_df.ne.values.astype('float64'))
        self.ni = self.normni * self.ne
        self.Te = xr_df.Te.values.astype('float64')
        self.Ti = xr_df.Ti.values.astype('float64')
        self.Ane = xr_df.Ane.values.astype('float64')
        self.Ani = xr_df.Ani.values.astype('float64')
        self.Ate = xr_df.Ate.values.astype('float64')
        self.Ati = xr_df.Ati.values.astype('float64')
        self.Ae = self.me
        self.Ai = xr_df.Ai.values.astype('float64')
        try:
            self.pfe_SI = xr_df.pfe_SI.values.astype('float64')
        except:
            self.pfe_SI = np.zeros(1)
        try:
            self.pfi_SI = xr_df.pfi_SI.values.astype('float64')
        except:
            self.pfi_SI = np.zeros(self.nions)
        try:
            self.efe_SI = xr_df.efe_SI.values.astype('float64')
        except:
            self.efe_SI = np.zeros(1)
        try:
            self.efi_SI = xr_df.efi_SI.values.astype('float64')
        except:
            self.efi_SI = np.zeros(self.nions)
        try:
            self.vfi_SI = xr_df.vfi_SI.values.astype('float64')
        except:
            self.vfi_SI = np.zeros(self.nions)
        try:
            self.vfe_SI = xr_df.vfe_SI.values.astype('float64')
        except:
            self.vfe_SI = np.zeros(1)

        self.kthetarhos = xr_df.kthetarhos.values.astype('float64')
        try:
            self.gam_SI = xr_df.gam_SI.values.astype('float64')
        except:
            try:
                self.gam_GB = xr_df.gam_GB.values.astype('float64')
            except:
                self.gam_SI = np.zeros(self.dimn)
        try:
            self.ome_SI = xr_df.ome_SI.values.astype('float64')
        except:
            try:
                self.ome_GB = xr_df.gam_GB.values.astype('float64')
            except:
                self.ome_SI = np.zeros(self.dimn)

        self.rrmodewidth = xr_df.rrmodewidth.values.astype('float64')
        self.rmodewidth = np.complex128(xr_df.rrmodewidth.values.astype('float64') + 1j * xr_df.irmodewidth.values.astype('float64'))
        self.rmodeshift = np.complex128(xr_df.rrmodeshift.values.astype('float64') + 1j * xr_df.irmodeshift.values.astype('float64'))
        self.distan = xr_df.distan.values.astype('float64')
        self.code_version = xr_df.QLK_CLOSEST_RELEASE
        self.commit = xr_df.QLK_GITSHAKEY
        try:
            self.typee = xr_df.typee.values
        except:
            self.typee = 2

        try:
            self.typei = xr_df.typei.values
        except:
            self.typei = np.ones(self.nions)

        try:
            self.rot_flag = xr_df.rot_flag.values
        except:
            pass
        self.Rref = self.Ro
        self.Bref = self.Bo
        self.Ai0 = np.float64(xr_df.isel(nions=0).Ai.values)
        self.mi0 = self.Ai0 * self.mref
        self.vth_i0 = np.sqrt(np.float64(self.Te) * self.Tref * self.qref / self.mi0)
        self.rho_i0 = self.mi0 * self.vth_i0 / (self.qref * self.Bref)

        try:
            self.anis = xr_df.anis.values.astype('float64')
            self.danisdr = xr_df.danisdr.values.astype('float64')
        except:
            self.anis = 0
            self.danisdr = 0

    def set_storage(self, qlk_path, run_path, run_name):
        # Saving the path of the IMAS2QLK
        imas2qlk_path = os.path.abspath(os.path.dirname(__file__))

        # Absolute qlk path
        qlk_path_abs = os.path.expanduser(qlk_path) #if run_path is relative (means use the "~") it will expand to the correct path (doesn't do anything if the user put an absolute path)
        run_path_abs = os.path.expanduser(run_path)
        print("Absolute Path of QLK fold:", qlk_path_abs)

        if not os.path.isdir(run_path_abs):
            os.mkdir(run_path_abs)

        # Creates runs directory if it doesn't already exist 
        runfoldpath=run_path_abs+'/'+run_name

        if not os.path.isdir(runfoldpath):
            os.mkdir(runfoldpath)

        # don't forget the QuaLiKiz executable
        shutil.copy(qlk_path_abs + '/QuaLiKiz', f'{runfoldpath}/QuaLiKiz')

        # Pre build of the files containing input and outputs
        os.makedirs(f'{runfoldpath}/input', exist_ok=True)
        os.makedirs(f'{runfoldpath}/output', exist_ok=True)
        os.makedirs(f'{runfoldpath}/output/primitive', exist_ok=True)
        os.makedirs(f'{runfoldpath}/debug', exist_ok=True)

        # Come back to the previous work path
        os.chdir(imas2qlk_path)

    def write_input(self, run_path, run_name):

        ################## Write binary input files ################################

        namearg = self.list_properties()
        namearg_that_are_not_qlk_inputs = ['anis', 'danis', 'danisdr', 'ni', 'Ai0', 'mi0', 'mi0', 'rho_i0', 'vth_i0', 'code_version', 'source', 'commit', 'cref', 'me', 'mref', 'Tref', 'nref', 'qref', 'Rref', 'Bref']
        namearg = [item for item in namearg if item not in namearg_that_are_not_qlk_inputs]
        nargu = len(namearg)
        stringind=[] #no string indexes. Kept if adding any future string inputs

        # Change the current work path to the run folder path 
        os.chdir(os.path.expanduser(run_path + '/' + run_name))

        # Write binaries
        for ii in range(nargu):
            if ii in stringind:
                with open(f'input/p{ii}.txt', 'wb') as fid:
                    fid.write(getattr(self, namearg[ii]))
            else:
                with open(f'input/{namearg[ii]}.bin', 'wb') as fid:
                    if isinstance(getattr(self, namearg[ii]), int):
                        fid.write(struct.pack('d', getattr(self, namearg[ii])))  # 'i' for int
                    elif isinstance(getattr(self, namearg[ii]), float):
                        fid.write(struct.pack('d', getattr(self, namearg[ii])))  # 'd' for float
                    elif isinstance(getattr(self, namearg[ii]), list):
                        for item in getattr(self, namearg[ii]):
                            fid.write(struct.pack('d', item))  # float list by default
                    elif isinstance(getattr(self, namearg[ii]), np.ndarray):
                        for item in getattr(self, namearg[ii]):
                            fid.write(struct.pack('d', item))  # float list by default


        # Come back to the previous work path (path of the script)
        os.chdir(os.path.abspath(os.path.dirname(__file__)))


    def load_runfolder(self, run_path=None, run_name = None):
        """Loads data from QLK output and debug files"""
        def read_dat_file(parent_path, filename):
            """
            Read the .dat file and return either a float, or a float list or a float matrix 
            """


            path = os.path.join(parent_path, filename+'.dat')
            with open(path, 'r') as f:
                lines = f.readlines()
                
               #if there is only one line
                if len(lines) == 1 : 
                    nombres = lines[0].split()
                    if len(nombres) == 1:
                        if filename in ['rmodeshift', 'rmodewidth', 'distan', 'kthetarhos', 'Zi', 'Ai', 'normni', 'Ani', 'Ti', 'Ati', 'Ani', 'typei']:
                            return np.asarray([float(nombres[0].strip())])
                        elif filename in ['nions', 'dimx', 'dimn', 'numsols']:
                            return int(nombres[0].strip())
                        else:
                            return float(nombres[0].strip())
                    else:
                        return np.asarray([float(nombre.strip()) for nombre in nombres]) #strip() is used to remove extra whitespaces
                
                
                # if several lines
                else:
                    print(filename)
                    return np.transpose(np.asarray([[float(val.strip()) for val in line.split()] for line in lines]))

        # namearg = self.list_properties()
        # namearg_that_are_not_qlk_inputs = ['cref', 'me', 'mref', 'Tref', 'nref', 'qref']
        # namearg = [item for item in namearg if item not in namearg_that_are_not_qlk_inputs]
       
        #--------------------------------------------------------------------#
        # First debug outputs to have the correct inputs used during the run #
        #--------------------------------------------------------------------#
        debug_path = os.path.expanduser(run_path + '/' + run_name + '/debug/')
        for file in os.listdir(debug_path):
            if file.endswith(".dat"):
                filename = os.path.splitext(file)[0]  # Remove the .dat extension of the file name
                setattr(self, filename, read_dat_file(debug_path, filename))

        #------------------#
        # Then the outputs #
        #------------------#
        output_path = os.path.expanduser(run_path + '/' + run_name + '/output/')
        for file in os.listdir(output_path):
            if file.endswith(".dat"):
                filename = os.path.splitext(file)[0]  # Remove the .dat extension of the file name
                setattr(self, filename, read_dat_file(output_path, filename))
        
        #----------------------------------------#
        # Then the primitive in the ouput folder #
        #----------------------------------------#
        output_primitive_path = os.path.expanduser(run_path + '/' + run_name + '/output/primitive/')
        for file in os.listdir(output_primitive_path):
            if file.endswith(".dat"):
                filename = os.path.splitext(file)[0]  # Remove the .dat extension of the file name
                setattr(self, filename, read_dat_file(output_primitive_path, filename))


    def list_properties(self,mode='short'):
        """lists properties of the class for testing purposes"""
        proplist = []
        if mode == 'short':
            for i in inspect.getmembers(self):

                # to remove private and protected
                # functions
                if not i[0].startswith('_'):

                    # To remove other methods that
                    # doesnot start with a underscore
                    if not inspect.ismethod(i[1]):
                        # print(f'{i[0], type(i[1])}: {i[1]}')
                        proplist.append(i[0])
        elif mode in ['-A','-a']:
            for i in inspect.getmembers(self):
                # print(f'{i[0], type(i[1])}: {i[1]}')
                proplist.append(i[0])
        return proplist

    def load_JET_QLK_line(self, inputs, outputs, wavevectors):
        """Loads data from a line of the QLK jet dataset"""
        self.source = 'JET Pandas'
        #unused inputs: ['Zeff', 'R0', 'logNustar']
        self.Rmin = inputs['Rmin']
        self.x = inputs['x']
        self.Ro = inputs['Ro']
        self.Bo = inputs['Bo']
        self.nions = 3

        self.q = inputs['q']
        self.smag = inputs['smag']
        self.alpha = inputs['alpha']
        self.Machtor = inputs['Machtor']
        self.Machpar = inputs['Machpar']
        self.Autor = inputs['Autor']
        self.Aupar = inputs['Aupar']
        self.gammaE = inputs['gammaE']
        self.normni = np.array([inputs[f'normni{n}'] for n in range(self.nions)])
        self.Zi = np.array([inputs[f'Zi{n}'] for n in range(self.nions)])
        self.ne = inputs['ne']
        self.ni = np.array([normni * self.ne for normni in self.normni])
        self.Te = 1
        self.Ti = np.array([inputs[f'Ti_Te{n}'] for n in range(self.nions)])
        self.Ane = inputs['Ane']
        self.Ani = np.array([inputs[f'Ani{n}'] for n in range(self.nions)])
        self.Ate = inputs['Ate']
        self.Ati = np.array([inputs[f'Ati{n}'] for n in range(self.nions)])
        self.Ai = np.array([inputs[f'Ai{n}'] for n in range(self.nions)])
        self.rho = inputs['rho']
        self.typei = np.array([1 for n in range(self.nions)])

        self.kthetarhos = np.array(wavevectors['kthetarhos'])
        self.dimn = len(self.kthetarhos)


        self.Rref = self.Ro
        self.Bref = self.Bo
        self.Ai0 = inputs['Ai0']
        self.mi0 = self.Ai0 * self.mref
        self.vth_i0 = np.sqrt(np.float64(self.Te) * self.Tref * self.qref / self.mi0)
        self.rho_i0 = self.mi0 * self.vth_i0 / (self.qref * self.Bref)


        self.numsols = 2

        self.gam_GB = np.array([[outputs[f'gam_GB_k{k}_s{n}'] for n in range(self.numsols)] for k in range(self.dimn)])
        self.ome_GB = np.array([[outputs[f'ome_GB_k{k}_s{n}'] for n in range(self.numsols)] for k in range(self.dimn)])


    def load_IMAS_ids_only_inputs(self, ids_path=None, ids=None, ):
        """Loads data from an ids file and converts it to QLK normalisations. Can pass an existing IDS or the path to one.
        :param ids_path: path to load IDS from
        :param ids: existing IDS to load, not used ids_path is provided
        """
        if ids is None and ids_path is None:
            print('no ids or path provided')
            return
        elif ids is None and ids_path is not None:
            # load ids from file
            print('generating empty ids')
            ids = gkids.GyrokineticsLocal()
            # fill with default values
            print('filling with default values')
            idspy.fill_default_values_ids(ids)
            # update with values from file
            print('filling with values from file')
            idspy.hdf5_to_ids(ids_path, ids)

        try:
            # get number of wavevectors
            dimn = len(ids.linear.wavevector)
        except:
            dimn = 0

        # check number of solutions for qlk numsols (max of 3)
        try:
            if dimn > 0:
                numsols = [len(ids.linear.wavevector[nk].eigenmode) for nk in range(dimn)]
            else:
                numsols = [2]
        except:
            numsols = [2]
        if set(numsols) == {numsols[0]}:
            numsols = min(3, numsols[0])
            print(f'Constant numsol')
        else:
            numsols = min(3, max(numsols))
            print(f'Number of eigenmodes per wavevector is different. Using largest <= 3 by default')



        # species related sorted and indexing to separate ions, electrons and find main ion
        e_count = 0
        i_indices = []
        max_ion_dens = 0
        for index, species in enumerate(ids.species):
            # round charge to nearest whole number to account for different normalising quantities
            charge = round(species.charge_norm, 0)
            if charge < 0:
                # TODO: fit an error in here if charge != -1?
                # index as not ion
                if charge == -1:
                    # running electron count, needs to be 1 for script to work
                    e_count += 1
                    e_index = index
                if charge != -1:
                    print('Non 1 negative charge on species (not an electron), conversion aborted.')
                    return
            elif charge > 0:
                # index as ion
                i_indices.append(index)
                # find main ion
                if species.density_norm > max_ion_dens:
                    max_ion_dens = species.density_norm
                    main_ion_index = index
            elif charge == 0:
                print('No charge on species, conversion aborted.')
                return
        if e_count != 1:
            print('Electron species count not equal to 1. Conversion aborted, please check input file.')
            return

        # rearrange ion indices to have main ion first
        i_indices_reordered = [main_ion_index]
        for index in i_indices:
            if index != main_ion_index:
                i_indices_reordered.append(index)

        # number of ions
        nions = len(ids.species) - e_count


        if idspy.is_default_imas_value(ids.normalizing_quantities, 'r'):
            self.Ro = 3
        else:
            self.Ro = ids.normalizing_quantities.r

        if idspy.is_default_imas_value(ids.normalizing_quantities, 'b_field_tor'):
            self.Bo = 5
        else:
            self.Bo = ids.normalizing_quantities.b_field_tor

        if idspy.is_default_imas_value(ids.normalizing_quantities, 'n_e'):
            self.ne = 1
        else:
            self.ne = ids.normalizing_quantities.n_e / self.nref

        if idspy.is_default_imas_value(ids.normalizing_quantities, 't_e'):
            self.Te = 10
        else:
            self.Te = ids.species[e_index].temperature_norm * ids.normalizing_quantities.t_e / self.Tref

        self.Rref = self.Ro
        self.Bref = self.Bo

        # load values to calculated IMAS constants and Ratios
        IMAS = constantsIMAS(self.Ro, self.Bo, self.Te, self.Te, self.Tref, self.nref)
        RATIOS = constantsRATIO(self, IMAS, baseline=0)

        self.source = 'ids'


        self.x = 1
        self.Rmin = ids.flux_surface.r_minor_norm * self.Ro / self.x

        self.q = np.abs(ids.flux_surface.q)
        self.smag = ids.flux_surface.magnetic_shear_r_minor
        self.alpha = ids.flux_surface.pressure_gradient_norm * self.q ** 2 * RATIOS.Rrat / RATIOS.Brat ** 2
        if not idspy.is_default_imas_value(ids.species_all, 'velocity_tor_norm'):
            self.Machtor = ids.species_all.velocity_tor_norm * IMAS.vth_ref * RATIOS.Rrat / self.cref
        else:
            self.Machtor = 0
        self.Machpar = 0
        self.Aupar = 0
        self.Autor = ids.species[main_ion_index].velocity_tor_gradient_norm * IMAS.vth_ref * (RATIOS.Rrat ** 2) / self.cref
        if not idspy.is_default_imas_value(ids.species[main_ion_index], 'velocity_tor_gradient_norm'):
            self.Autor = ids.species[main_ion_index].velocity_tor_gradient_norm * IMAS.vth_ref * (RATIOS.Rrat ** 2) / self.cref
        else:
            self.Autor = 0

        if not idspy.is_default_imas_value(ids.species_all, 'shearing_rate_norm'):
            self.gammaE = ids.species_all.shearing_rate_norm * IMAS.vth_ref * RATIOS.Rrat / self.cref
        else:
            print('No shearing_rate_norm field in IDS, falling back to default value of 0')
            self.gammaE = 0

        vel_grads = {species.velocity_tor_gradient_norm for species in ids.species}
        # loop to check velocity gradients are equal and send warning if not.
        if len(vel_grads) > 1:
            print(
                'IDS Species have different velocity gradients. This is not supported in QLK and the main ion velocity gradient has been used by default.')

        # Electron Properties
        # select electron species from ids for simplicity
        electron = ids.species[e_index]
        self.Ane = electron.density_log_gradient_norm * RATIOS.Rrat
        # QLK Electron Logarithmic Temperature Gradient
        self.Ate = electron.temperature_log_gradient_norm * RATIOS.Rrat
        e_charge = np.abs(electron.charge_norm)



        # Ion Properties
        qlk_Ions_props = {'Ti': [], 'normni': [], 'Ani': [], 'Ati': [], 'Ai': [], 'Zi': [], 'typei': []}
        for index in i_indices_reordered:
            # select ion species from ids for simplicity
            ion = ids.species[index]
            # QLK Ion Temperature
            Ti_QLK = ion.temperature_norm * self.Te
            qlk_Ions_props['Ti'].append(Ti_QLK)
            # QLK Ion Density
            normni_QLK = ion.density_norm
            qlk_Ions_props['normni'].append(normni_QLK)
            # QLK Ion Logarithmic Density Gradient
            Ani_QLK = ion.density_log_gradient_norm * RATIOS.Rrat
            qlk_Ions_props['Ani'].append(Ani_QLK)
            # QLK Ion Logarithmic Temperature Gradient
            Ati_QLK = ion.temperature_log_gradient_norm * RATIOS.Rrat
            qlk_Ions_props['Ati'].append(Ati_QLK)
            # QLK Ion Mass
            Ai_QLK = ion.mass_norm / RATIOS.mrat
            qlk_Ions_props['Ai'].append(Ai_QLK)
            # QLK Ion Charge (adjusted to be relative to the QLK electron charge of 1)
            Zi_QLK = ion.charge_norm / e_charge
            qlk_Ions_props['Zi'].append(Zi_QLK)
            # QLK Ion type
            if normni_QLK == 0:
                # set to tracer if density = 0
                qlk_Ions_props['typei'].append(3)
            else:
                # set to active otherwise
                qlk_Ions_props['typei'].append(1)

        self.normni = np.array(qlk_Ions_props['normni'])
        self.Zi = np.array(qlk_Ions_props['Zi'])
        self.ni = self.normni * self.ne
        self.Ai = np.array(qlk_Ions_props['Ai'])
        self.Ti = np.array(qlk_Ions_props['Ti'])
        self.Ani = np.array(qlk_Ions_props['Ani'])
        self.Ati = np.array(qlk_Ions_props['Ati'])

        self.Ai0 = self.Ai[0]
        self.mi0 = self.Ai0 * self.mref
        self.vth_i0 = np.sqrt(np.float64(self.Te) * self.Tref * self.qref / self.mi0)
        self.rho_i0 = self.mi0 * self.vth_i0 / (self.qref * self.Bref)

        # recalculate ratios now that we have all the main ion quantities
        RATIOS = constantsRATIO(self, IMAS)

        self.kthetarhos = np.array([wavevector.binormal_wavevector_norm * RATIOS.rho_rat for wavevector in ids.linear.wavevector])


        # adiabatic electrons for both QLK-IMAS and IMAS-QLK
        if ids.model.adiabatic_electrons == 1:
            # set to adibatic electrons if IMAS = 1
            self.typee = 2
        else:
            # set to active electrons if IMAS = 0
            self.typee = 1

        self.typei = np.array([3 if dens == 0 else 1 for dens in self.normni])

        # temperature anisotropy and gradient set to 0
        self.anis = 0
        self.danisdr = 0
        try:
            if not idspy.is_default_imas_value(ids.model, 'include_centrifugal_effects'):
                self.rot_flag = ids.model.include_centrifugal_effects
            else:
                pass
        except:
            pass


        self.numsols = numsols
        self.dimn = dimn
        self.nions = nions

   

    def load_IMAS_ids(self, ids_path=None, ids=None, ):
        """Loads data from an ids file and converts it to QLK normalisations. Can pass an existing IDS or the path to one.
        :param ids_path: path to load IDS from
        :param ids: existing IDS to load, not used ids_path is provided
        """
        if ids is None and ids_path is None:
            print('no ids or path provided')
            return
        elif ids is None and ids_path is not None:
            # load ids from file
            print('generating empty ids')
            ids = gkids.GyrokineticsLocal()
            # fill with default values
            print('filling with default values')
            idspy.fill_default_values_ids(ids)
            # update with values from file
            print('filling with values from file')
            idspy.hdf5_to_ids(ids_path, ids)

        try:
            # get number of wavevectors
            dimn = len(ids.linear.wavevector)
        except:
            dimn = 0

        # check number of solutions for qlk numsols (max of 3)
        try:
            if dimn > 0:
                numsols = [len(ids.linear.wavevector[nk].eigenmode) for nk in range(dimn)]
            else:
                numsols = [2]
        except:
            numsols = [2]
        if set(numsols) == {numsols[0]}:
            numsols = min(3, numsols[0])
            print(f'Constant numsol')
        else:
            numsols = min(3, max(numsols))
            print(f'Number of eigenmodes per wavevector is different. Using largest <= 3 by default')



        # species related sorted and indexing to separate ions, electrons and find main ion
        e_count = 0
        i_indices = []
        max_ion_dens = 0
        for index, species in enumerate(ids.species):
            # round charge to nearest whole number to account for different normalising quantities
            charge = round(species.charge_norm, 0)
            if charge < 0:
                # TODO: fit an error in here if charge != -1?
                # index as not ion
                if charge == -1:
                    # running electron count, needs to be 1 for script to work
                    e_count += 1
                    e_index = index
                if charge != -1:
                    print('Non 1 negative charge on species (not an electron), conversion aborted.')
                    return
            elif charge > 0:
                # index as ion
                i_indices.append(index)
                # find main ion
                if species.density_norm > max_ion_dens:
                    max_ion_dens = species.density_norm
                    main_ion_index = index
            elif charge == 0:
                print('No charge on species, conversion aborted.')
                return
        if e_count != 1:
            print('Electron species count not equal to 1. Conversion aborted, please check input file.')
            return

        # rearrange ion indices to have main ion first
        i_indices_reordered = [main_ion_index]
        for index in i_indices:
            if index != main_ion_index:
                i_indices_reordered.append(index)

        # number of ions
        nions = len(ids.species) - e_count


        if idspy.is_default_imas_value(ids.normalizing_quantities, 'r'):
            self.Ro = 3
        else:
            self.Ro = ids.normalizing_quantities.r

        if idspy.is_default_imas_value(ids.normalizing_quantities, 'b_field_tor'):
            self.Bo = 5
        else:
            self.Bo = ids.normalizing_quantities.b_field_tor

        if idspy.is_default_imas_value(ids.normalizing_quantities, 'n_e'):
            self.ne = 1
        else:
            self.ne = ids.normalizing_quantities.n_e / self.nref

        if idspy.is_default_imas_value(ids.normalizing_quantities, 't_e'):
            self.Te = 10
        else:
            self.Te = ids.species[e_index].temperature_norm * ids.normalizing_quantities.t_e / self.Tref

        self.Rref = self.Ro
        self.Bref = self.Bo

        # load values to calculated IMAS constants and Ratios
        IMAS = constantsIMAS(self.Ro, self.Bo, self.Te, self.Te, self.Tref, self.nref)
        RATIOS = constantsRATIO(self, IMAS, baseline=0)

        self.source = 'ids'


        self.x = 1
        self.Rmin = ids.flux_surface.r_minor_norm * self.Ro / self.x

        self.q = np.abs(ids.flux_surface.q)
        self.smag = ids.flux_surface.magnetic_shear_r_minor
        self.alpha = ids.flux_surface.pressure_gradient_norm * self.q ** 2 * RATIOS.Rrat / RATIOS.Brat ** 2
        if not idspy.is_default_imas_value(ids.species_all, 'velocity_tor_norm'):
            self.Machtor = ids.species_all.velocity_tor_norm * IMAS.vth_ref * RATIOS.Rrat / self.cref
        else:
            self.Machtor = 0
        self.Machpar = 0
        self.Aupar = 0
        self.Autor = ids.species[main_ion_index].velocity_tor_gradient_norm * IMAS.vth_ref * (RATIOS.Rrat ** 2) / self.cref
        if not idspy.is_default_imas_value(ids.species[main_ion_index], 'velocity_tor_gradient_norm'):
            self.Autor = ids.species[main_ion_index].velocity_tor_gradient_norm * IMAS.vth_ref * (RATIOS.Rrat ** 2) / self.cref
        else:
            self.Autor = 0

        if not idspy.is_default_imas_value(ids.species_all, 'shearing_rate_norm'):
            self.gammaE = ids.species_all.shearing_rate_norm * IMAS.vth_ref * RATIOS.Rrat / self.cref
        else:
            print('No shearing_rate_norm field in IDS, falling back to default value of 0')
            self.gammaE = 0

        vel_grads = {species.velocity_tor_gradient_norm for species in ids.species}
        # loop to check velocity gradients are equal and send warning if not.
        if len(vel_grads) > 1:
            print(
                'IDS Species have different velocity gradients. This is not supported in QLK and the main ion velocity gradient has been used by default.')

        # Electron Properties
        # select electron species from ids for simplicity
        electron = ids.species[e_index]
        self.Ane = electron.density_log_gradient_norm * RATIOS.Rrat
        # QLK Electron Logarithmic Temperature Gradient
        self.Ate = electron.temperature_log_gradient_norm * RATIOS.Rrat
        e_charge = np.abs(electron.charge_norm)



        # Ion Properties
        qlk_Ions_props = {'Ti': [], 'normni': [], 'Ani': [], 'Ati': [], 'Ai': [], 'Zi': [], 'typei': []}
        for index in i_indices_reordered:
            # select ion species from ids for simplicity
            ion = ids.species[index]
            # QLK Ion Temperature
            Ti_QLK = ion.temperature_norm * self.Te
            qlk_Ions_props['Ti'].append(Ti_QLK)
            # QLK Ion Density
            normni_QLK = ion.density_norm
            qlk_Ions_props['normni'].append(normni_QLK)
            # QLK Ion Logarithmic Density Gradient
            Ani_QLK = ion.density_log_gradient_norm * RATIOS.Rrat
            qlk_Ions_props['Ani'].append(Ani_QLK)
            # QLK Ion Logarithmic Temperature Gradient
            Ati_QLK = ion.temperature_log_gradient_norm * RATIOS.Rrat
            qlk_Ions_props['Ati'].append(Ati_QLK)
            # QLK Ion Mass
            Ai_QLK = ion.mass_norm / RATIOS.mrat
            qlk_Ions_props['Ai'].append(Ai_QLK)
            # QLK Ion Charge (adjusted to be relative to the QLK electron charge of 1)
            Zi_QLK = ion.charge_norm / e_charge
            qlk_Ions_props['Zi'].append(Zi_QLK)
            # QLK Ion type
            if normni_QLK == 0:
                # set to tracer if density = 0
                qlk_Ions_props['typei'].append(3)
            else:
                # set to active otherwise
                qlk_Ions_props['typei'].append(1)

        self.normni = np.array(qlk_Ions_props['normni'])
        self.Zi = np.array(qlk_Ions_props['Zi'])
        self.ni = self.normni * self.ne
        self.Ai = np.array(qlk_Ions_props['Ai'])
        self.Ti = np.array(qlk_Ions_props['Ti'])
        self.Ani = np.array(qlk_Ions_props['Ani'])
        self.Ati = np.array(qlk_Ions_props['Ati'])

        self.Ai0 = self.Ai[0]
        self.mi0 = self.Ai0 * self.mref
        self.vth_i0 = np.sqrt(np.float64(self.Te) * self.Tref * self.qref / self.mi0)
        self.rho_i0 = self.mi0 * self.vth_i0 / (self.qref * self.Bref)

        # recalculate ratios now that we have all the main ion quantities
        RATIOS = constantsRATIO(self, IMAS)

        self.kthetarhos = np.array([wavevector.binormal_wavevector_norm * RATIOS.rho_rat for wavevector in ids.linear.wavevector])


        # adiabatic electrons for both QLK-IMAS and IMAS-QLK
        if ids.model.adiabatic_electrons == 1:
            # set to adibatic electrons if IMAS = 1
            self.typee = 2
        else:
            # set to active electrons if IMAS = 0
            self.typee = 1

        self.typei = np.array([3 if dens == 0 else 1 for dens in self.normni])

        # temperature anisotropy and gradient set to 0
        self.anis = 0
        self.danisdr = 0
        try:
            if not idspy.is_default_imas_value(ids.model, 'include_centrifugal_effects'):
                self.rot_flag = ids.model.include_centrifugal_effects
            else:
                pass
        except:
            pass

        # load remaining values not used for QLK (to be able to loop conversion back and forth)
        self.code_version = ids.code.version
        self.commit = ids.code.commit

        self.numsols = numsols
        self.dimn = dimn
        self.nions = nions

        # outputs
        try:
            self.gam_SI = np.array([[eigenmode.growth_rate_norm * IMAS.vth_ref / IMAS.Rref for eigenmode in wavevector.eigenmode] for wavevector in ids.linear.wavevector])
        except:
            pass
        try:
            self.ome_SI = np.array([[-eigenmode.frequency_norm * IMAS.vth_ref / IMAS.Rref for eigenmode in wavevector.eigenmode] for wavevector in ids.linear.wavevector])
        except:
            pass
        try:
            self.pfe_SI = np.array(ids.non_linear.fluxes_1d.particles_phi_potential[e_index] * self.ne * np.float64(1e19) * (IMAS.vth_ref * IMAS.rho_star) ** 2)
        except:
            pass
        try:
            self.efe_SI = np.array(ids.non_linear.fluxes_1d.energy_phi_potential[e_index] * self.ne * np.float64(1e19) * IMAS.Tref * IMAS.vth_ref * IMAS.rho_star ** 2)
        except:
            pass
        try:
            self.vfe_SI = np.array(ids.non_linear.fluxes_1d.momentum_tor_perpendicular_phi_potential[e_index] * self.ne * np.float64(1e19) * IMAS.mref * IMAS.Rref * (IMAS.vth_ref * IMAS.rho_star) ** 2)
        # fluxes contain all particles in a specific order, but ni has already been reordered so need to iterate through both differently hence the n and i_index
        except:
            pass
        try:
            self.pfi_SI = np.array(ids.non_linear.fluxes_1d.particles_phi_potential)[i_indices_reordered] * self.ni * np.float64(1e19) * (IMAS.vth_ref * IMAS.rho_star) ** 2
        except:
            pass
        try:
            self.efi_SI = np.array(ids.non_linear.fluxes_1d.energy_phi_potential)[i_indices_reordered] * self.ni * np.float64(1e19) * IMAS.Tref * IMAS.vth_ref * IMAS.rho_star ** 2
        except:
            pass
        try:
            self.vfi_SI = np.array(ids.non_linear.fluxes_1d.momentum_tor_perpendicular_phi_potential)[i_indices_reordered] * self.ni * np.float64(1e19) * IMAS.mref * IMAS.Rref * (IMAS.vth_ref * IMAS.rho_star) ** 2
        except:
            pass
        #
        # self.distan =
        # self.rrmodewidth =
        # self.rmodeshift =
        # self.rmodewidth =


        try:
            self.thetas = np.array([[eigenmode.angle_pol for eigenmode in wavevector.eigenmode] for wavevector in ids.linear.wavevector])
        except:
            pass
        try:
            self.pol_turns = np.array([[eigenmode.poloidal_turns for eigenmode in wavevector.eigenmode] for wavevector in ids.linear.wavevector])
        except:
            pass
        try:
            self.phi = np.array([[eigenmode.fields.phi_potential_perturbed_norm for eigenmode in wavevector.eigenmode] for wavevector in ids.linear.wavevector])
        except:
            pass
    
    def to_IMAS_ids(self, provider, comment=''):
        """
        converts the existing QLK class to IMAS IDS format and returns the filled IDS
        :param provider: name of person doing conversion
        :param comment: optional comment
        """
        # initialise constants
        IMAS = constantsIMAS(self.Ro, self.Bo, self.Te, self.ne, self.Tref, self.nref)
        RATIOS = constantsRATIO(self, IMAS)

        # ids properties #
        ids_properties: gkids.IdsProperties = gkids.IdsProperties(
            provider=provider,
            creation_date=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            comment=f'Converted from {self.source}. {comment}',
            homogeneous_time=int(2),
        )

        # code #
        code: gkids.Code = gkids.Code(
            name="QuaLiKiz",
            repository="https://gitlab.com/qualikiz-group/QuaLiKiz",
            commit=str(self.commit),
            version=str(self.code_version),
            # library = , (not relevant)
        )

        # normalizing_quantities #
        normalizing_quantities: gkids.InputNormalizing = gkids.InputNormalizing(
            t_e=IMAS.Tref,
            n_e=IMAS.nref,
            r=np.float64(IMAS.Rref),
            b_field_tor=np.float64(IMAS.Bref),
        )

        # model #
        model: gkids.Model = gkids.Model(
            adiabatic_electrons=electron_type(self.typee),
            include_a_field_parallel=include_a_field_parallel(),
            include_b_field_parallel=include_b_field_parallel(),
            include_full_curvature_drift=include_full_curvature_drift(),
            #TODO include_coriolis_drift=,
            include_centrifugal_effects=include_centrifugal_effects(self.rot_flag, self.x),
            collisions_pitch_only=collisions_pitch_only(),
            collisions_momentum_conservation=collisions_momentum_conservation(),
            collisions_energy_conservation=collisions_energy_conservation(),
            collisions_finite_larmor_radius=collisions_finite_larmor_radius(),
        )
        # flux_surface #
        flux_surface: gkids.FluxSurface = gkids.FluxSurface(
            ip_sign=ip_sign(),
            b_field_tor_sign=b_field_tor_sign(),
            r_minor_norm=r_minor_norm(self.Rmin, self.x, self.Ro),
            q=q_IMAS(self.q),
            magnetic_shear_r_minor=magnetic_shear_r_minor(self.smag),
            pressure_gradient_norm=pressure_gradient_norm(self.alpha, self.q, RATIOS.Rrat, RATIOS.Brat),
            dgeometric_axis_r_dr_minor=dgeometric_axis_r_dr_minor(self.alpha),
            dgeometric_axis_z_dr_minor=dgeometric_axis_z_dr_minor(),
            elongation=elongation(),
            delongation_dr_minor_norm=delongation_dr_minor_norm(),
            shape_coefficients_c=shape_coefficients_c(),
            dc_dr_minor_norm=dc_dr_minor_norm(),
            shape_coefficients_s=shape_coefficients_s(),
            ds_dr_minor_norm=ds_dr_minor_norm(),
        )

        # species_all #
        species_all = gkids.InputSpeciesGlobal(
            velocity_tor_norm=velocity_tor_norm(self.Machtor, self.cref, IMAS.vth_ref, RATIOS.Rrat),
            shearing_rate_norm=shearing_rate_norm(self.gammaE, self.cref, IMAS.vth_ref, RATIOS.Rrat),
            beta_reference=beta_reference(),
            debye_length_norm=debye_length_reference(),
            #TODO angle_pol=,
        )

        # species #
        # electron first
        electron_temp = gkids.Species(
            charge_norm=charge_norm(np.float64(-1), RATIOS.qrat),
            mass_norm=mass_norm(self.me / self.mref, RATIOS.mrat),
            density_norm=density_norm_e(self.ne),
            density_log_gradient_norm=density_log_gradient(self.Ane, RATIOS.Rrat),
            temperature_norm=temperature_norm(self.Te, self.Te),
            temperature_log_gradient_norm=temperature_log_gradient(self.Ate, RATIOS.Rrat),
            velocity_tor_gradient_norm=velocity_tor_gradient_norm(self.Autor, self.cref, IMAS.vth_ref, RATIOS.Rrat),
            #TODO potential_energy_norm ?
            #TODO potential_energy_gradient_norm ?

        )



        # ions
        # precalculate arrays to access with loop over nions
        Charge_Norm = charge_norm(self.Zi, RATIOS.qrat)
        Mass_norm = mass_norm(self.Ai, RATIOS.mrat)
        # calculate density of ions accounting for tracer ions
        ion_density_mult = ion_type(self.typei)
        dens_norm = density_norm_i(self.normni) * ion_density_mult
        dens_grad = density_log_gradient(self.Ani, RATIOS.Rrat)
        temperature_norm_i = temperature_norm(self.Ti, self.Te)
        temp_grad_i = temperature_log_gradient(self.Ati, RATIOS.Rrat)
        print('temperature norm : ', temperature_norm_i)
        species: tuple[gkids.Species] = (electron_temp,) + tuple([gkids.Species(
            charge_norm=Charge_Norm[nion],
            mass_norm=Mass_norm[nion],
            density_norm=dens_norm[nion],
            density_log_gradient_norm=dens_grad[nion],
            temperature_norm=temperature_norm_i[nion],
            temperature_log_gradient_norm=temp_grad_i[nion],
            velocity_tor_gradient_norm=electron_temp.velocity_tor_gradient_norm
            #TODO potential_energy_norm ?
            #TODO potential_energy_gradient_norm?
        ) for nion in range(self.nions)])

        # collisionality #
        coll_array = np.zeros((self.nions + 1, self.nions + 1))
        coll_norm = collisionality_norm(IMAS.Rref,
                                        IMAS.vth_ref,
                                        self.ne,
                                        dens_norm,
                                        self.Zi,
                                        RATIOS.qrat,
                                        self.Te,
                                        IMAS.vth_e)
        for nion in range(self.nions):
            coll_array[0, nion + 1] = coll_norm[nion]

        collisions: gkids.Collisions = gkids.Collisions(collisionality_norm=coll_array)

        # linear #
        linear: gkids.GyrokineticsLinear() = gkids.GyrokineticsLinear()

        # wavevectors
        binormal_wavevector_norm = binormal_component_norm(RATIOS.rho_rat, self.kthetarhos)
        # calculate growth rate and frequency for whole set
        try:
            growth_rate_norms = growth_rate_norm_SI(self.gam_SI, IMAS.Rref, IMAS.vth_ref)
        except:
            growth_rate_norms = growth_rate_norm_GB(self.gam_GB, self.Rmin, IMAS.Rref, RATIOS.vth_rat)
        try:
            frequency_norms = frequency_norm_SI(self.ome_SI, IMAS.Rref, IMAS.vth_ref)
        except:
            frequency_norms = frequency_norm_GB(self.ome_GB, self.Rmin, IMAS.Rref, RATIOS.vth_rat)


        # loop over wavevectors
        for dimn in range(self.dimn):
            try:
                # eigenmode
                d = self.distan[dimn]
                w = self.rmodewidth[dimn]
                x0 = self.rmodeshift[dimn]
                print(dimn)
                print(self.numsols)
                print(growth_rate_norms)
                # growth_rate_norms = np.transpose(growth_rate_norms)
                # print(growth_rate_norms)

                poloidal_angle, pol_turns = poloidal_angle_pturns(w, d, x0)
                phi_potential_perturbed_norm, Af = phi_potential_perturbed_norm_linear(poloidal_angle, w, x0, d)
                # generate default wavevector class
                wavevector_temp:gkids.Wavevector() = gkids.Wavevector(
                    radial_wavevector_norm=radial_component_norm(),
                    binormal_wavevector_norm=binormal_wavevector_norm[dimn],

                    # iterate over the number of solutions
                    eigenmode=[gkids.Eigenmode(poloidal_turns=pol_turns,
                                                angle_pol=poloidal_angle,
                                                #todo: time_norm = 1,?
                                                initial_value_run = 0,
                                                code=gkids.Code(),
                                                growth_rate_norm=growth_rate_norms[dimn, numsol],
                                                frequency_norm=frequency_norms[dimn, numsol],
                                                fields=gkids.EigenmodeFields(phi_potential_perturbed_norm=[phi_potential_perturbed_norm]),
                                                # moments=,
                                                # linear_weights=,
                                                )
                                for numsol in range(self.numsols)]
                )
            except:
                try:
                    wavevector_temp: gkids.Wavevector() = gkids.Wavevector(
                        radial_wavevector_norm=radial_component_norm(),
                        binormal_wavevector_norm=binormal_wavevector_norm[dimn],

                        # iterate over the number of solutions
                        eigenmode=[gkids.Eigenmode(poloidal_turns=self.pol_turns[dimn, numsol],
                                                    angle_pol=self.thetas[dimn, numsol],
                                                    # todo: time_norm = 1,?
                                                    initial_value_run=0,
                                                    code=gkids.Code(),
                                                    growth_rate_norm=growth_rate_norms[dimn, numsol],
                                                    frequency_norm=frequency_norms[dimn, numsol],
                                                    fields=gkids.EigenmodeFields(
                                                        phi_potential_perturbed_norm=self.phi[dimn, numsol]),
                                                    # moments=,
                                                    # linear_weights=,
                                                    )
                            for numsol in range(self.numsols)]
                    )
                except:
                    wavevector_temp: gkids.Wavevector() = gkids.Wavevector(
                        radial_wavevector_norm=radial_component_norm(),
                        binormal_wavevector_norm=binormal_wavevector_norm[dimn],

                        # iterate over the number of solutions
                        eigenmode=[gkids.Eigenmode()
                                    for numsol in range(self.numsols)]
                    )
            linear.wavevector.append(wavevector_temp)

        # non-linear
        try:
            ion_particle_flux = normalise_integrated_particle_flux(self.pfi_SI, self.ni, IMAS.vth_ref, IMAS.rho_star, Af=1)
        except:
            ion_particle_flux = np.zeros(self.nions)
        try:
            electron_particle_flux = normalise_integrated_particle_flux(self.pfe_SI, self.ne, IMAS.vth_ref, IMAS.rho_star, Af=1)
        except:
            electron_particle_flux = np.zeros(1)

        particle_flux = np.append(electron_particle_flux, ion_particle_flux)

        try:
            ion_energy_flux = normalise_integrated_energy_flux(self.efi_SI, self.ni, IMAS.vth_ref, IMAS.rho_star, IMAS.Tref, Af=1)
        except:
            ion_energy_flux = np.zeros(self.nions)
        try:
            electron_energy_flux = normalise_integrated_energy_flux(self.efe_SI, self.ne, IMAS.vth_ref, IMAS.rho_star, IMAS.Tref, Af=1)
        except:
            electron_energy_flux = np.zeros(1)

        energy_flux = np.append(electron_energy_flux, ion_energy_flux)
        parallel_momentum_flux = np.append(np.array(0), np.zeros(self.nions))
        # catch missing vfe from adiabatic electrons
        try:
            ion_perpendicular_momentum_flux = normalise_integrated_momentum_flux(self.vfi_SI, self.ni, IMAS.vth_ref, IMAS.rho_star, self.mref, IMAS.Rref, Af=1)
        except:
            ion_perpendicular_momentum_flux = np.zeros(self.nions)
        try:
            electron_perpendicular_momentum_flux = normalise_integrated_momentum_flux(self.vfe_SI, self.ne, IMAS.vth_ref, IMAS.rho_star, self.mref, IMAS.Rref, Af=1)
        except:
            electron_perpendicular_momentum_flux = np.zeros(1)

        perpendicular_momentum_flux = np.append(electron_perpendicular_momentum_flux, ion_perpendicular_momentum_flux)
        non_linear: gkids.GyrokineticsNonLinear() = gkids.GyrokineticsNonLinear(binormal_wavevector_norm=binormal_wavevector_norm,
                                                                                radial_wavevector_norm=radial_component_norm(binormal_wavevector_norm),
                                                                                quasi_linear=1,
                                                                                code=gkids.CodePartialConstant(),
                                                                                fluxes_1d=gkids.FluxesNl1D(
                                                                                    particles_phi_potential=particle_flux,
                                                                                    energy_phi_potential=energy_flux,
                                                                                    momentum_tor_parallel_phi_potential=parallel_momentum_flux,
                                                                                    momentum_tor_perpendicular_phi_potential=perpendicular_momentum_flux)
                                                                                )


        ids = gkids.GyrokineticsLocal(
            ids_properties=ids_properties,
            code=code,
            normalizing_quantities=normalizing_quantities,
            model=model,
            flux_surface=flux_surface,
            species_all=species_all,
            species=species,
            collisions=collisions,
            linear=linear,
            non_linear=non_linear,
        )
        return ids


    def to_IMAS_pandas(self, df_to_append=None, column_names_list=None, FASTER_dataset='Edge10D', output='split_df'):
        """
        converts the existing QLK file to IMAS and outputs lines of a pandas dataframe specifically for machine learning
        purposes in the FASTER project. If given an existing dataframe, will add the line to the existing dataframe.
        Note that this output may not be suitable for use outside of this intended function.

        :param df_to_append: dataframe to which lines are appended, optional.
        :param column_names_list: if no dataframe provided, will use these as the variable names. Otherwise will default to the FASTER_dataset default value.
        :param FASTER_dataset: string indicating which dataset the data comes from (treats different with different number of columns in the output dataframe.
        Current/planned options: "Edge10D, JET_QLK, JET_GKW"
        :param output: ('dict'/'df'/'split_df') whether to output as a dataset or a dict (recommend dict for speed which can later be turned into a df)
        """
        # initialise constants
        IMAS = constantsIMAS(self.Ro, self.Bo, self.Te, self.ne, self.Tref, self.nref)
        RATIOS = constantsRATIO(self, IMAS)

        # calculate non loop quantities
        IMAS_quantities = {
        'density_log_gradient_e' : density_log_gradient(self.Ane, RATIOS.Rrat),
        'temperature_log_gradient_e' : temperature_log_gradient(self.Ate, RATIOS.Rrat),
        'q': q_IMAS(self.q),
        'magnetic_shear_r_minor': magnetic_shear_r_minor(self.smag),
        'binormal_component_norm': binormal_component_norm(RATIOS.rho_rat2, self.kthetarhos)
        }

        # loop over number of modes for growth rate and freqs
        try:
            full_growth_rate_norm = growth_rate_norm_SI(self.gam_SI, IMAS.Rref, IMAS.vth_ref)
        except:
            full_growth_rate_norm = growth_rate_norm_GB(self.gam_GB, self.Rmin, IMAS.Rref, RATIOS.vth_rat)

        try:
            full_frequency_norm = frequency_norm_SI(self.ome_SI, IMAS.Rref, IMAS.vth_ref)
        except:
            full_frequency_norm = frequency_norm_GB(self.ome_GB, self.Rmin, IMAS.Rref, RATIOS.vth_rat)

        for numsol in range(self.numsols):
            IMAS_quantities[f'growth_rate_norm_{numsol}'] = full_growth_rate_norm[:,numsol]
            IMAS_quantities[f'frequency_norm_{numsol}'] = full_frequency_norm[:,numsol]

        # loop over ions for ion quantities
        imas_density_mult = ion_type(self.typei)
        density_norm_ion = density_norm_i(self.normni) * imas_density_mult

        IMAS_quantities['collisionality_norm'] = np.sum(collisionality_norm(IMAS.Rref, IMAS.vth_ref, self.ne, density_norm_ion, self.Zi, RATIOS.qrat, self.Te,IMAS.vth_e))
        temperature_log_gradient_i = temperature_log_gradient(self.Ati, RATIOS.Rrat)


        # assign instability based on value of growth rate 0
        IMAS_quantities['instability'] = np.array([1 if gam0 > 0 else 0 for gam0 in IMAS_quantities['growth_rate_norm_0']])

        # output method defined by input dataset
        output_dicts = []

        # EDGE10D conversion specific
        if FASTER_dataset in ['Edge10D','Edge10D2']:

            particles_phi_potential_i = normalise_integrated_particle_flux(self.pfi_SI, self.ni, IMAS.vth_ref,
                                                                           IMAS.rho_star)
            energy_phi_potential_i = normalise_integrated_energy_flux(self.efi_SI, self.ni, IMAS.vth_ref, IMAS.rho_star,
                                                                      IMAS.Tref)
            momentum_tor_perpendicular_phi_potential_i = normalise_integrated_momentum_flux(self.vfi_SI, self.ni,
                                                                                            IMAS.vth_ref, IMAS.rho_star,
                                                                                            IMAS.mref, IMAS.Tref)
            IMAS_quantities['particles_phi_potential_e'] = normalise_integrated_particle_flux(self.pfe_SI, self.ne, IMAS.vth_ref, IMAS.rho_star),
            IMAS_quantities['energy_phi_potential_e'] = normalise_integrated_energy_flux(self.efe_SI, self.ne, IMAS.vth_ref, IMAS.rho_star, IMAS.Tref)
            for nion in range(self.nions):
                IMAS_quantities[f'density_norm_i_{nion}'] = density_norm_ion[nion]
                IMAS_quantities[f'temperature_log_gradient_i_{nion}'] = temperature_log_gradient_i[nion]
                IMAS_quantities[f'particles_phi_potential_i_{nion}'] = particles_phi_potential_i[nion]
                IMAS_quantities[f'energy_phi_potential_i_{nion}'] = energy_phi_potential_i[nion]
                IMAS_quantities[f'momentum_tor_perpendicular_phi_potential_i_{nion}'] = momentum_tor_perpendicular_phi_potential_i[nion]

            # set column names for dataset
            if df_to_append is not None:
                column_names_list = df_to_append.columns.values
            elif column_names_list is not None:
                pass
            else:
                column_names_list = ['density_norm_i_0', 'density_log_gradient_e',
                                    'temperature_log_gradient_e', 'temperature_log_gradient_i_0', 'q', 'magnetic_shear_r_minor',
                                    'collisionality_norm', 'binormal_component_norm', 'growth_rate_norm_0',
                                    'frequency_norm_0', 'growth_rate_norm_1', 'frequency_norm_1',
                                    'particles_phi_potential_e', 'energy_phi_potential_e', 'particles_phi_potential_i_0',
                                    'energy_phi_potential_i_0', 'momentum_tor_perpendicular_phi_potential_i_0',
                                    'particles_phi_potential_i_1', 'energy_phi_potential_i_1',
                                    'momentum_tor_perpendicular_phi_potential_i_1', 'irmodewidth', 'rrmodewidth', 'distan',
                                    'instability']

            # split the column names into wavevector columns and shared columns
            wv_cols = [name for name in column_names_list if np.shape(IMAS_quantities[name]) == (self.dimn,)]
            non_wv_cols = [name for name in column_names_list if name not in wv_cols]

            if FASTER_dataset == 'Edge10D':
                output_dicts = {name: [] for name in column_names_list}
                # fill non-wv fields
                for name in non_wv_cols:
                    output_dicts[name] = [IMAS_quantities[name] for x in range(self.dimn)]
                # iterate over wavevectors to append into dict lists
                for nk in range(self.dimn):
                    for name in wv_cols:
                        output_dicts[name].append(IMAS_quantities[name][nk])

            elif FASTER_dataset == 'Edge10D2':
                # prepare the shared dict
                dict_shared = {name:IMAS_quantities[name] for name in non_wv_cols}
                # iterate over wavevectors to create 1 dict per wavevector
                for nk in range(self.dimn):
                    wv_dict = dict_shared
                    for name in wv_cols:
                        wv_dict[name] = IMAS_quantities[name][nk]
                    output_dicts.append(wv_dict)



        elif FASTER_dataset == 'JET_QLK':
            # set column names for dataset
            if df_to_append is not None:
                column_names_list = df_to_append.columns.values
            elif column_names_list is not None:
                pass
            else:
                column_names_list = ['density_log_gradient_e',
                                     'temperature_log_gradient_e',
                                     'velocity_tor_norm',
                                     'velocity_tor_gradient_norm',
                                     'shearing_rate_norm',
                                     'r_minor_norm',
                                     'q',
                                     'magnetic_shear_r_minor',
                                     'charge_norm_i_1', # charge0 =  1, charge2 = 28
                                     'density_norm_i_0',
                                     'density_norm_i_1',
                                     'density_norm_i_2',
                                     'mass_norm_i_0',
                                     'mass_norm_i_1', #mass2 = 58
                                     'collisionality_norm',
                                     'temperature_norm', #ion temps all equal
                                     'temperature_log_gradient_i_0',
                                     'temperature_log_gradient_i_12', # same for species 1 and 2
                                     'density_log_gradient_i_0',
                                     'density_log_gradient_i_12', # same for species 1 and 2
                                     'pressure_gradient_norm',
                                     'binormal_component_norm',
                                     'growth_rate_norm_0',
                                     'frequency_norm_0',
                                     'growth_rate_norm_1',
                                     'frequency_norm_1',
                                     'instability']
            charge_norm_ion = charge_norm(self.Zi, RATIOS.qrat)
            mass_norm_ion = mass_norm(self.Ai, RATIOS.mrat)
            density_log_gradient_ion = density_log_gradient(self.Ani, RATIOS.Rrat)
            IMAS_quantities[f'temperature_norm'] = temperature_norm(self.Ti[0], self.Te)
            IMAS_quantities[f'density_log_gradient_i_0'] = density_log_gradient_ion[0]
            IMAS_quantities[f'density_log_gradient_i_12'] = density_log_gradient_ion[1]
            IMAS_quantities[f'temperature_log_gradient_i_0'] = temperature_log_gradient_i[0]
            IMAS_quantities[f'temperature_log_gradient_i_12'] = temperature_log_gradient_i[1]
            IMAS_quantities[f'pressure_gradient_norm'] = pressure_gradient_norm(self.alpha, self.q, RATIOS.Rrat, RATIOS.Brat)
            for nion in range(self.nions):
                IMAS_quantities[f'density_norm_i_{nion}'] = density_norm_ion[nion]
                IMAS_quantities[f'density_log_gradient_i_{nion}'] = density_log_gradient_ion[nion]
                IMAS_quantities[f'charge_norm_i_{nion}'] = charge_norm_ion[nion]
                IMAS_quantities[f'mass_norm_i_{nion}'] = mass_norm_ion[nion]

            IMAS_quantities['velocity_tor_norm'] = velocity_tor_norm(self.Machtor, self.cref, IMAS.vth_ref, RATIOS.Rrat)
            IMAS_quantities['velocity_tor_gradient_norm'] = velocity_tor_gradient_norm(self.Autor, self.cref, IMAS.vth_ref, RATIOS.Rrat)
            IMAS_quantities['shearing_rate_norm'] = shearing_rate_norm(self.gammaE, self.cref, IMAS.vth_ref, RATIOS.Rrat)
            IMAS_quantities['r_minor_norm'] = r_minor_norm(self.Rmin, self.x, self.Ro)

            # check
            # print('Missing::', [name for name in column_names_list if name not in IMAS_quantities.keys()])
            # print('Unused::', [name for name in IMAS_quantities.keys() if name not in column_names_list])
            # split the column names into wavevector columns and shared columns
            wv_cols = [name for name in column_names_list if np.shape(IMAS_quantities[name]) == (self.dimn,)]
            non_wv_cols = [name for name in column_names_list if name not in wv_cols]

            if output == 'split_df':

                input_dicts = {}
                output_dicts = {name: [] for name in wv_cols}
                # fill non-wv fields
                for name in non_wv_cols:
                    input_dicts[name] = [IMAS_quantities[name]]
                # iterate over wavevectors to append into dict lists
                for nk in range(self.dimn):
                    for name in wv_cols:
                        output_dicts[name].append(IMAS_quantities[name][nk])

                df_out = pd.DataFrame(output_dicts)
                df_inp = pd.DataFrame(input_dicts)
                return df_inp, df_out
            else:
                output_dicts = {name: [] for name in column_names_list}
                # fill non-wv fields
                for name in non_wv_cols:
                    output_dicts[name] = [IMAS_quantities[name] for x in range(self.dimn)]
                # iterate over wavevectors to append into dict lists
                for nk in range(self.dimn):
                    for name in wv_cols:
                        output_dicts[name].append(IMAS_quantities[name][nk])
        elif FASTER_dataset == 'JET GKW':
            pass
        else:
            pass

        # final output

        if output =='dict':
            return output_dicts
        elif output == 'df':
            df_out = pd.DataFrame(output_dicts)
            return df_out
        elif output == "both":
            df_out = pd.DataFrame(output_dicts)
            return df_out, output_dicts


    

    def run_bash(self, qlk_path, run_path, run_name, CPU = 32):
        # cwd = os.getcwd() # give the folder of the workaspace (and not the script)

        run_dir = str(os.path.join(run_path,run_name))
        run_dir = os.path.expanduser(run_dir)

        # Change the current work path to the run folder path 
        os.chdir(run_dir)
        self.set_storage(qlk_path, run_path, run_name)
        self.write_input(run_path, run_path)
        os.system(f'mpiexec -np {CPU} ./QuaLiKiz')
        self.load_rundfolder
        # Come back to the previous work path
        os.chdir(os.path.abspath(os.path.dirname(__file__)))

    def run_QLK(self, name, runsdir, qlk_root_path, template_path='./default_parameters.json'):
        '''
        runs QLK in the specified directory using the current class as input. Creates .nc file with collated results.
        :param runs_relpath:
        :param run_name:
        :param qlk_relpath:
        :param overwrite:
        :return:
        '''
        cwd = os.getcwd()
        qlk_plan = self.to_QLK_plan(template_path=template_path)

        # TODO: fix so that path can have spacebar
        qlk_binpath = os.path.join(qlk_root_path, "QuaLiKiz")
        run_dir = str(os.path.join(runsdir,name))
        binreldir = Path(relpath(qlk_binpath, start=run_dir))


        run = Run(runsdir, name, binreldir, qualikiz_plan=qlk_plan)
        runlist = [run]

        batch = Batch(runsdir, name, runlist)

        batch.prepare()
        os.chdir(run_dir)
        batch.generate_input()

        batch.launch()

        batch.to_netcdf()

        # run_to_netcdf(run_dir)

        os.chdir(cwd)


    def update(self, **kwargs):# ???
        """Used to update the existing class from a dict. Useful for cases where not all the values of the class change for faster processing but ultimately not very useful unless dealing with large datasets"""
        for key, value in kwargs.items():
            setattr(self, key, value)


    


    def compare(self,c2,mode='default'):
        """used to compare this current QLK class to a different QLK class (c2) for testing purposes"""

        if mode == 'IMAS_check':
            IMAS_fields = ['x','Rmin','Aupar','Machpar', 'danisdr','source']
            fields1 = [field[0] for field in inspect.getmembers(self) if ((not field[0].startswith('_') and not inspect.ismethod(field[1])) and (field[0] not in IMAS_fields))]
            fields2 = [field[0] for field in inspect.getmembers(c2) if ((not field[0].startswith('_') and not inspect.ismethod(field[1])) and (field[0] not in IMAS_fields))]
        else:
            fields1 = [field[0] for field in inspect.getmembers(self) if (not field[0].startswith('_') and not inspect.ismethod(field[1]))]
            fields2 = [field[0] for field in inspect.getmembers(c2) if (not field[0].startswith('_') and not inspect.ismethod(field[1]))]
        print(fields1)
        print(fields2)
        missing_fields = [field for field in fields2 if field not in fields1]
        extra_fields = [field for field in fields1 if field not in fields2]
        same_fields = [field for field in fields1 if field in fields2]
        different_entries = []
        for field_name in same_fields:
                    v1 = getattr(self, field_name)
                    v2 = getattr(c2, field_name)
                    try:
                        if not np.array_equal(v1, v2):
                            print(f'{field_name} 1: {np.shape(v1)} {type(v1)}\n{v1}')
                            print(f'{field_name} 2: {np.shape(v2)} {type(v2)}\n{v2}')
                            different_entries.append(field_name)
                    except:
                        if not np.all(v1 == v2):
                            print(f'{field_name} 1: {np.shape(v1)} {type(v1)}\n{v1}')
                            print(f'{field_name} 2: {np.shape(v2)} {type(v2)}\n{v2}')
                            different_entries.append(field_name)
        print(f'different_entries: {different_entries}')
        print(f'missing_fields: {missing_fields}')
        print(f'extra_fields: {extra_fields}')
        return different_entries, missing_fields, extra_fields



def r_minor_norm(Rmin, x, Ro):
    """
    Calculates the IMAS normalised minor radius r_minor_norm given the following qualikiz inputs:
    :param Rmin: array or float of  QLK minor radius of LCS (ds.Rmin.values)
    :param x: array or float of  QLK normalised minor radius of flux surface (ds.x.values)
    :param Ro: array or float of QLK major radius (ds.Ro.values)

    :returns: array or float of IMAS minor radius normalised to major radius
    """
    return Rmin * x / Ro


def b_field_tor_sign():
    """
    Calculates the sign of the IMAS toroidal field direction:
    Always returns 1 for QLK
    """
    return 1


def ip_sign():
    """
    Calculates the sign of the IMAS toroidal current direction:
    Always returns 1 for QLK
    """
    return 1


def q_IMAS(q):
    """
    Calculates the IMAS safety factor:
    :param q: float or array of the qlk safety factor (ds.q.values)
    :return: float or array of the IMAS normalised safety factor
    """
    return np.float64(q)


def magnetic_shear_r_minor(smag):
    """
    Calculates the IMAS magnetic shear given the following qualikiz inputs:
    :param smag: array or float of QLK magnetic shear (ds.smag.values)

    :return: IMAS normalised magnetic shear
    """
    return np.float64(smag)


def pressure_gradient_norm(alpha, q, Rrat, Brat):
    """
    Calculates the IMAS normalised pressure gradient (pressure_gradient_norm) given the following qualikiz inputs:
    :param alpha: array or float of QLK MHD alpha (ds.alpha.values)
    :param q: array or float of  QLK safety factor (ds.q.values)
    :param Rrat: float of QLK:IMAS ratio of reference lengths
    :param Rrat: float of QLK:IMAS ratio of reference magnetic fields

    :returns: array or float of IMAS normalised pressure gradient
    """

    pres_grad_norm = alpha * Brat ** 2 / q ** 2 / Rrat
    return pres_grad_norm


def dgeometric_axis_r_dr_minor(alpha):
    """
    Calculates the IMAS derivative of R_0 wrt. r at r=r_0 given the following qualikiz inputs:
    :param alpha: array or float of QLK MHD alpha (ds.alpha.values)

    :return: array or float of IMAS derivative of R_0 wrt. r
    """
    return np.float64(-alpha)


def dgeometric_axis_z_dr_minor():
    """
    Returns the IMAS derivative of z wrt. r at r=r_0 which is always 0 for QLK

    :return: always 0 for QLK
    """
    return np.float64(0)


def elongation():
    """
    Returns the IMAS elongation which is always 1 for QLK

    :return: always 1 for QLK
    """
    return np.float64(1)


def delongation_dr_minor_norm():
    """
    Calculates the IMAS derivative of elongation wrt. r at r=r_0 which is always 0 for QLK

    :return: always 0 for QLK
    """
    return np.float64(0)


def shape_coefficients_c():
    """
    Calculates the IMAS MXH cosine shape coefficients which are always 0 for QLK

    :return: always 0 for QLK
    """
    return np.array([0], dtype=np.float64)


def shape_coefficients_s():
    """
    Calculates the IMAS MXH sine shape coefficients which are always 0 for QLK

    :return: always 0 for QLK
    """
    return np.array([0], dtype=np.float64)


def dc_dr_minor_norm():
    """
    Calculates the IMAS derivative of IMAS MXH cosine shape coefficients wrt. r at r=r_0 which is always 0 for QLK

    :return: always 0 for QLK
    """
    return np.array([0], dtype=np.float64)


def ds_dr_minor_norm():
    """
    Calculates the IMAS derivative of IMAS MXH sine shape coefficients wrt. r at r=r_0 which is always 0 for QLK

    :return: always 0 for QLK
    """
    return np.array([0], dtype=np.float64)


def charge_norm(Zi_QLK, q_rat):
    """
    Calculates the IMAS normalised charge of an ion species given the following inputs:
    Zi_QLK: array of the ion species charge number from QLK
    q_rat: float of the ratio of QLK:IMAS reference charge (ratioclass.qrat)
    """
    return Zi_QLK * q_rat


def mass_norm(Ai_QLK, m_rat):
    """
    Calculates the IMAS normalised charge of a species given the following inputs:
    Ai_QLK: ds.Ai, the species mass number from QLK
    m_rat: the ratio of QLK:IMAS reference masses (ratioclass.mrat)
    """
    print(Ai_QLK)
    print(m_rat)
    return Ai_QLK * m_rat


def density_norm_e(ne_QLK):
    """
    Calculates the normalised IMAS electron density. Should always be 1
    :param ne_QLK: float or array of the electron density from QLK (ds.ne.values)
    :return: same dimension array as ne, filled with 1s
    """
    if np.isscalar(ne_QLK):
        return np.float64(1)
    else:
        return np.ones_like(ne_QLK, np.float64)


def density_norm_i(normni_QLK):
    """
    Calculates the normalised IMAS ion density. Should always be the values from ds.normni
    but needs to be split into one value for each ion species
    :param normni_QLK: array or float of QLK normalised ion densities ds.normni.values
    :return: input
    """
    return normni_QLK


def density_log_gradient(Ans_QLK, R_rat):
    """
    Calculates the IMAS normalised charge of a species given the following inputs:
    :param Ans_QLK: float or array of the species normalised logarithmic temperature gradient from QLK (ds.Ani.values / ds.Ane.values)
    :param L_rat: float of the ratio of QLK:IMAS reference masses (RATIOS.Rrat)

    :return: IMAS normalised logarithmic temperature gradient
    """
    Ans = Ans_QLK / R_rat
    return Ans


def temperature_norm(Ts_QLK, Te_QLK):
    """
    Calculates the IMAS normalised temperature of a species given the following inputs:
    Ts_QLK: float or array of (ds.Te.values or ds.Ti.values) the species temperature from QLK
    Te_QLK: float of (ds.Te.values) the electron temperature from QLK
    :return: normalised temperature, Ts / Te
    """
    T_norm = Ts_QLK / Te_QLK
    return T_norm


def temperature_log_gradient(Ats_QLK, R_rat):
    """
    Calculates the IMAS normalised temperature gradient of a species given the following inputs:
    Ans_QLK: ds.Ati/ds.Ate the species normalised temperature gradient from QLK
    R_rat: the ratio of QLK:IMAS reference lengths (ratioclass.Lrat)

    strictly speaking, should be Ans_QLK/L_rat but since L_rat is 1 (for now) it just saves processing time
    """
    return Ats_QLK / R_rat


def velocity_tor_norm(Machtor, cref_QLK, vth_ref_IMAS, Rrat):
    """
    Calculates the IMAS normalised toroidal veloctiy given the following qualikiz inputs:

    :param Machtor: array or float of toroidal mach number from QLK files (ds.Machtor.values)
    :param cref_QLK: float of normalising velocity for QLK normalisations (QLK.cref)
    :param vth_ref_IMAS: float of IMAS reference thermal velocity (IMAS.vth_ref)
    :param Rrat: float of ratio of QLK to IMAS major radius (RATIOS.Rrat)
    :return: IMAS normalised toroidal velocity
    """
    vel_tor_norm = Machtor * cref_QLK / (vth_ref_IMAS * Rrat)
    return vel_tor_norm


def velocity_tor_gradient_norm(Autor_QLK, cref_QLK, vth_ref_IMAS, Rrat_ratio):
    """
    Calculates the IMAS normalised toroidal veloctiy gradient (or parallel flow shear) given the following qualikiz inputs:
    :param Autor_QLK: array or float of the toroidal velocity gradient from QLK files (ds.Autor.values)
    :param cref_QLK: float of normalising velocity for QLK normalisations  (QLK.cref)
    :param vth_ref_IMAS: float of IMAS reference thermal velocity (IMAS.vth_ref)
    :param Rrat:  float of ratio of QLK to IMAS major radius (RATIOS.Rrat)
    :return: IMAS normalised toroidal velocity gradient
    """
    vel_tor_gradient_norm = cref_QLK * Autor_QLK / (vth_ref_IMAS * Rrat_ratio ** 2)
    return vel_tor_gradient_norm


def shearing_rate_norm(gammE_QLK, cref_QLK, vth_ref_IMAS, Rrat):
    """
    Calculates the IMAS normalised perpendicular flow shear given the following qualikiz inputs:

    :param gammE_QLK: array or float of (ds.gammaE.values) the QLK gammaE data
    :param cref_QLK:  array or float of (QLK.cref) QLK reference velocity for normalising velocities
    :param vth_ref_IMAS: array or float of (IMAS.vth_ref) IMAS thermal reference velocity
    :param Rrat: array or float of (RATIOS.Rrat) ratio of QLK to IMAS R (=1)
    :return: IMAS normalised perpendicular flow shear
    """
    shear_rate_norm = gammE_QLK * cref_QLK / (vth_ref_IMAS * Rrat)
    return shear_rate_norm


def beta_reference():
    """
    beta reference is set to 0 for electrostatic codes like QLk
    """
    return np.float64(0)


def debye_length_reference():
    """
    debye length is assumed to be 0 for simplifications to hold
    """
    return np.float64(0)


def radial_component_norm(kthetarho_QLK=None):
    """
    radial wavevector is set to 0
    """
    if kthetarho_QLK is None:
        return 0
    else:
        return np.zeros_like(kthetarho_QLK, dtype=np.float64)


def binormal_component_norm(rho_rat, kthetarho_QLK):
    """
    Calculates the IMAS binormal wavevector given the following inputs:

    :param kthetarho_QLK: float or array of the wavenumber from QLK (ds.kthetarhos.values)
    :param rho_rat: float of (RATIOS.rho_rat) the ratio of larmor radii of QLK to IMAS :return: IMAS binormal wavevector
    """
    if np.isscalar(rho_rat):
        kthetas = kthetarho_QLK / rho_rat
    else:
        kthetas = np.array([kthetarho_QLK / rho for rho in rho_rat])
    return kthetas


def collisionality_norm(IMAS_Rref, IMAS_vth_ref, QLK_n_e, QLK_n_b, QLK_Z_b, q_rat, QLK_t_e, IMAS_vth_a):
    """
    Calculates the IMAS normalised collisionality (only valid for electrons onto ions for QLK?). Splits the ion density
    array and calculates the collisionality for each ion type separately before summing. Uses the following inputs:
    IMAS_Rref: IMAS reference length (IMAS.Rref)
    IMAS_vth_ref: IMAS reference thermal velocity (IMAS.vth_ref)
    QLK_n_e: float or array of  qualikiz electron density in units of 1e19 (ds.ne.values)
    QLK_n_b: float or array of normalised qualikiz density of target ion (SI), (ds.normni.values)
    QLK_Z_b: float or array of charge number of target ion (ds.Zi.values)
    q_rat: float of QLK:IMAS refernce charge ratio
    QLK_t_e: float or array of QLK electron temperature (ds.Te.values)
    IMAS_vth_a: thermal velocity of incident particles i.e. electrons (IMAS.vth_e)

    returns: sum of the collisionality arrays for each ion
    """

    def coulomblog(QLK_ne, QLK_Te):
        coulog = np.float64(15.2 - 0.5 * np.log(QLK_ne / 10) + np.log(QLK_Te))
        return coulog

    # convert qualikiz normalised ion density to SI
    n_b = QLK_n_b * QLK_n_e * np.float64(1e19)
    Z_a = np.float64(-1) * q_rat  # incident particles are hardcoded to electrons in QLK
    Z_b = QLK_Z_b * q_rat
    e = scipy.constants.physical_constants['electron volt'][0]
    e_0 = scipy.constants.physical_constants['vacuum electric permittivity'][0]
    m_a = scipy.constants.physical_constants['electron mass'][0] # incident particles are hardcoded to electrons in QLK
    coulomblog = coulomblog(QLK_n_e, QLK_t_e)
    collated_constants = Z_a ** 2 * e ** 4 / (4 * np.pi * e_0 ** 2 * m_a ** 2)
    collisionality = collated_constants * IMAS_Rref * n_b * (Z_b ** 2) * coulomblog / (IMAS_vth_ref * (IMAS_vth_a ** 3))
    return collisionality


def include_centrifugal_effects(rot_flag_QLK, x_QLK):
    """
    returns if centrifugal effects are taken into account in QLK.
    todo: check with someone the correct equality sign (currently assumed and would make a difference for runs at x=0.5)
    """
    if rot_flag_QLK == 0 or (rot_flag_QLK == 2 and x_QLK < 0.5):
        return 0
    elif rot_flag_QLK == 1 or (rot_flag_QLK == 2 and x_QLK >= 0.5):
        return 1


def include_a_field_parallel():
    return 0


def include_b_field_parallel():
    return 0


def include_full_curvature_drift():
    return 0


def collisions_pitch_only():
    """return true for QLK"""
    return 1


def collisions_momentum_conservation():
    return 0


def collisions_energy_conservation():
    return 0


def collisions_finite_larmor_radius():
    return 0


def growth_rate_norm_GB(gamma_GB_QLK, Rmin_QLK, Rref_IMAS, vth_rat_RATIO):
    """
    Calculates the IMAS normalised growth rate of a mode given the following inputs:
    gamma_GB_QLK: float or array of (ds.gam_GB.values)  GyroBohm normalised growth rate from QLK
    Rmin_QLK: float or array of (ds.Rmin.values) QLK minor radius of LCS
    Rref_IMAS: (IMAS.Rref) IMAS reference length for the normalisation
    vth_rat_RATIO: (RATIOS.vth_rat) ratio of QLK:IMAS thermal velocities
    """
    print(gamma_GB_QLK)
    print(Rref_IMAS)
    print(vth_rat_RATIO)
    print(Rmin_QLK)
    return gamma_GB_QLK * Rref_IMAS * vth_rat_RATIO / Rmin_QLK



def growth_rate_norm_SI(gamma_SI_QLK, Rref_IMAS, vth_ref_IMAS):
    """
    Calculates the IMAS normalised growth rate of a mode given the following inputs:
    gamma_SI_QLK: float or array of (ds.gam_SI.values)  SI growth rate from QLK
    Rref_IMAS: (IMAS.Rref) IMAS reference length for the normalisation
    vth_ref_IMAS: (IMAS.vth_ref) IMAS reference thermal velocity
    """
    return gamma_SI_QLK * Rref_IMAS / vth_ref_IMAS



def frequency_norm_GB(omega_GB_QLK, Rmin_QLK, Rref_IMAS, vth_rat_RATIO):
    """
    Calculates the IMAS normalised frequency of a mode given the following inputs:
    omega_GB_QLK: float or array of (ds.ome_GB.values) GyroBohm normalised frequency from QLK
    Rmin_QLK: (ds.Rmin) QLK minor radius of LCS
    Rref_IMAS: (IMAS.Rref) IMAS reference length for the normalisation
    vth_rat_RATIO: (RATIOS.vth_rat) ratio of QLK:IMAS thermal velocities
    """
    return -omega_GB_QLK * Rref_IMAS * vth_rat_RATIO / Rmin_QLK


def frequency_norm_SI(omega_SI_QLK, Rref_IMAS, vth_ref_IMAS):
    """
    Calculates the IMAS normalised frequency of a mode given the following inputs:
    omega_SI_QLK: float or array of (ds.ome_SI.values) SI normalised frequency from QLK
    Rref_IMAS: (IMAS.Rref) IMAS reference length for the normalisation
    vth_ref_IMAS: (IMAS.vth_ref) IMAS reference thermal velocity
    """
    return -omega_SI_QLK * Rref_IMAS / vth_ref_IMAS


def phi_potential_perturbed_norm_nonlinear():
    """
    Calculates the IMAS normalised perturbed potential for a nonlinear simulation given the following inputs:
    TODO: uneeded for our purposes, need for QLK.nc generic file conversion? not complete in doc yet
    """
    return


def phi_potential_perturbed_norm_linear(theta, rmodewidth, rmodeshift, d, phi_0=1,test=False):
    """
    Calculates the IMAS normalised perturbed potential for a linear simulation given the following inputs:

    :param theta: theta array over which to integrate/calculate potential
    :param w: rmodewidth from QLK. (Complex number made up of ds.irmodewidth and ds.rrmodewidth)
    :param x0: rmodeshift from QLK. (Complex number made up of ds.irmodeshift and ds.rrmodeshift)
    :param d: ds.distan, another scaling factor for the gaussian width scaling inversely to w. Represents the distance between rational flux surfaces
    :return: the normalised IMAS potential over the theta grid
    """

    exp1 = np.exp(-rmodewidth**2*theta**2/(2 * d**2))
    exp2 = np.exp(1j * theta * rmodeshift / d)
    phi_hat_N = exp1 * exp2

    phi_hat_abs_sq = np.abs(phi_hat_N) ** 2

    Af = np.sqrt(1 / (2*np.pi) * np.trapz(phi_hat_abs_sq, theta))

    max_index = np.argmax(np.abs(phi_hat_N))
    phi_hat_N_max = np.abs(phi_hat_N)[max_index]

    e_i_alpha = phi_hat_N_max / phi_hat_N[max_index]

    phi_Nf = phi_hat_N * e_i_alpha / Af

    if test is True:
        plt.plot(theta, np.real(phi_hat_N), label='Re')
        plt.plot(theta, np.imag(phi_hat_N), label='Im')
        plt.plot(theta, np.abs(phi_hat_N), label='Abs')
        plt.legend()
        plt.show()

        plt.plot(theta, phi_Nf,label='Nf')
        plt.legend()
        plt.show()
    return phi_Nf, Af


def phi_potential_perturbed_norm_linear2(theta, w, x0, d, recip_rho_star, q_ref, T_ref, phi_0=1,test=False):
    """
    WIP to avoid underflow errors, circumvented by smarter coding (in theory) for now

    Calculates the IMAS normalised perturbed potential for a linear simulation given the following inputs:

    :param theta: theta array over which to integrate/calculate potential
    :param w: rmodewidth from QLK. (Complex number made up of ds.irmodewidth and ds.rrmodewidth)
    :param x0: rmodeshift from QLK. (Complex number made up of ds.irmodeshift and ds.rrmodeshift)
    :param d: ds.distan, another scaling factor for the gaussian width scaling inversely to w. Represents the distance between rational flux surfaces
    :param phi_0: normalising factor, wont affect the result
    :param recip_rho_star: reciprocal of rho_star, the normalised IMAS reference larmor radius
    :param q_ref: reference IMAS q
    :param T_ref: reference IMAS Temperature
    :return: the normalised IMAS potential over the theta grid
    """

    def try_exp(x):
        try:
            return np.exp(x)
        except:
            return 0

    exp_factor1 = -w**2*theta**2/(2 * d**2)
    exp1 = try_exp(exp_factor1)
    exp_factor2 = 1j * theta * x0 / d
    exp2 = try_exp(exp_factor2)

    def try_mult(a,b):
        try:
            return a*b
        except:
            return 0

    phi_hat_N = try_mult(exp1,exp2)

    plt.plot(theta, np.real(phi_hat_N),label='Re')
    plt.plot(theta, np.imag(phi_hat_N),label='Im')
    plt.plot(theta, np.abs(phi_hat_N),label='Abs')
    plt.legend()
    plt.show()

    def try_abs_sq(x):
        try:
            return np.abs(x)**2
        except:
            return 0
    phi_hat_abs_sq = try_abs_sq(phi_hat_N)

    def try_af(x, theta):
        try:
            return np.sqrt(1 / (2*np.pi) * np.trapz(phi_hat_abs_sq, theta))
        except: return 0

    Af = np.sqrt(1 / (2*np.pi) * np.trapz(phi_hat_abs_sq, theta))
    # Af2 = try_af(phi_hat_abs_sq, theta)

    max_index = np.argmax(np.abs(phi_hat_N))
    phi_hat_N_max = np.abs(phi_hat_N)[max_index]

    e_i_alpha = phi_hat_N_max / phi_hat_N[max_index]

    phi_Nf = phi_hat_N * e_i_alpha / Af

    if test is True:
        plt.plot(theta, phi_Nf,label='Nf')
        plt.legend()
        plt.show()


    return phi_Nf, Af


def normalise_integrated_particle_flux(particle_flux, n_s, v_thref, rho_star, Af=1):
    '''
    Normalises a QLK particle flux to IMAS standard.
    :param particle_flux: float or array of QLK particle flux (ds.pfe_SI.values or ds.pfi_SI.values)
    :param n_s: float or array of particle density (ds.ne.values or ds.normni.values * ds.ne.values) in units of 1e19
    :param v_thref: float of IMAS v_thref
    :param rho_star: float of IMAS rhostar (reference larmor radius normalised to reactor Lref)
    :param Af: Potential normalisation factor: 1 if non-linear, otherwise calculated from phi_potential_perturbed_norm_linear()

    :return: IMAS normalised particle flux
    '''
    denom = Af ** 2 * n_s * np.float64(1e19) * (v_thref * rho_star) ** 2
    particle_flux_norm = np.divide(particle_flux, denom, out=np.zeros_like(denom), where=denom != 0)
    return particle_flux_norm


def normalise_integrated_energy_flux(energy_flux, n_s, v_thref, rho_star, T_ref, Af=1):
    '''
    Normalises a QLK particle flux to IMAS standard.
    :param energy_flux: float or array of QLK energy flux (ds.efe_SI.values or ds.efi_SI.values)
    :param n_s: float or array of particle density (ds.ne.values or ds.normni.values * ds.ne.values) in units of 1e19
    :param v_thref: float of IMAS v_thref
    :param rho_star: float of IMAS rhostar (reference larmor radius normalised to reactor Lref)
    :param T_ref: float of IMAS reference temperature (IMAS.Tref)
    :param Af: Potential normalisation factor: 1 if non-linear, otherwise calculated from phi_potential_perturbed_norm_linear()

    :return: IMAS normalised energy flux
    '''
    denom = Af ** 2 * n_s * np.float64(1e19) * T_ref * v_thref * rho_star ** 2
    energy_flux_norm = np.divide(energy_flux, denom, out=np.zeros_like(denom), where=denom != 0)
    return energy_flux_norm


def normalise_integrated_momentum_flux(momentum_flux, n_s, v_thref, rho_star, m_ref, L_ref, Af=1):
    '''
    Normalises a QLK particle flux to IMAS standard.
    :param momentum_flux: float or array of QLK momentum flux (ds.vfe_SI.values or ds.vfi_SI.values)
    :param n_s: float or array of particle density (ds.ne.values or ds.normni.values * ds.ne.values) in units of 1e19
    :param v_thref: float of IMAS v_thref
    :param rho_star: float of IMAS rhostar (reference larmor radius normalised to reactor Lref)
    :param m_ref: float of IMAS reference mass (IMAS.mref)
    :param L_ref: float of IMAS reference major radius (IMAS.Rref)
    :param Af: Potential normalisation factor: 1 if non-linear, otherwise calculated from phi_potential_perturbed_norm_linear()

    :return: IMAS normalised energy flux
    '''
    denom = Af ** 2 * n_s * np.float64(1e19) * m_ref * L_ref * (v_thref * rho_star) ** 2
    # conditon for dealing with 0 density species, sets outputs of division to 0 if denom = 0
    momentum_flux_norm = np.divide(momentum_flux, denom, out=np.zeros_like(denom), where=denom!=0)
    return momentum_flux_norm


def electron_type(etype):
    # converts from QLK electron type (1: active, 2: adiabatic, 3: adiabatic passing at ion scales) to IMAS electron type (0: active, 1: adiabatic)
    if etype == 1:
        return 0
    elif etype == 2:
        return 1
    elif etype == 3:
        print('electron type not supported')
        return 'electron type not supported'
    else:
        print(f'electron type not recognised: {etype, type(etype)}')
        return f'electron type not recognised: {etype, type(etype)}'


def ion_type(itypes):
    imas_density_mult = []
    print(itypes)
    for itype in itypes:
        if itype == 1:
            imas_density_mult.append(1)
        elif itype == 3:
            imas_density_mult.append(0)
        elif itype == 2:
            print(f'Adiabatic ions are incompatible with IMAS')
            return
        elif itype == 4:
            print(f'Type 4 ions are incompatible with IMAS')
            return
        else:
            print(f'"{itype}" Ion type not recognised')
    return imas_density_mult

def poloidal_angle_pturns(w, d, x0, res=32, domain_mult=4, max_rot=6,test=False):
    """
    function to create a theta grid 4 FWHMs (maximum of 6pi, should never be reached due to strong ballooning) around the
    center of the potential, with a given resolution.
    :param w: ds.rrmodewidth + ds.irmodewidth
    :param x0: ds.rrmodeshift + ds.irmodeshift
    :param d: ds.distan
    :param res: resolution in points per FWHM (default of 4 FWHM plotted)
    :param domain_mult: controls number of FWHM to plot (recommend leaving default or smaller to avoid underflow errors)
    :param max_rot: maximum number of full rotations for theta, default to 6 rotations, should be more than big enough

    :return: poloidal angle grid
    """
    if res % 2 == 1:
        print('odd resolution detected, recommend using even numbers to include peak values')

    a = np.abs(w) ** 2 / (2 * d **2)
    b = np.imag(x0) / d
    center = -b / (2*a)

    fwhm = d * np.sqrt(2 * np.log(2) / (np.real(w)**2 - np.imag(w)**2))
    domain_lim = max_rot * np.pi
    domain = min(domain_mult * fwhm, domain_lim)
    edges = (center - domain, center + domain)
    pol_turns = domain / np.pi
    int_pol_turns = int(np.ceil(pol_turns))
    if domain < domain_lim:
        pol_ang = np.linspace(edges[0], edges[1], (domain_mult * res) + 1)
    else:
        pol_ang = np.linspace(edges[0], edges[1], (domain_lim / (domain_mult * fwhm) * res) + 1)
    if test is True:
        print(w,x0)
        plt.axvline(center)
        y = np.exp(-w**2 * pol_ang**2/(2*d**2)) * np.exp(1j*x0/d * pol_ang)
        plt.plot(pol_ang, np.real(y))
        plt.plot(pol_ang, np.imag(y))
        plt.plot(pol_ang, np.abs(y))
        edges = ((center - fwhm * (x + 1), center + fwhm * (x+1)) for x in reversed(range(domain_mult)))
        cols = ('r','g','b','y')
        for n, mult in enumerate(edges):
            plt.hlines(0.5, mult[0], mult[1], colors=cols[n])
        plt.show()
    return pol_ang, int_pol_turns

# qlk_path = '~/Documents/These/QuaLiKiz'
# run_path = '~/Documents/These/QuaLiKiz/runs'
# run_name = 'my_run'
# provider = 'Enzo'
# obj = QLK_class()
# obj.set_storage(qlk_path, run_path, run_name)
# obj.write_input(run_path, run_name)
# obj.run_bash(run_path, run_name, CPU = 32)
# obj.load_runfolder(run_path, run_name)
# IDS = obj.to_IMAS_ids(provider)
# print(IDS.linear.wavevector[-1].eigenmode[0].growth_rate_norm)