'''
FINAL VERSION
'''

import numpy as np
#useful for the matlab code (Fevre)
import subprocess
import io
import os
# import codata #useful for international values (used for IMAS conversion) like the electron charge, the electric permittivity...
from scipy.constants import epsilon_0
# interpolation functions
from scipy.interpolate import interp1d
#from GKIDS git
import sys
sys.path.append("/Home/EV273809/Documents/These/Python/imas-gk/gkids/tools")
import FS_param #to use geometric functions from Yann (to have cs, ss... extended miller param)
from idspy_dictionaries import ids_gyrokinetics_local
sys.path.append('/Home/EV273809/Documents/These/Python/imas-gk/idspy_toolkit/idspy_toolkit')
import idspy_toolkit

# for debug
import matplotlib.pyplot as plt

class IMASgkids():
    def __init__(self, user_time = 8, rayon_norm = 0.5, t_ignitron = None, core_profiles = None, equilibrium = None, fevre_computation = False):
        
        # TO ADD : RAISE ERROR IF ONE OF THE ARGS IS MISSING

        self.erreur_fevre = False
        self.fevre_computation = fevre_computation

        self.imas_data_to_gkids_dico(user_time, rayon_norm, t_ignitron, core_profiles, equilibrium) 
        self.conversion_dico_to_gkids()
       
    #####_______________________________________________#####
    #####______________Utilitary functions______________#####
    #####_______________________________________________#####

    # fonction qui retourne l'indice d'une liste correspondant Ã  la valeur la plus proche de la valeur en entrÃ©e
    def get_closest_index(self, value, input_list):
        # "RÃ©cupÃ¨re l'indice le plus proche d'une valeur dans une liste stictement croissante."
        lst = np.abs(np.array(input_list)-value)
        min = np.min(lst)
        
        return np.where(lst == min)[0][0]
    
    
    #####_______________________________________________#####
    #####_________________Main function_________________#####
    #####_______________________________________________#####

    
    #reception of data from the IMAS database
    #Data stucture: 1D : time or r
    #               2D : r x time
    def imas_data_to_gkids_dico(self, user_time, rayon_norm, t_ignitron, core_profiles, equilibrium):
        
        # t_ignitron= pw.tsbase(shot_number, 'RIGNITRON')[0][0][0]
        
        
        t_equi = equilibrium.time - t_ignitron
        ind_t_equi = self.get_closest_index(user_time, t_equi)

        ### ---------------- ###
        ### RADIAL TREATMENT ###
        ### ---------------- ###

        #for the different miller extended parameters
        Rg = equilibrium.time_slice[ind_t_equi].profiles_2d[0].r # I take the index  for profiles_2d but I don't know...
        Zg = equilibrium.time_slice[ind_t_equi].profiles_2d[0].z
        psi = equilibrium.time_slice[ind_t_equi].profiles_2d[0].psi

        psi_ax = equilibrium.time_slice[ind_t_equi].global_quantities.psi_axis
        psi_sep = equilibrium. time_slice[ind_t_equi].global_quantities.psi_boundary # check if it is the correct def
        #psi_sep = equilbirum.time_slices[ind_t_equi].boundary_separatrix.psi ???

        Rg = Rg[:,0]
        Zg = Zg[0,:]
        
        PSIN = (psi - psi_ax)/(psi_sep - psi_ax)
        psin_resolution_gkids = np.linspace(0.05, 0.90, 80)
        R_x_point, Z_x_point = None, None

        R, Z = FS_param.psi2rz(Rg, Zg, PSIN, psiN_out=psin_resolution_gkids, R_lcfs = R_x_point, Z_lcfs = Z_x_point) # Npsi (= 101) x Nth (=35)
        r_min, r_max = [], []
        for rr in range(len(psin_resolution_gkids)):
            r_min.append(np.min(R[rr,:]))
            r_max.append(np.max(R[rr,:]))

        r_min = np.array(r_min)
        r_max = np.array(r_max)
        r_gkids = (r_max-r_min)/2 # 80
        R_gkids = (r_max+r_min)/2

        
        
        ind_r_gkids = self.get_closest_index(rayon_norm, r_gkids/r_gkids[-1]) 
        # print("ind_r_gkids=", ind_r_gkids)

        ### ---------------- ###
        ###  DATA RETRIEVAL  ###
        ### ---------------- ###
        #core_profile_imas_public: Useful pour Te, ne, t, rho_tor_norm
        t_core = core_profiles.time - t_ignitron
        ind_t_core = self.get_closest_index(user_time, t_core)
        #core_profile_imas_simu : Useful pour Ti
        z_eff_res_core = core_profiles.global_quantities.z_eff_resistive
        z_eff_res_core = z_eff_res_core[ind_t_core]

        nbr_ions = len(core_profiles.profiles_1d[ind_t_core].ion)
        nbr_ions_dens_null = 0
        Ti_core = []
        for jj in range(nbr_ions):
            ni_ =core_profiles.profiles_1d[ind_t_core].ion[jj].density
            if (ni_[ind_r_gkids]==0) or (ni_[ind_r_gkids]==np.nan): #we store only ion with no null density
                nbr_ions_dens_null +=1
            else:
                ii = jj-nbr_ions_dens_null
                Ti_core.append(core_profiles.profiles_1d[ind_t_core].ion[ii].temperature) #useless to retrieve data for each ion species bc they have alle the same temperature profiles in the IMAS_simulation db
        
        nbr_ions = nbr_ions-nbr_ions_dens_null # NEED TO REDO THIS PART OF THE CODE TO HAVE CLEANER CODE 
        print("ionnumber non denity null:", nbr_ions)

        Te_core = core_profiles.profiles_1d[ind_t_core].electrons.temperature
        ne_core = core_profiles.profiles_1d[ind_t_core].electrons.density
        
        
        #equilibrium: Useful for ne, q, R, r, a, rho_tor_norm
        # FALSE
        # r_equi = ((equilibrium.time_slice[ind_t_equi].profiles_1d.r_outboard - equilibrium.time_slice[ind_t_equi].profiles_1d.r_inboard)/2)
        # R_equi = ((equilibrium.time_slice[ind_t_equi].profiles_1d.r_inboard + equilibrium.time_slice[ind_t_equi].profiles_1d.r_outboard)/2)
        
        q_equi = -equilibrium.time_slice[ind_t_equi].profiles_1d.q # np.array dim r
        magn_shear_equi = equilibrium.time_slice[ind_t_equi].profiles_1d.magnetic_shear # defined as rho_tor/q . dq/drho_tor
        Rmag_equi = equilibrium.vacuum_toroidal_field.r0 # FLOAT a vÃ©rifier: pas utile Ã  priori, c'est une constante
        Bmag_equi = -equilibrium.vacuum_toroidal_field.b0 # FLOAT
        #B_tor_equi = equilibrium.b_tor_vallGet(shot_number, 'core_profiles/profiles_1d('+str(ind_t_core)+')/electrons/density', 0, 0, 'imas_public', 'west')
        a_equi = equilibrium.time_slice[ind_t_equi].boundary.minor_radius # FLOAT
        elong_equi = equilibrium.time_slice[ind_t_equi].profiles_1d.elongation #difference with equilibrium.profiles_1d.elongation ???
        triang_up_equi = equilibrium.time_slice[ind_t_equi].profiles_1d.triangularity_upper# np.array dim r
        triang_low_equi = equilibrium.time_slice[ind_t_equi].profiles_1d.triangularity_lower
        beta_norm_equi = equilibrium.time_slice[ind_t_equi].global_quantities.beta_normal # FLOAT
        beta_tor_equi = equilibrium.time_slice[ind_t_equi].global_quantities.beta_tor # FLOAT
        psi_equi = equilibrium.time_slice[ind_t_equi].profiles_1d.psi
        #psin_equi = np.transpose(np.divide(psi_equi, psi_equi[:,0][:,np.newaxis])) #on normalise par la premiÃ¨re colonne de la matrice
        # psin_equi = (np.divide((psi_equi - psi_equi[:,[0]]), (-psi_equi[:,[0]])))[ind_t_equi]
        #fonction diamagnétique f = R*B_tor : faudra vérifier quel R est utilisé dans la définition de f
        f_equi = -equilibrium.time_slice[ind_t_equi].profiles_1d.f
        #Pressure
        pressure_equi = equilibrium.time_slice[ind_t_equi].profiles_1d.pressure

        ### ---------------- ###
        ###   INTERPOLATION  ###
        ### ---------------- ###
        def interpolate_function(L, f_L, L_prime, kind='linear'):
            L = np.array(L)
            f_L = np.array(f_L)
            L_prime = np.array(L_prime)
            # Interpolator creation
            interpolator = interp1d(L, f_L, kind=kind, fill_value="extrapolate")
            # Interpolation on new vector
            f_interp_L_prime = interpolator(L_prime)
            return f_interp_L_prime.tolist()
        psi_equi_1d = equilibrium.time_slice[ind_t_equi].profiles_1d.psi
        psin_equi_1d = (psi_equi_1d - psi_sep)/(psi_ax - psi_sep) # = np.linspace(0,1,101)

        # from equi
        q_gkids = interpolate_function(psin_equi_1d, q_equi, psin_resolution_gkids)
        magn_shear_gkids = interpolate_function(psin_equi_1d, magn_shear_equi, psin_resolution_gkids)
        Rmag_gkids = Rmag_equi
        Bmag_gkids =Bmag_equi
        a_gkids = a_equi # USEFUL???
        beta_norm_gkids = beta_norm_equi
        beta_tor_gkids = beta_tor_equi
        psi_gkids = interpolate_function(psin_equi_1d, psi_equi, psin_resolution_gkids)
        f_gkids = interpolate_function(psin_equi_1d, f_equi, psin_resolution_gkids)
        pressure_gkids = interpolate_function(psin_equi_1d, pressure_equi, psin_resolution_gkids)

        B_from_f_gkids = f_gkids/R_gkids #ce que je dois utiliser d'après Yann, cependant je suppose constant R=R0


        # from core_profile
        psi_core_1d = core_profiles.profiles_1d[ind_t_core].grid.psi 
        psin_core_1d = (psi_core_1d - psi_core_1d[0])/(psi_core_1d[-1]-psi_core_1d[0]) # = np.linspace(0,1,101)


        
    
        Ti_gkids = []
        for ii in range(nbr_ions):
            Ti_gkids.append(interpolate_function(psin_core_1d, Ti_core[ii], psin_resolution_gkids)) # useless to retrieve data for each ion species bc they have all the same temperature profiles in the IMAS_simulation db

        Te_gkids = interpolate_function(psin_core_1d, Te_core, psin_resolution_gkids)
        ne_gkids = interpolate_function(psin_core_1d, ne_core, psin_resolution_gkids)
        
        ###---------------------------###
        ### GK PARAMETERS COMPUTATION ###
        ###---------------------------###
                
        # Standard quantities        
        qref = 1.602176634e-19 #elementary charge (C)        
        mref = 3.3435837724e-27 #Deuterium mass (kg)
        Tref = Te_gkids[ind_r_gkids] #outboard midplane Te 
        nref = ne_gkids[ind_r_gkids] #outboard midplane ne
        #Lref = Rmag_gkids # R0 the major radius of the flux surface centre
        Lref = R_gkids[ind_r_gkids]
        Bref = B_from_f_gkids[ind_r_gkids] # Bt(R0) the toroidal magnetic field for the flux surface under consideration

        vthref = np.sqrt(2*Tref/mref)
        rhoref = mref*vthref/(qref*Bref)
        
        shear_rate_gk = 0 # TO DO : routine Clarisse à prendre
            
        grad_Te = np.gradient(Te_gkids, r_gkids)
        grad_ne = np.gradient(ne_gkids, r_gkids)
        grad_q = np.gradient(q_gkids, r_gkids)
        grad_p = np.gradient(pressure_gkids, r_gkids)
        
        rlte = -R_gkids[ind_r_gkids]*grad_Te[ind_r_gkids]/Te_gkids[ind_r_gkids]
        rlne = -R_gkids[ind_r_gkids]*grad_ne[ind_r_gkids]/ne_gkids[ind_r_gkids]

        magn_shear_gk = -r_gkids[ind_r_gkids]*grad_q[ind_r_gkids]/q_gkids[ind_r_gkids]
        
        Tref = Te_gkids[0]

        te_te = Te_gkids[ind_r_gkids]/Tref
        
        charge_electron_norm = -1.0
        mass_electron_norm = 9.1093837e-31/mref #check how many decimals are needed
        density_electron_norm = ne_gkids[ind_r_gkids]/nref

        grad_Ti = []
        grad_ni = []
        rlti = []
        rlni = []
        ti_te = []
        charge_ion_norm = []
        mass_ion_norm = []
        density_ion_norm = []
        velocity_tor_list = []
        grad_velocity_tor_list = []
        amu = 1.6605402e-27 # kg - Atomic Mass Unit -> need to know the decimal accuracy
        nbr_ions_dens_null = 0
        ni_gkids = []

        # REDO THIS PART WITH THE PREVIOUS FOR LOOP ON IONS
        nbr_ions = len(core_profiles.profiles_1d[ind_t_core].ion) # NEED TO REMOVE THAT
        for jj in range(nbr_ions):
            ni_ =core_profiles.profiles_1d[ind_t_core].ion[jj].density
            #density_norm
            if (ni_[ind_r_gkids]==0) or (ni_[ind_r_gkids]==np.nan): #we store only ion with no null density
                nbr_ions_dens_null +=1
            else:
                ii = jj-nbr_ions_dens_null
                ni_gkids.append(interpolate_function(psin_core_1d, ni_, psin_resolution_gkids))# store list 
                density_ion_norm.append(ni_[ind_r_gkids]/nref)
                # charge_norm
                # if iw.partialGet(shot_number, f"core_profiles/profiles_1d({ind_t_gkids})/ion({ii})/z_ion", 0, 0, "imas_simulation") != 0:
                charge_ion_norm.append(core_profiles.profiles_1d[ind_t_core].ion[ii].z_ion)#Already in elementary charge unit
                # mass_norm
                mass_atom_amu = core_profiles.profiles_1d[ind_t_core].ion[ii].element[0].a
                mass_ion = mass_atom_amu*amu
                # atoms_number = iw.partialGet(shot_number, f"core_profiles/profiles_1d({ind_t_gkids})/ion({ii})/element({0})/atoms_n", 0, 0, "imas_simulation")
                # mass_ion = mass_atom *atoms_number
                mass_ion_norm.append(mass_ion/mref)
                grad_Ti.append(np.gradient(Ti_gkids[ii], r_gkids))

                grad_ni.append(np.gradient(ni_gkids[ii], r_gkids))
                rlti.append(-R_gkids[ind_r_gkids]*grad_Ti[ii][ind_r_gkids]/Ti_gkids[ii][ind_r_gkids])
                if ni_gkids[ii][ind_r_gkids] ==0:
                    rlni.append(0)
                else: 
                    rlni.append(-R_gkids[ind_r_gkids]*grad_ni[ii][ind_r_gkids]/ni_gkids[ii][ind_r_gkids])
                ti_te.append(Ti_gkids[ii][ind_r_gkids]/Tref) #for my internship I took Ti_gkids[ii][ind_r_gkids]/Te_gkids[ind_r_gkids]
                try:#if rotation measurements are available
                    velocity_tor_list.append(core_profiles.profiles_1d[ind_t_core].ion[ii].velocity.toroidal)
                    grad_velocity_tor_list.append(np.gradient(velocity_tor_list[ii][:],r_gkids)/vthref)
                except:
                    velocity_tor_list.append(0)
                    grad_velocity_tor_list.append(0)

        nbr_ions = nbr_ions-nbr_ions_dens_null
        
        
        try:#if rotation measurements are available
            grad_velocity_tor_elec = np.gradient(core_profiles.profiles_1d[ind_t_core].electrons.velocity.toroidal,r_gkids)/vthref
            veltor = max(velocity_tor_list[:][ind_r_gkids])/vthref #CHECK LA DEFINITION
        except:
            grad_velocity_tor_elec = 0
            veltor = 0

        # USE CODATA VALUE INSTEAD:
        me = 9.109382e-31 # en kg
        qe = -1.602176e-19 # en C
        kb = 1.380649e-23 # en m2 kg s-2 K-1
        mu_zero = 12.56637e-7 #kg m A-2 s-2

        # TO DO : NORMALISATIONS ARE SURELY BAD
        collisionality_matrix = [[0 for sp in range(nbr_ions+1)] for spp in range(nbr_ions+1)]
        density_species = [density_electron_norm] + density_ion_norm
        charge_species = [charge_electron_norm] + charge_ion_norm 
        mass_species = [mass_electron_norm] + mass_ion_norm 
        T_species = np.concatenate((np.array([Te_gkids[ind_r_gkids]]), np.array(Ti_gkids)[:,ind_r_gkids]))
        for ii in range(nbr_ions+1): # ii = a
            for jj in range(nbr_ions+1): # jj = b
                lambda_debye_ab = epsilon_0 * Tref /(density_species[ii]*charge_species[ii]**2 + density_species[jj]*charge_species[jj]**2) 
                Lambda_ab = lambda_debye_ab/a_gkids
                vth_a = np.sqrt(2*T_species[ii]/mass_species[ii])
                num = R_gkids[ind_r_gkids] * density_species[jj] * mass_species[ii]**2 * mass_species[jj]**2 * qe**4 * np.log(Lambda_ab)
                denom = vthref * 4 * np.pi * epsilon_0**2 * mass_species[ii] * vth_a**3
                collisionality_matrix[ii][jj] = num/denom

        #beta
        beta_norm_gk = 2*mu_zero * (ne_gkids[ind_r_gkids] * Te_gkids[ind_r_gkids] *(-qe) )/Bmag_gkids[ind_r_gkids]**2
        beta_norm_gk_f = 2*mu_zero * (ne_gkids[ind_r_gkids] * Te_gkids[ind_r_gkids] *(-qe) )/B_from_f_gkids[ind_r_gkids]**2
       
        pressure_norm_gk = -(2*mu_zero*Rmag_gkids)/(Bmag_gkids[ind_r_gkids]**2) * grad_p[ind_r_gkids]
        pressure_norm_gk_f = -(2*mu_zero*Rmag_gkids)/(B_from_f_gkids[ind_r_gkids]**2) * grad_p[ind_r_gkids]

        x = (r_gkids/a_gkids)[ind_r_gkids]
        r_minor_norm = r_gkids[ind_r_gkids]/Lref

        # Yann routines for extended miller param computation
        R_x_point = None
        Z_x_point = None
        r0, R0, dR0dr, Z0, dZ0dr, k, dkdr, c, dcdr, s,dsdr, R_out, Z_out, err_out = FS_param.rz2mxh(R,Z)

        
        print("-----------------------------------------------\n")
        print("| ALLEGED VERSION OF WEST IMAS DATA TO GK IDS |\n")
        print("-----------------------------------------------\n")
        
        self.westgkids_dico = {
            'Tref' : Tref,
            'nref' : nref,
            'Bref' : Bref,
            'Lref' : Lref,
            'r_minor_norm' : r_minor_norm,
            'rlte': rlte,
            'rlti': rlti, #list ions number length
            'rlne': rlne,
            'rlni' : rlni,
            'ti_te': ti_te, #list ions number length
            'te_te': te_te,
            'k': k, # elongation
            'dkdr': dkdr,
            'q': q_gkids[ind_r_gkids],
            'magn_shear_gk': magn_shear_gk,
            'collisionality_matrix' : collisionality_matrix,
            'beta_norm_gk': beta_norm_gk,
            'beta_norm_gk_f': beta_norm_gk_f,
            'pressure_norm_gk': pressure_norm_gk,
            'pressure_norm_gk_f': pressure_norm_gk_f,
            'dR0dr': dR0dr[1],
            'dZ0dr': dZ0dr[1],
            'c': c[1],
            's': s[1],
            'dcdr': dcdr[1],
            'dsdr': dsdr[1],
            'veltor' : veltor,
            'grad_vel_e' : grad_velocity_tor_elec,
            'grad_vel_i' : grad_velocity_tor_list, #list ions number length
            'shear_rate' : shear_rate_gk,
            'mass_electron_norm' : mass_electron_norm, #list ions number length
            'charge_electron_norm' : charge_electron_norm, #list ions number length
            'density_electron_norm' : density_electron_norm,
            'mass_ion_norm' : mass_ion_norm, #list ions number length
            'charge_ion_norm' : charge_ion_norm, #list ions number length
            'density_ion_norm' : density_ion_norm #list ions number length equivalent of ni_ne / ti_te for temperature norm (revoir les nomenclature pour meilleure lecture)
        }
        print('x:', x)
        print('beta_norm gk : ', beta_norm_gk)
        print('beta_norm_f_gk :', beta_norm_gk_f)
        print('beta_norm_equi:', beta_norm_gkids)


    def conversion_dico_to_gkids(self):
        self.IDS_GK  = ids_gyrokinetics_local.GyrokineticsLocal()
        idspy_toolkit.fill_default_values_ids(self.IDS_GK)

        '''******************************************************'''
        '''*********************---INPUTS---*********************'''
        '''******************************************************'''


        ### --------------------------------
        ### -------IDS.ids_properties-------
        ### --------------------------------
        self.IDS_GK.ids_properties.provider = 'Name : '
        self.IDS_GK.ids_properties.comment  = 'Machine : WEST ; Shot : '

        ### --------------------------------
        ### -----------IDS.code-------------
        ### --------------------------------
        self.IDS_GK.code.name = 'WEST IDS inputs for GK simulations'
        self.IDS_GK.code.repository = 'no url'
        self.IDS_GK.code.commit = 'no commit'
        self.IDS_GK.code.version = 'no version'
        self.IDS_GK.code.library = 'no library'
        self.IDS_GK.code.output_flag = 0


        ### --------------------------------
        ### ---IDS.normalizing_quantities---
        ### --------------------------------
        self.IDS_GK.normalizing_quantities.t_e = self.westgkids_dico['Tref']
        self.IDS_GK.normalizing_quantities.n_e = self.westgkids_dico['nref']
        self.IDS_GK.normalizing_quantities.b_field_tor = self.westgkids_dico['Bref']
        self.IDS_GK.normalizing_quantities.r = self.westgkids_dico['Lref']

        ### --------------------------------
        ### -----------IDS.model------------
        ### --------------------------------
        self.IDS_GK.model.adiabatic_electrons = 0 # under which experimental considerations could elecrons be considered as adiabatic?
        self.IDS_GK.model.include_a_field_parallel = 0 # same commentary
        self.IDS_GK.model.include_b_field_parallel = 0 # same commentary
        #self.IDS_GK.model.use_mhd_rule = 0 # same commentary NOT UPDATED 
        self.IDS_GK.model.include_coriolis_drift = 0 # same commentary
        self.IDS_GK.model.include_centrifugal_effects = 0 # same commentary
        self.IDS_GK.model.collisions_pitch_only = 0 # same commentary
        self.IDS_GK.model.collisions_momentum_conservation = 0 # same commentary
        self.IDS_GK.model.collisions_energy_conservation = 0 # same commentary
        self.IDS_GK.model.collisions_momentum_conservation = 0 # same commentary
        
        ### --------------------------------
        ### --------IDS.flux_surface--------
        ### --------------------------------
        self.IDS_GK.flux_surface.ip_sign = 1
        self.IDS_GK.flux_surface.b_field_tor_sign = 1
        self.IDS_GK.flux_surface.r_minor_norm = self.westgkids_dico['r_minor_norm']
        self.IDS_GK.flux_surface.q = self.westgkids_dico['q']
        self.IDS_GK.flux_surface.magnetic_shear_r_minor = self.westgkids_dico['magn_shear_gk']
        self.IDS_GK.flux_surface.pressure_gradient_norm = self.westgkids_dico['pressure_norm_gk']
        self.IDS_GK.flux_surface.dgeometric_axis_r_dr_minor = self.westgkids_dico['dR0dr']
        self.IDS_GK.flux_surface.dgeometric_axis_z_dr_minor = self.westgkids_dico['dZ0dr']
        self.IDS_GK.flux_surface.elongation = self.westgkids_dico['k'][1] # the one of the flux surface under consideration
        self.IDS_GK.flux_surface.delongation_dr_minor_norm = self.westgkids_dico['dkdr'][1] # the one of the flux surface under consideration
        self.IDS_GK.flux_surface.shape_coefficients_c = self.westgkids_dico['c']
        self.IDS_GK.flux_surface.shape_coefficients_s = self.westgkids_dico['s']
        self.IDS_GK.flux_surface.dc_dr_minor_norm = self.westgkids_dico['dcdr']
        self.IDS_GK.flux_surface.ds_dr_minor_norm = self.westgkids_dico['dsdr']

        ### --------------------------------
        ### --------IDS.species_all---------
        ### --------------------------------
        Tref = self.westgkids_dico['Tref']
        nref = self.westgkids_dico['nref']
        qref = 1.602176634e-19 #elementary charge (C) TO DO WITH CODATA
        mref = 3.3435837724e-27 #Deuterium mass (kg) TO DO WITH CODATA
        Bref = self.westgkids_dico['Bref']
        vthref = np.sqrt(2*Tref*qref/mref)
        rhoref = mref*vthref/(qref*Bref)

        self.IDS_GK.species_all.beta_reference = self.westgkids_dico['beta_norm_gk']
        self.IDS_GK.species_all.velocity_tor_norm = self.westgkids_dico['veltor']
        self.IDS_GK.species_all.shearing_rate_norm = self.westgkids_dico['shear_rate'] # to be checked
        self.IDS_GK.species_all.debye_length_norm = 1/rhoref * np.sqrt(epsilon_0*Tref/(nref*qref**2))
        self.IDS_GK.species_all.angle_pol = 0

        ### --------------------------------
        ### ----------IDS.species-----------
        ### --------------------------------
        dum_e = ids_gyrokinetics_local.Species()
        dum_e.charge_norm = self.westgkids_dico['charge_electron_norm']
        dum_e.mass_norm = self.westgkids_dico['mass_electron_norm']
        dum_e.density_norm = self.westgkids_dico['density_electron_norm']
        dum_e.density_log_gradient_norm = self.westgkids_dico['rlne']
        dum_e.temperature_norm = self.westgkids_dico['te_te']
        dum_e.temperature_log_gradient_norm = self.westgkids_dico['rlte']
        dum_e.velocity_tor_gradient_norm = self.westgkids_dico['grad_vel_e']

        self.IDS_GK.species.append(dum_e)

        nbr_ions = len(self.westgkids_dico['charge_ion_norm'])
        for ii in range(nbr_ions):
            dum_i = ids_gyrokinetics_local.Species()
            dum_i.charge_norm = self.westgkids_dico['charge_ion_norm'][ii]
            dum_i.mass_norm = self.westgkids_dico['mass_ion_norm'][ii]
            dum_i.density_norm = self.westgkids_dico['density_ion_norm'][ii]
            dum_i.density_log_gradient_norm = self.westgkids_dico['rlni'][ii]
            dum_i.temperature_norm = self.westgkids_dico['ti_te'][ii]
            dum_i.temperature_log_gradient_norm = self.westgkids_dico['rlti'][ii]
            dum_i.velocity_tor_gradient_norm = self.westgkids_dico['grad_vel_i'][ii] # to be checked

            self.IDS_GK.species.append(dum_i)

        

        ### --------------------------------
        ### --------IDS.collisions----------
        ### --------------------------------
        self.IDS_GK.collisions.collisionality_norm = self.westgkids_dico['collisionality_matrix'] #matrix number_species x number_species
