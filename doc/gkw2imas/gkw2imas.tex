\newcommand{\U}[1]{\ensuremath{\,\textrm{#1}}}
\newcommand{\tref}{\ensuremath{\textrm{ref}}}
\newcommand{\vthref}{\ensuremath{v_\textrm{thref}}}
\newcommand{\gkw}{\ensuremath{\texttt{GKW}}}
\newcommand{\imas}{\ensuremath{\texttt{IMAS}}}

\documentclass[a4paper]{report}

\usepackage{fullpage}
\usepackage{graphicx}
\usepackage{epstopdf, epsfig}
\usepackage{hyperref}
\hypersetup{colorlinks=true,urlcolor=blue,linkcolor=blue,citecolor=blue}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{bm}
\usepackage{tabularx}
\usepackage{enumitem}

\begin{document}

\title{GKW to IMAS data conversion}

\author{Y. Camenen}

\date{March 2024}

\maketitle

\tableofcontents

\vfill
\noindent
\parbox[b]{0.7\textwidth}{\raggedright This work is distributed under a Creative Commons Attribution 4.0 International licence.\\ \url{https://creativecommons. org/licenses/by/4.0/}}
\includegraphics[width=0.25\textwidth]{../images/by.pdf}

\newpage

\chapter{Preamble}
This document describes how to transform inputs and outputs from a GKW flux-tube simulation to match the format defined by the IMAS \texttt{gyrokinetics\_local} Interface Data Structure (GK IDS) and used in the GyroKinetic DataBase (GKDB). \\ 
The reader is assumed to have some knowledge of GKW \cite{Peeters:CPC09,GKW:wiki} and to have read the documentation of the \texttt{gyrokinetics\_local} data model \cite{GKDB:wiki}. \\
The Python script \url{https://bitbucket.org/gkw/gkw/src/develop/python/yc/gkwimas.py} in the GKW git repository implements the transformations describes in this document to transform back and forth between GKW and a GK IDS.

\chapter{GKW to IMAS conversion}
\section{Conventions and normalisations}
\section{Coordinate systems}
In GKW, the toroidal direction is defined to have the cylindrical coordinate system $(R,Z,\varphi)$ right-handed whereas in IMAS conventions, it is defined to have $(R,\varphi,Z)$ right-handed, see Fig.\ref{fig:coord1}. In practice, it means that: 
\begin{equation}
\varphi^\imas=-\varphi^\gkw
\end{equation}
\begin{figure}[h]
	\begin{center}
		\includegraphics[width=7cm]{../images/GKW_coord.pdf}
		\includegraphics[width=7cm]{../images/IMAS_coord.pdf}
		\caption{\label{fig:coord1} Cylindrical coordinate system used in GKW (left) and in IMAS (right).}
	\end{center}
\end{figure}\\
In GKW. the flux surface centre definition depends on how the magnetic equilibrium is specified.\\
For \texttt{miller} parametrisation, the definition of $R_0$ is identical to that used in the GK IDS and $Z_0$ is given as an input in the geometry namelist:
\begin{equation}
R_0^\texttt{GKW-miller} = R_0^\imas \qquad \qquad Z_0^\texttt{GKW-miller} = \texttt{zmil}R_\tref^\gkw
\end{equation}
For \texttt{fourier} and \texttt{mxh} parametrisation, the definition of $R_0$ is also identical to that used in IMAS and $Z_0$ is not specified:
\begin{equation}
R_0^\texttt{GKW-fourier} = R_0^\texttt{GKW-mxh} = R_0^\imas 
\end{equation}
For \texttt{chease} equilibrium, $R_0$ is taken to be the value of \texttt{R0EXP} specified in the \texttt{hamada.dat} file and $Z_0$ is the elevation of the magnetic axis.
\begin{equation}
R_0^\texttt{GKW-chease} = \texttt{R0EXP} \qquad \qquad Z_0^\texttt{GKW-chease} = Z_\textrm{axis}
\end{equation}
The definition of the (dimensional) radial coordinate $r$ is identical in GKW and in IMAS:
\begin{equation}
r^\gkw = r^\imas
\end{equation}
The calculation of the  GK DD poloidal angle $\theta$ from GKW inputs is documented in section~\ref{sec:magequil}. At this stage, just notice that most of the time $Z_0^\gkw\neq Z_0^\imas$, therefore, the points $s=0$ and $\theta=0$ do not necessarily coincide. 


\section{Reference quantities} \label{sec:refquantities}
In GKW and the IMAS GK IDS, all quantities are normalised and made dimensionless by making use of reference quantities. In what follows, normalised quantities are denoted with a "N" subscript. For instance, the normalised version of an arbitrary quantity $\mathcal{A}$ with the dimension of a length will be $\mathcal{A}_N^\gkw=\mathcal{A}/R_\tref^\gkw$ in GKW and $\mathcal{A}_N^\imas=\mathcal{A}/R_\tref^\imas$ in IMAS. The conversion from GKW  to IMAS  involves the ratio of reference quantities. For the example above:
\begin{equation}
\mathcal{A}_N^\imas = \frac{R_\tref^\gkw}{R_\tref^\imas} \mathcal{A}_N^\gkw = R_\textrm{rat} \mathcal{A}_N^\gkw
\end{equation}
The ratio of the various reference quantities used in GKW and the IMAS GK DD are:
\begin{align*}
q_\textrm{rat} &=  \frac{q_\tref^\gkw}{q_\tref^\imas} = - \frac{1}{\texttt{z}^\gkw_{e^-}} & R_\textrm{rat}^\texttt{miller/fourier} &= \frac{R_\tref^\texttt{GKW-miller/fourier/mxh}}{R_\tref^\imas} = 1 \\
m_\textrm{rat} &=  \frac{m_\tref^\gkw}{m_\tref^\imas} = \frac{m_e}{m_D}\frac{1}{\texttt{mass}^\gkw_{e^-}} & B_\textrm{rat}^\texttt{miller/fourier} &= \frac{B_\tref^\texttt{GKW-miller/fourier/mxh}}{B_\tref^\imas} = 1 \\
T_\textrm{rat} &=  \frac{T_\tref^\gkw}{T_\tref^\imas} = \frac{1}{\texttt{temp}^\gkw_{e^-}} &R_\textrm{rat}^\texttt{chease} &= \frac{R_\tref^\texttt{GKW-chease}}{R_\tref^\imas} = \frac{\texttt{R0EXP}}{R_0^\imas}  \\
n_\textrm{rat} &=  \frac{n_\tref^\gkw}{n_\tref^\imas} =  \frac{n_\tref^\gkw}{n_e(\theta=0)} & B_\textrm{rat}^\texttt{chease} &= \frac{B_\tref^\texttt{GKW-chease}}{B_\tref^\imas} = \frac{\texttt{B0EXP}}{B_0^\imas}
\end{align*}
where the $e^-$ subscript denotes the electron species and the electron to deuterium mass ratio is taken to be $\frac{m_e}{m_D}=2.72443710748 \times 10^{-4}$ in IMAS.\\
The reference charge, mass, temperature and density ratio can be computed from the electron species parameters in the \texttt{SPECIES} namelist of the GKW input file. The poloidal asymmetry factor for the electron density can be computed from data in the \texttt{cfdens} file (GKW output).  With \texttt{chease} geometry, the ratios $ R_\textrm{rat}$ and $ B_\textrm{rat}$ can be computed from data in the \texttt{hamada.dat} file (GKW input).\\
The following derived quantities will also be used for the conversion:
\begin{align*}
v_\textrm{thrat} &= \sqrt{\frac{T_\textrm{rat}}{m_\textrm{rat}}} & \rho_\textrm{rat} = \frac{m_\textrm{rat}v_\textrm{thrat}}{q_\textrm{rat} B_\textrm{rat}}
\end{align*}


\section{Inputs}
\subsection{Magnetic equilibrium} \label{sec:magequil}
Only \texttt{fourier}, \texttt{miller}, \texttt{mxh} and \texttt{chease} magnetic equilibrium specifications are compatible with the GK IMAS format (\texttt{s-alpha} and \texttt{circ} are not an exact solution of the Grad-Shafranov equation).
\subsubsection{Flux surface centre}
Let's call $\{R_{\Psi_0},Z_{\Psi_0}\}$ a set of points discretizing the flux surface of interest.\\
The values of $\{R_{\Psi_0},Z_{\Psi_0}\}/R_\tref^\gkw$ are available in the \texttt{geom.dat} file (GKW output) and can be used to compute $\{R_0^\imas,Z_0^\imas\}/R_\tref^\gkw$. 

\subsubsection{Poloidal angle}
With these values, one can then compute the IMAS poloidal angle $\theta$:
\begin{equation}
 \tan \theta = - \frac{Z_{\Psi_0}/R_\tref^\gkw-Z_0^\imas/R_\tref^\gkw}{R_{\Psi_0}/R_\tref^\gkw-R_0^\imas/R_\tref^\gkw}
\end{equation}
As the discretisation of the flux surface in \texttt{geom.dat} is done on the GKW $s$ grid, the equation above gives the relationship between $\theta$ and $s$.

\subsubsection{Radial coordinate}
\begin{equation}
r_N^\imas = r_N^\gkw \cdot R_\textrm{rat}= \texttt{eps}\cdot R_\textrm{rat}
\end{equation}

\subsubsection{Toroidal field and current direction}
\begin{equation}
 s_b^\imas = - s_b^\gkw=-\texttt{signb} \qquad \textrm{and} \qquad s_j^\imas=-s_j^\gkw=-\texttt{signj}
\end{equation}

\subsubsection{Safety factor}
\begin{equation}
q^\imas = s_b^\gkw s_j^\gkw q^\gkw = \texttt{signb}\cdot\texttt{signj}\cdot\texttt{q}
\end{equation}

\subsubsection{Magnetic shear}
\begin{equation}
\hat{s}^\imas = \hat{s}^\gkw = \texttt{shat}
\end{equation}

\subsubsection{Pressure gradient (entering the curvature drift)}
\begin{equation}
p^{' \imas}_N = - \beta^{' \gkw}_N \frac{B_\textrm{rat}^2}{R_\textrm{rat}}
\end{equation}
The value of $\beta^{' \gkw}_N $ is taken from \texttt{betaprime\_ref} for \texttt{miller} geometry or from the \texttt{geom.dat} file for \texttt{chease} geometry.

\subsubsection{Plasma shape}
For \texttt{mxh} geometry, the IMAS shaping coefficients and their radial derivatives are computed directly from the GKW shaping coefficients.\\
For \texttt{miller} and \texttt{fourier} geometry, the shape of the flux surface $\{R_{\Psi_0}(r,\theta),Z_{\Psi_0}(r,\theta)\}/R_\tref^\gkw$ is first computed from the GKW shaping coefficients and then used to compute the IMAS coefficients.\\
For \texttt{chease} geometry $\{R_{\Psi_0}(r,s),Z_{\Psi_0}(r,s)\}$ is directly available in the \texttt{hamada.dat} file and can be used together with the relationship between $\theta$ and $s$ to compute the IMAS coefficients. 

\subsection{Species}

\subsubsection{Charge}
\begin{equation}
Z_{sN}^\imas = Z_{sN}^\gkw \cdot q_\textrm{rat} = \texttt{z}_s \cdot q_\textrm{rat} 
\end{equation}

\subsubsection{Mass}
\begin{equation}
m_{sN}^\imas = m_{sN}^\gkw \cdot m_\textrm{rat} = \texttt{mass}_s \cdot m_\textrm{rat}
\end{equation}

\subsubsection{Density}
\begin{equation}
n_{sN}^\imas(\theta=0) = n_{sN}^\gkw(\theta=0) \cdot n_\textrm{rat} = \texttt{dens}_s \cdot n_\textrm{rat} \cdot \frac{n_{sN}^\gkw(\theta=0)}{n_{sN}^\gkw(s=0)}
\end{equation}
In the presence of poloidal asymmetries, the density at $\theta=0$ can be obtained from the \texttt{cfdens} output file. In this file, the three columns are the $s$ grid, the "full" logarithmic density gradient of each species and the species density, normalised to the species density at $s=0$:
\begin{equation}
s, \qquad\qquad -\frac{1}{n_{sN}^\gkw}\frac{\partial n_{sN}^\gkw}{\partial r_N^\gkw}, \qquad\qquad \frac{n_s^\gkw}{n_s^\gkw(s=0)}
\end{equation}
 
\subsubsection{Centrifugal energy}
The effective potential energy arising from centrifugal effects can also be computed using the \texttt{cfdens} data: 
\begin{equation}\label{eq:cf_energy}
\mathcal{E}_{sN}^\imas = \mathcal{E}_{sN}^\gkw\cdot T_\textrm{rat}  - \texttt{temp}_s\cdot T_\textrm{rat} 
\textrm{ln} \frac{n_s^\gkw(s=0)}{n_s^\gkw(\theta=0)}
\end{equation}
with
\begin{equation}
\mathcal{E}_{sN}^\gkw = - \texttt{temp}_s\cdot \textrm{ln} \frac{n_s^\gkw}{n_s^\gkw(s=0)}
\end{equation}
 

\subsubsection{Logarithmic density gradient}
Since $\mathcal{E}_{sN}^\imas(\theta=0)$ and its radial gradient are zero, by definition, we have 
\begin{equation}
\left. \frac{R}{L_{n_s}}\right|^\imas = -\frac{1}{n_{sN}^\imas(\theta=0)}\frac{\partial n_{sN}^\imas(\theta=0)}{\partial r_N^\imas} = -\left.\frac{1}{n_s^\imas}\frac{\partial n_{sN}^\imas }{\partial r_N^\imas}\right|_\theta(\theta=0) 
\end{equation}
The radial derivative at constant $\theta$ is related to the radial derivative at constant $s$ by:
\begin{equation}
\left.\frac{1}{n_{sN}^\imas}\frac{\partial n_{sN}^\imas}{\partial r_N^\imas}\right|_\theta = \left.\frac{1}{n_{sN}^\gkw}\frac{\partial n_{sN}^\gkw}{\partial r_N^\gkw}\right|_s\cdot \frac{1}{R_\textrm{rat}} + \left.\frac{1}{n_{sN}^\gkw}\frac{\partial n_{sN}^\gkw}{\partial s}\right|_r \cdot \frac{1}{R_\textrm{rat}} \frac{\nabla r\cdot \nabla s}{\nabla r \cdot \nabla r}R_\tref^\gkw
\end{equation}
where the GKW metric tensors can be obtained from the \texttt{geom.dat} output on the $s$ grid:
\begin{equation}
\frac{\nabla r\cdot \nabla s}{\nabla r \cdot \nabla r}R_\tref^\gkw = \frac{g^{\epsilon s}_N}{g^{\epsilon,\epsilon}_N}
\end{equation}
Finally, we get:
\begin{equation}
\left. \frac{R}{L_{n_s}}\right|^\imas = -\left.\frac{1}{n_{sN}^\gkw}\frac{\partial n_{sN}^\gkw}{\partial r_N^\gkw}\right|_s(\theta=0)\cdot \frac{1}{R_\textrm{rat}}- \left.\frac{1}{n_{sN}^\gkw}\frac{\partial n_{sN}^\gkw}{\partial s}\right|_r(\theta=0) \cdot \frac{1}{R_\textrm{rat}} \frac{g^{\epsilon s}_N(\theta=0)}{g^{\epsilon,\epsilon}_N(\theta=0)}
\end{equation}

\subsubsection{Centrifugal energy gradient}
Using Eq.~(\ref{eq:cf_energy}), we get:
\begin{align}
\left.\frac{\partial \mathcal{E}_{sN}^\imas}{\partial r_N^\imas}\right|_\theta = & \left.\frac{\partial \mathcal{E}_{sN}^\gkw}{\partial r_N^\gkw}\right|_\theta\cdot \frac{T_\textrm{rat}}{R_\textrm{rat}}  
 + \texttt{rlt}_s \cdot \texttt{temp}_s\cdot \frac{T_\textrm{rat}}{R_\textrm{rat}} \textrm{ln} \frac{n_s^\gkw(s=0)}{n_s^\gkw(\theta=0)} \nonumber \\
 & + \texttt{rln}_s\cdot \texttt{temp}_s\cdot \frac{T_\textrm{rat}}{R_\textrm{rat}} - \texttt{temp}_s\cdot T_\textrm{rat} \left. \frac{R}{L_{n_s}}\right|^\imas
\end{align}
which can be computed using 
\begin{equation}
\left.\frac{\partial \mathcal{E}_{sN}^\gkw}{\partial r_N^\gkw}\right|_\theta = \left.\frac{\partial \mathcal{E}_{sN}^\gkw}{\partial r_N^\gkw}\right|_s + \left.\frac{\partial \mathcal{E}_{sN}^\gkw}{\partial s}\right|_r \frac{g^{\epsilon s}_N}{g^{\epsilon \epsilon}_N}
\end{equation}
and
\begin{equation}
\left.\frac{\partial \mathcal{E}_{sN}^\gkw}{\partial r_N^\gkw}\right|_s = -\frac{1}{n_{sN}^\gkw}\frac{\partial n_{sN}^\gkw}{\partial r_N^\gkw}\cdot\texttt{temp}_s  - \texttt{rln}_s\cdot\texttt{temp}_s - \mathcal{E}_{sN}^\gkw  \texttt{rlt}_s
\end{equation}
where 
\begin{equation}
\mathcal{E}_{sN}^\gkw = - \texttt{temp}_s \textrm{ln} \frac{n_s^\gkw}{n_s^\gkw(s=0)}
\end{equation}
\subsubsection{Temperature}
\begin{equation}
T_{sN}^\imas = T_{sN}^\gkw \cdot T_\textrm{rat} = \texttt{temp}_s \cdot T_\textrm{rat}
\end{equation}

\subsubsection{Logarithmic temperature gradient}
\begin{equation}
\left. \frac{R}{L_{T_s}}\right|^\imas  = \texttt{rlt}_s \cdot \frac{1}{R_\textrm{rat}}
\end{equation}

\subsubsection{Toroidal velocity}
\begin{equation}
u_N^\imas = - s_b^\gkw\cdot u_N^\gkw \cdot \frac{v_\textrm{thrat}}{R_\textrm{rat}}  = - \texttt{signb} \cdot \texttt{vcor} \cdot \frac{v_\textrm{thrat}}{R_\textrm{rat}}
\end{equation}

\subsubsection{Toroidal velocity gradient}
\begin{equation}
u^{'\imas}_{sN} = -s_b^\gkw\cdot  u^{'\gkw}_{sN} \cdot \frac{v_\textrm{thrat}}{R_\textrm{rat}^2} = -\texttt{signb} \cdot \texttt{uprim}_s \cdot \frac{v_\textrm{thrat}}{R_\textrm{rat}^2}
\end{equation}

\subsubsection{$E\times B$ shearing rate}
\begin{equation}
\gamma_E^\imas =  \gamma_E^\gkw \cdot \frac{v_\textrm{thrat}B_\textrm{rat}}{R_\textrm{rat}}
\end{equation}

\subsubsection{Plasma beta}
\begin{equation}
\beta_{eN}^\imas = \beta_N^\gkw  \cdot \frac{B_\textrm{rat}^2}{n_\textrm{rat}  T_\textrm{rat}} 
\end{equation}
The value of $\beta_N^\gkw$ is either taken from the input \texttt{beta\_ref} of the \texttt{SPCGENERAL} namelist when the  \texttt{beta\_type='ref'} option is used or extracted from the output file \texttt{out} when the  \texttt{beta\_type='eq'} option is used.
 
\subsubsection{Collisionality}
\begin{equation}
\nu_{a/bN}^\imas =  \nu_{a/bN}^\gkw \cdot \frac{v_\textrm{thrat}}{R_\textrm{rat}}\cdot \sqrt{\frac{\texttt{temp}_a}{\texttt{mass}_a}}\cdot \frac{n_{sN}^\gkw(\theta=0)}{n_{sN}^\gkw(s=0)}
\end{equation}
The values of $\nu_{a/bN}^\gkw$ are available in the \texttt{CollFreqs} file (GKW output). 

\subsubsection{Debye length}
Not yet implemented in GKW (to do).

\subsection{Wave vector}
\subsubsection{Binormal wave vector}
Since GKW $\zeta$ coordinate and IMAS $y$ coordinate are in the same toroidal direction, we have:
\begin{equation}
\left. k_{\theta*}\rho_\tref\right.^\imas = \texttt{kthrho}\cdot\frac{1}{\rho_\textrm{rat}}\cdot\sqrt{\frac{g^{\zeta\zeta}(\theta=0)}{g^{\zeta\zeta}(s=0)}}
\end{equation}
The quantity $g^{\zeta \zeta}$ required for the conversion is given on the $s$ grid in \texttt{geom.dat}. 

\subsubsection{Radial wave vector}
Similarly:
\begin{equation}
\left. k_{r*}\rho_\tref\right.^\imas =\texttt{krrho}\cdot\frac{1}{\rho_\textrm{rat}}\cdot\sqrt{\frac{g^{\psi\psi}(\theta=0)}{g^{\psi\psi}(s=0)}}
\end{equation}
The quantity $g^{\psi \psi}$ is given on the $s$ grid in \texttt{geom.dat}. 

\section{Outputs}

\subsection{Time grid}
\begin{equation}
t_N^\imas = t_N^\gkw \cdot \frac{R_\textrm{rat}}{v_\textrm{thrat}}
\end{equation}

\subsection{Mode growth rate and frequency}
\begin{equation}
\gamma_N^\imas= \gamma_N^\gkw \cdot \frac{v_\textrm{thrat}}{R_\textrm{rat}} \qquad\qquad \textrm{and}
\qquad \qquad \omega_{rN}^\imas= s_j^\gkw \cdot \omega_{rN}^\gkw \cdot \frac{v_\textrm{thrat}}{R_\textrm{rat}}
\end{equation}
The tolerance on the mode growth rate convergence can be computed directly from the values of $\gamma_N^\gkw(t)$ stored in the GKW output file \texttt{time.dat}. In GKW units, the time interval used to perform the integral is given by $\Delta t_N^\gkw=\textrm{max}(\tau_{\parallel sN}^\gkw,\tau_{\perp sN}^\gkw)$
with 
\begin{align*}
\tau_{\parallel sN}^\gkw & = 2\pi q \sqrt{\frac{\texttt{mass}_s}{\texttt{temp}_s}\frac{m_\textrm{rat}}{T_\textrm{rat}}}\\
\tau_{\perp  sN}^\gkw& = \frac{2\pi}{k_{\theta*} \rho_\tref} \frac{|\texttt{z}_s|q_\textrm{rat}}{\texttt{temp}_s T_\textrm{rat}}
\end{align*}

\subsection{Mode amplitude}
The normalised fields in GKW and the IMAS GK DD are related as follows:
\begin{equation}
\hat{\phi}_N^\imas = \hat{\phi}_N^\gkw\cdot\frac{T_\textrm{rat}\rho_\textrm{rat}}{q_\textrm{rat}R_\textrm{rat}}, \qquad \qquad
\hat{A}_{\parallel N}^\imas = \hat{A}_{\parallel N}^\gkw\cdot\frac{B_\textrm{rat}\rho_\textrm{rat}^2}{R_\textrm{rat}}, \qquad \qquad 
\hat{B}_{\parallel N}^\imas = \hat{B}_{\parallel N}^\gkw\cdot\frac{B_\textrm{rat}\rho_\textrm{rat}}{R_\textrm{rat}},  
\end{equation}
In GKW linear runs, the exponentially growing fields are further normalised with respect to the mode amplitude:
\begin{equation}
\hat{\phi}_{NN}^\gkw = \frac{1}{\mathcal{A}_f^\gkw}\hat{\phi}_N^\gkw,  \qquad \qquad \hat{A_\parallel}_{NN}^\gkw = \frac{1}{\mathcal{A}_f^\gkw}\hat{A_\parallel}_N^\gkw,  \qquad \qquad \hat{B_\parallel}_{NN}^\gkw = \frac{1}{\mathcal{A}_f^\gkw}\hat{B_\parallel}_N^\gkw
\end{equation}
with
\begin{equation}
\mathcal{A}_f^\gkw = \sqrt{\int \left[|\hat{\phi}_N^\gkw|^2 + |\hat{A}_{\parallel N}^\gkw|^2+|\hat{B}_{\parallel N}^\gkw|^2\right] \,\textrm{d}s \bigg/ \int \,\textrm{d}s} 
\end{equation}
The mode amplitude used to normalise linear runs and compute the mode growth rate in the IMAS GK DD is 
\begin{equation}
\mathcal{A}_f^\imas  = \sqrt{\frac{1}{2\pi}\left.\int \left[|\hat{\phi}_N^\imas|^2 + |\hat{A}_{\parallel N}^\imas|^2  + |\hat{B}_{\parallel N}^\imas|^2\right] \,\textrm{d}\theta \right.}
\end{equation}
and the ratio of the IMAS to GKW mode amplitudes is therefore given by:
\begin{equation}
\frac{1}{\mathcal{A}_\textrm{rat}} = \frac{\mathcal{A}_f^\imas}{\mathcal{A}_f^\gkw} = \sqrt{\frac{1}{2\pi}\int\left[ |\frac{T_\textrm{rat}\rho_\textrm{rat}}{q_\textrm{rat}R_\textrm{rat}}\cdot\hat{\phi}_{NN}^\gkw|^2 + |\frac{B_\textrm{rat}\rho_\textrm{rat}^2}{R_\textrm{rat}}\cdot\hat{A}_{\parallel NN}^\gkw|^2+|\frac{B_\textrm{rat}\rho_\textrm{rat}}{R_\textrm{rat}}\cdot\hat{B}_{\parallel NN}^\gkw|^2\right]\,\textrm{d}\theta }
\end{equation}
The mode amplitude ratio can therefore be computed from the GKW output file \texttt{parallel.dat} which contains $\hat{\phi}_{NN}^\gkw(s)$, $\hat{A_\parallel}_{NN}^\gkw(s)$ and $\hat{B_\parallel}_{NN}^\gkw(s)$. Note that in this file, the fields may have been rotated in the complex plane, but this does not impact the mode amplitude calculation. Alternatively (default option in \texttt{gkwimas.py}), the \texttt{kykxs} diagnostics can be used.

\subsection{Fields}
\begin{align}
\phi_{Nf}^\imas &= \hat{\phi}_{NN}^\gkw\cdot \mathcal{A}_\textrm{rat}\frac{T_\textrm{rat}\rho_\textrm{rat}}{q_\textrm{rat}R_\textrm{rat}}\cdot \textrm{e}^{i\alpha}\\
A_{\parallel Nf}^\imas &= \hat{A}_{\parallel NN}^\gkw\cdot \mathcal{A}_\textrm{rat}\frac{B_\textrm{rat}\rho_\textrm{rat}^2}{R_\textrm{rat}}\cdot \textrm{e}^{i\alpha}\\
B_{\parallel Nf}^\imas &= \hat{B}_{\parallel NN}^\gkw\cdot \mathcal{A}_\textrm{rat}\frac{B_\textrm{rat}\rho_\textrm{rat}}{R_\textrm{rat}}\cdot \textrm{e}^{i\alpha}
\end{align}
with
\begin{equation}
\textrm{e}^{i\alpha} = \frac{|\hat{\phi}_{NN}^\gkw(\theta^*)|}{\hat{\phi}_{NN}^\gkw(\theta^*)}
\end{equation}
where $\theta^*$ is the lowest value of $\theta$ at which $|\hat{\phi}_{NN}^\gkw|$ is maximum.

\subsection{Moments of the distribution function}
\begin{align}
\delta n_{sN}^\imas & = \texttt{dens}_s \cdot \mathcal{A}_\textrm{rat}  \frac{\rho_\textrm{rat}}{R_\textrm{rat}}\frac{n_{sN}^\gkw(s=0)}{n_{sN}^\gkw(\theta=0)} \textrm{e}^{i\alpha}\\
\delta j_{\parallel sN}^\imas & =  \texttt{vpar}_s\cdot q_{sN}^\gkw q_\textrm{rat} \sqrt{\frac{T_{sN}^\gkw }{m_{sN}^\gkw }} \sqrt{\frac{T_\textrm{rat}}{m_\textrm{rat}}}\cdot \mathcal{A}_\textrm{rat}  \frac{\rho_\textrm{rat}}{R_\textrm{rat}} \frac{n_{sN}^\gkw(s=0)}{n_{sN}^\gkw(\theta=0)}\textrm{e}^{i\alpha}\\
\delta p_{\perp sN}^\imas & =  \texttt{Tperp}_s\cdot \frac{4}{3}T_{sN}^\gkw T_\textrm{rat}\cdot \mathcal{A}_\textrm{rat}  \frac{\rho_\textrm{rat}}{R_\textrm{rat}}\frac{n_{sN}^\gkw(s=0)}{n_{sN}^\gkw(\theta=0)} \textrm{e}^{i\alpha}\\
\delta p_{\parallel sN}^\imas & =  \texttt{Tpar}_s\cdot \frac{2}{3}T_{sN}^\gkw T_\textrm{rat}\cdot \mathcal{A}_\textrm{rat}  \frac{\rho_\textrm{rat}}{R_\textrm{rat}}\frac{n_{sN}^\gkw(s=0)}{n_{sN}^\gkw(\theta=0)} \textrm{e}^{i\alpha}\\
\delta q_{\parallel sN}^\imas & =  \texttt{Qpar}_s\cdot T_{sN}^\gkw T_\textrm{rat}  \sqrt{\frac{T_{sN}^\gkw }{m_{sN}^\gkw }} \sqrt{\frac{T_\textrm{rat}}{m_\textrm{rat}}}\cdot \mathcal{A}_\textrm{rat}  \frac{\rho_\textrm{rat}}{R_\textrm{rat}}\frac{n_{sN}^\gkw(s=0)}{n_{sN}^\gkw(\theta=0)} \textrm{e}^{i\alpha}\\
\delta M_{12sN}^\imas & = \texttt{M12}_s \cdot T_{sN}^\gkw T_\textrm{rat}  \sqrt{\frac{T_{sN}^\gkw }{m_{sN}^\gkw }} \sqrt{\frac{T_\textrm{rat}}{m_\textrm{rat}}} \cdot \mathcal{A}_\textrm{rat}  \frac{\rho_\textrm{rat}}{R_\textrm{rat}}\frac{n_{sN}^\gkw(s=0)}{n_{sN}^\gkw(\theta=0)} \textrm{e}^{i\alpha}\\
\delta M_{24sN}^\imas & = \texttt{M24}_s \cdot T_{sN}^\gkw T_\textrm{rat}  \frac{T_{sN}^\gkw }{m_{sN}^\gkw } \frac{T_\textrm{rat}}{m_\textrm{rat}} \cdot \mathcal{A}_\textrm{rat}  \frac{\rho_\textrm{rat}}{R_\textrm{rat}}\frac{n_{sN}^\gkw(s=0)}{n_{sN}^\gkw(\theta=0)} \textrm{e}^{i\alpha}
\end{align}

\subsection{Fluxes}
The GKW fluxes \texttt{pflux}, \texttt{vflux} and \texttt{eflux} in the output files \texttt{fluxes.dat} and \texttt{fluxes\_lab.dat} are slightly different from the IMAS fluxes. Namely:
\begin{itemize}
	\item The heat flux misses the contribution from the centrifugal energy
	\item The parallel momentum flux due to the magnetic flutter misses the polarisation/magnetisation contribution
	\item The perpendicular momentum flux is not computed
	\item The laboratory frame heat flux misses the contribution from the perpendicular momentum flux.
\end{itemize}
 These missing contributions are computed in post-processing in \texttt{gkwrun.py} and converted to IMAS as follows:
 \begin{align}
\Gamma_{Ns}^\imas &= \Gamma_{Ns}^\gkw \cdot \mathcal{A}_\textrm{rat}^2\frac{v_\textrm{thrat}\rho_\textrm{rat}^2}{R_\textrm{rat}^2}\frac{n_{sN}^\gkw(s=0)}{n_{sN}^\gkw(\theta=0)}\\
\Pi_{Ns}^{\parallel \imas} &= -\Pi_{Ns}^{\parallel \gkw} \cdot \mathcal{A}_\textrm{rat}^2\frac{m_\textrm{rat}v_\textrm{thrat}^2\rho_\textrm{rat}^2}{R_\textrm{rat}}\frac{n_{sN}^\gkw(s=0)}{n_{sN}^\gkw(\theta=0)}\sqrt{T_{sN}^\gkw m_{sN}^\gkw}\\
\Pi_{Ns}^{\perp \imas} &= -\Pi_{Ns}^{\perp \gkw} \cdot \mathcal{A}_\textrm{rat}^2\frac{m_\textrm{rat}v_\textrm{thrat}^2\rho_\textrm{rat}^2}{R_\textrm{rat}}\frac{n_{sN}^\gkw(s=0)}{n_{sN}^\gkw(\theta=0)}\sqrt{T_{sN}^\gkw m_{sN}^\gkw}\\
Q_{Ns}^\imas &= Q_{Ns}^\gkw \cdot \mathcal{A}_\textrm{rat}^2\frac{v_\textrm{thrat}T_\textrm{rat}\rho_\textrm{rat}^2}{R_\textrm{rat}^2}\frac{n_{sN}^\gkw(s=0)}{n_{sN}^\gkw(\theta=0)}T_{sN}^\gkw
\end{align}
  

\chapter{IMAS to GKW conversion}

\section{Conventions and normalisations}
\subsection{Coordinate system}
In GKW, the flux surface centre definition depends on how the magnetic equilibrium is specified. For \texttt{mxh} parametrisation, which will be used to generate the GKW input file from the GK IDS, the definition of $R_0$ and $Z_0$ are identical to that used in IMAS:
\begin{eqnarray}
R_0^\texttt{GKW-mxh} &=& R_0^\imas =\frac{1}{2}[R_\textrm{min}+R_\textrm{max}]\\
Z_0^\texttt{GKW-mxh} &=& Z_0^\imas=\frac{1}{2}[Z_\textrm{min}+Z_\textrm{max}]
\end{eqnarray}
As a consequence, the positions of $\theta=0$ and $s=0$ coincide, which greatly facilitates the conversion process. 
The definition of the (dimensional) radial coordinate $r$ is also identical in GKW and in IMAS:
\begin{equation}
r^\gkw = r^\imas =\frac{1}{2}[R_\textrm{max}-R_\textrm{min}]
\end{equation}

\subsection{Reference quantities}
The conversion from GKW  to IMAS  involves the ratio of reference quantities introduced in Sec.~\ref{sec:refquantities}. To ease the conversion process we will choose the reference quantities in GKW to match that of the IMAS standard, when possible: 
\begin{align*}
q_\textrm{rat} &=  \frac{q_\tref^\gkw}{q_\tref^\imas} = 1 \\
m_\textrm{rat} &=  \frac{m_\tref^\gkw}{m_\tref^\imas} =1 \\
T_\textrm{rat} &=  \frac{T_\tref^\gkw}{T_\tref^\imas} = T_{iN}^\imas\\
n_\textrm{rat} &=  \frac{n_\tref^\gkw}{n_\tref^\imas} = 1\\
R_\textrm{rat} &= \frac{R_\tref^\texttt{GKW-mxh}}{R_\tref^\imas} = 1 \\
B_\textrm{rat} &= \frac{B_\tref^\texttt{GKW-mxh}}{B_\tref^\imas} = 1 
\end{align*}
The only exception is for the reference temperature, for which we choose the main ion temperature as is customary in GKW. The main ion is defined as the species with positive charge and the largest density.
The following derived quantities will also be used for the conversion:
\begin{align*}
v_\textrm{thrat} &= \sqrt{\frac{T_\textrm{rat}}{m_\textrm{rat}}} & \rho_\textrm{rat} = \frac{m_\textrm{rat}v_\textrm{thrat}} {q_\textrm{rat} B_\textrm{rat}}
\end{align*}

\subsection{Defaults}
Due to the versatility of GKW, there are several ways to prepare a run matching a GK IDS. On top of the choice of reference quantities described in the previous section, we adopt the following settings:
\begin{itemize}
	\item Flux surface description with Miller eXtended Harmonics parametrisation:\\ $\texttt{geom\_type}=$'mxh' in \texttt{GEOM} namelist
	\item Pressure gradient contribution to curvature drift specified via $\beta'$:\\
	$\texttt{gradp\_type}=$ 'beta\_prime' in \texttt{GEOM} namelist
	\item $\beta$ and $\beta'$ defined from user input:\\
	$\texttt{beta\_type}=$'ref' in \texttt{SPCGENERAL} namelist\\
	$\texttt{betaprime\_type}=$'ref' in \texttt{SPCGENERAL} namelist
	\item User defined collision frequencies:\\
	$\texttt{freq\_input}=$.true. in \texttt{COLLISIONS} namelist
	\item All centrifugal switches on if centrifugal effects are included:\\
	$\texttt{upphi}=$.true. in \texttt{ROTATION} namelist\\
	$\texttt{upsrc}=$.true. in \texttt{ROTATION} namelist
	\item For $E\times B$ shearing rate: \\
	its value is specified in $\texttt{shear\_rate}$ input in \texttt{ROTATION} namelist\\
	$\texttt{shear\_profile}$='wavevector\_remap' in \texttt{ROTATION} namelist\\
	$\texttt{toroidal\_shear}$='none' in \texttt{ROTATION} namelist
	\item For linear runs, $\texttt{mode\_box}=$.false. and $\texttt{kr\_type}=$'kr' enforced in \texttt{MODE} namelist  
\end{itemize}

\section{Inputs conversion}

\subsection{Magnetic equilibrium} 
Using the \texttt{mxh} parametrisation in GKW makes the conversion of the magnetic equilibrium description pretty straightforward. The only difference is that the poloidal angle is of opposite direction in IMAS and GKW which simply changes the sign of the coefficients for the cosine components in the Fourier expansion. 

\subsubsection{Radial coordinate}
\begin{equation}
r_N^\imas = r_N^\gkw \cdot R_\textrm{rat}
\end{equation}
\begin{equation}
\texttt{eps} = \texttt{r\_minor\_norm} \cdot \frac{1}{R_\textrm{rat}}  
\end{equation}

\subsubsection{Toroidal field and current direction}
\begin{equation}
s_b^\imas = - s_b^\gkw \qquad \textrm{and} \qquad s_j^\imas=-s_j^\gkw
\end{equation}
\begin{equation}
\texttt{signb} = - \texttt{b\_field\_tor\_sign} \qquad \textrm{and} \qquad \texttt{signj} = - \texttt{ip\_sign}
\end{equation}

\subsubsection{Safety factor}
\begin{equation}
q^\imas = s_b^\gkw s_j^\gkw q^\gkw
\end{equation}
\begin{equation}
\texttt{q} = \texttt{b\_field\_tor\_sign} \cdot \texttt{ip\_sign} \cdot \texttt{q}
\end{equation}

\subsubsection{Magnetic shear}
\begin{equation}
\hat{s}^\imas = \hat{s}^\gkw 
\end{equation}
\begin{equation}
\texttt{shat} = \texttt{magnetic\_shear\_r\_minor}
\end{equation}

\subsubsection{Pressure gradient (entering the curvature drift)}
\begin{equation}
p^{' \imas}_N = - \beta^{' \gkw}_N \frac{B_\textrm{rat}^2}{R_\textrm{rat}}
\end{equation}
The value of $\beta^{' \gkw}_N $ is given in \texttt{betaprime\_ref} with the default settings chosen for the IMAS/GKW conversion.
\begin{equation}
\texttt{betaprime\_ref} = - \texttt{pressure\_gradient\_norm} \cdot \frac{R_\textrm{rat}}{B_\textrm{rat}^2}
\end{equation}

\subsubsection{Plasma shape}
\begin{align}
\texttt{dRmil} & = \texttt{dgeometric\_axis\_r\_dr\_minor} \\
\texttt{dZmil} & = \texttt{dgeometric\_axis\_z\_dr\_minor} \\
\texttt{kappa} & = \texttt{elongation} \\
\texttt{skappa} & = \frac{\texttt{r\_minor\_norm}}{\texttt{elongation}}\cdot\texttt{delongation\_dr\_minor\_norm}\\
\texttt{c} & = - \texttt{shape\_coefficients\_c}\\
\texttt{s} & = \texttt{shape\_coefficients\_s}\\
\texttt{c\_prime} & = - \texttt{dc\_dr\_minor\_norm}\cdot R_\textrm{rat}\\
\texttt{s\_prime} & = \texttt{ds\_dr\_minor\_norm}\cdot R_\textrm{rat}\\
\end{align}

\subsection{Species}

\subsubsection{Charge}
\begin{equation}
Z_{sN}^\imas = Z_{sN}^\gkw \cdot q_\textrm{rat} 
\end{equation}
\begin{equation}
\texttt{z}_s = \texttt{charge\_norm}_s\cdot \frac{1}{q_\textrm{rat} }
\end{equation}

\subsubsection{Mass}
\begin{equation}
m_{sN}^\imas = m_{sN}^\gkw \cdot m_\textrm{rat}
\end{equation}
\begin{equation}
\texttt{mass}_s = \texttt{mass\_norm}_s\cdot \frac{1}{m_\textrm{rat} }
\end{equation}

\subsubsection{Density}
\begin{equation}
n_{sN}^\imas(\theta=0) = n_{sN}^\gkw(\theta=0) \cdot n_\textrm{rat} = \texttt{dens}_s \cdot n_\textrm{rat} \cdot \frac{n_{sN}^\gkw(\theta=0)}{n_{sN}^\gkw(s=0)}
\end{equation}
\begin{equation}
\texttt{dens}_s = \texttt{density\_norm}_s \cdot \frac{1}{n_\textrm{rat}}
\end{equation}
since with \texttt{mxh} shape parametrisation the positions of $\theta=0$ and $s=0$ coincide. 

\subsubsection{Logarithmic density gradient}
\begin{equation}
\frac{R_\tref^\imas}{L_{n_s}^\imas}(\theta=0) =  \frac{R_\tref^\gkw}{L_{n_s}^\gkw}(\theta=0) \cdot  \frac{1}{R_\textrm{rat}} = \texttt{rln}_s \cdot  \frac{1}{R_\textrm{rat}} \cdot \frac{{L_{n_s}^\gkw}(s=0)}{{L_{n_s}^\gkw}(\theta=0)}
\end{equation}
\begin{equation}
\texttt{rln}_s = \texttt{density\_log\_gradient\_norm} \cdot R_\textrm{rat}
\end{equation}

\subsubsection{Temperature}
\begin{equation}
T_{sN}^\imas = T_{sN}^\gkw \cdot T_\textrm{rat} = \texttt{temp}_s \cdot T_\textrm{rat}
\end{equation}
\begin{equation}
\texttt{temp}_s = \texttt{temperature\_norm}\cdot\frac{1}{T_\textrm{rat}}
\end{equation}

\subsubsection{Logarithmic temperature gradient}
\begin{equation}
\frac{R_\tref^\imas}{L_{T_s}^\imas} =  \frac{R_\tref^\gkw}{L_{T_s}^\gkw} \cdot \frac{1}{R_\textrm{rat}} = \texttt{rlt}_s \cdot \frac{1}{R_\textrm{rat}}
\end{equation}
\begin{equation}
\texttt{rlt}_s = \texttt{temperature\_log\_gradient\_norm} \cdot R_\textrm{rat}
\end{equation}

\subsubsection{Toroidal velocity}
\begin{equation}
u_N^\imas = -s_b^\gkw\cdot u_N^\gkw \cdot \frac{v_\textrm{thrat}}{R_\textrm{rat}}  = -\texttt{signb} \cdot \texttt{vcor} \cdot \frac{v_\textrm{thrat}}{R_\textrm{rat}}
\end{equation}
\begin{equation}
\texttt{vcor}  = \texttt{velocity\_tor\_norm}\cdot\texttt{b\_field\_tor\_sign}\cdot\frac{R_\textrm{rat}}{v_\textrm{thrat}}
\end{equation}

\subsubsection{Toroidal velocity gradient}
\begin{equation}
u'^{\imas}_{sN} = -s_b^\gkw\cdot  u'^{\gkw}_{sN} \cdot \frac{v_\textrm{thrat}}{R_\textrm{rat}^2} = -\texttt{signb} \cdot \texttt{uprim}_s \cdot \frac{v_\textrm{thrat}}{R_\textrm{rat}^2}
\end{equation}
\begin{equation}
\texttt{uprim}_s =  \texttt{velocity\_tor\_gradient\_norm}_s\cdot \texttt{b\_field\_tor\_sign}\cdot\frac{R^2_\textrm{rat}}{v_\textrm{thrat}}
\end{equation}

\subsubsection{$E\times B$ shearing rate}
\begin{equation}
\gamma_E^\imas =  \gamma_E^\gkw \cdot \frac{v_\textrm{thrat}B_\textrm{rat}}{R_\textrm{rat}}
\end{equation}
\begin{equation}
\texttt{shear\_rate}=  \texttt{shearing\_rate\_norm} \cdot \frac{R_\textrm{rat}}{v_\textrm{thrat}B_\textrm{rat}}
\end{equation}

\subsubsection{Plasma beta}
\begin{equation}
\beta_{eN}^\imas = \beta_N^\gkw  \cdot \frac{B_\textrm{rat}^2}{n_\textrm{rat}  T_\textrm{rat}} 
\end{equation}
\begin{equation}
\texttt{beta\_ref}=\texttt{beta\_reference}\cdot\frac{n_\textrm{rat}  T_\textrm{rat}}{B_\textrm{rat}^2}
\end{equation}

\subsubsection{Collisionality}
\begin{equation}
\nu_{a/bN}^\imas = \nu_{a/bN}^\gkw \cdot \frac{v_\textrm{thrat}}{R_\textrm{rat}}\cdot \sqrt{\frac{T_{aN}^\gkw}{m_{aN}^\gkw}} = \nu_{a/bN}^\gkw \cdot\frac{1}{R_\textrm{rat}}\cdot  \sqrt{\frac{T_{aN}^\imas}{m_{aN}^\imas}}
\end{equation}
\begin{equation}
\texttt{nu\_ab}=\texttt{collisionality\_norm}\cdot R_\textrm{rat}\cdot\sqrt{\frac{\texttt{mass\_norm}_a}{\texttt{temperature\_norm}_a}}
\end{equation}
The values of $\nu_{a/bN}^\gkw$ are available in the \texttt{CollFreqs} file (GKW output). 

\subsubsection{Debye length}
Always zero in GKW runs.

\subsection{Wave vector}
\subsubsection{Binormal wave vector}
\begin{equation}
\left. k_{\theta*}\rho_\tref\right.^\imas = \texttt{kthrho}\cdot\frac{1}{\rho_\textrm{rat}}\cdot\sqrt{\frac{g^{\zeta\zeta}(\theta=0)}{g^{\zeta\zeta}(s=0)}}
\end{equation}
Since with the MXH parametrisation the $\theta=0$ of IMAS and the $s=0$ of GKW coincides, we simply have:
\begin{equation}
\texttt{kthrho}=\left. k_{\theta*}\rho_\tref\right.^\imas \cdot \rho_\textrm{rat}
\end{equation}

\subsubsection{Radial wave vector}
\begin{equation}
\left. k_{r*}\rho_\tref\right.^\imas = \texttt{krrho}\cdot\frac{1}{\rho_\textrm{rat}}\cdot\sqrt{\frac{g^{\psi\psi}(\theta=0)}{g^{\psi\psi}(s=0)}}
\end{equation}
Similarly, this leads to:
\begin{equation}
\texttt{krrho}=\left. k_{r*}\rho_\tref\right.^\imas \cdot \rho_\textrm{rat}
\end{equation}

\bibliographystyle{unsrt}
\bibliography{gkw2imas}

\end{document}
