\newcommand{\U}[1]{\ensuremath{\,\textrm{#1}}}

\documentclass[a4paper]{article}

\usepackage{fullpage}
\usepackage{graphicx}
\usepackage{epstopdf, epsfig}
\usepackage{hyperref}
\hypersetup{colorlinks=true,urlcolor=blue,linkcolor=blue,citecolor=blue}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{bm}
\usepackage{tabularx}
\usepackage{enumitem}
\newcolumntype{M}{>{\arraybackslash}m{0.08cm}}
\newcolumntype{N}{>{\arraybackslash}m{0.05cm}}

\begin{document}
\title{Documentation of the \texttt{gyrokinetics\_local}\\ IMAS Interface Data Structure}

\author{Y. Camenen}
\date{Last update: \today}

\maketitle

\begin{abstract}
A unified set of conventions and normalisations for the inputs and outputs of delta-f flux-tube gyrokinetic simulations has been developed within the ITER Integrated Modelling and Analysis Suite (IMAS). It is the \texttt{gyrokinetics\_local} Interface Data Structure. This standard aims at facilitating benchmarks and data exchange between different code users, increasing the modularity of frameworks manipulating inputs and outputs from gyrokinetic simulations and serving as the basis for databases storing simulations from multiple gyrokinetic codes. 
The first version of the \texttt{gyrokinetics\_local} Interface Data Structure was described in \cite{Camenen:NF2024}. The present document is based on the initial publication but is regularly updated to reflect developments of the data dictionary. It also provides additional information and documents tools developed to manipulate \texttt{gyrokinetics\_local} Interface Data Structures.\\
This document and related Python scripts are available at \url{https://gitlab.com/gkdb/imas-gk}
\end{abstract}


 \vfill
 \noindent
 \parbox[b]{0.7\textwidth}{\raggedright This work is distributed under a Creative Commons Attribution 4.0 International licence.\\ \url{https://creativecommons. org/licenses/by/4.0/}}
 \includegraphics[width=0.25\textwidth]{../images/by.pdf}
 
\newpage
\tableofcontents

\section{Introduction}
Once finalised copy-paste NF paper here

\section{Manipulating GK IDSs}
\subsection{Magnetic equilibrium and associated metric}
The magnetic equilibrium and the related metric tensors can be computed from the content of the \texttt{flux\_surface} structure as detailed in \cite{Candy:PPCF2009}. For convenience, the main equations are given here with IMAS normalisations and reference to the equations number in the original publication. A Python script computing the metric tensors defined in this section from the \texttt{flux\_surface} structure of a GK IDS is available at \url{https://gitlab.com/gkdb/imas-gk}.
\\
The flux surface shape is obtained from the parametrisation introduced in Sec.~\ref{sec:flux_surface}:
\begin{align}
R_N(r_N,\theta) &= 1 + r_N \cos \theta_R \\
Z_N(r_N,\theta) &= -r_N\kappa\sin \theta
\end{align}
where $R_N=R_\Psi/R_0$ and $Z_N=Z_\Psi/R_0$. The corresponding radial and azimuthal derivatives are:
\begin{align}
\frac{\partial R_N}{\partial \bar{r}_N} &= \frac{\partial R_c}{\partial r} + \cos \theta_R - \frac{\partial \theta_R}{\partial \bar{r}_N} \sin \theta \\
\frac{\partial Z_N}{\partial \bar{r}_N} &= \frac{\partial Z_c}{\partial r} -\kappa \sin \theta -r_N \frac{\partial \kappa}{\partial \bar{r}_N} \sin \theta\\
\frac{\partial R_N}{\partial \theta} &= -r_N\frac{\partial \theta_R}{\partial \theta}\sin \theta_R\\
\frac{\partial Z_N}{\partial \theta} &= -r_N\kappa\cos \theta
\end{align}
where we have introduced the normalised radial coordinate $\bar{r}_N=r/R_0$ and where
\begin{align}
\frac{\partial \theta_R}{\partial \bar{r}_N}  & = \sum_{n=0}^{n_{sh}-1} \frac{\partial c_N}{\partial \bar{r}_N}\cos n\theta + \frac{\partial s_N}{\partial \bar{r}_N}s_n(r) \sin n\theta\\
\frac{\partial \theta_R}{\partial \theta} &= 1 +  \sum_{n=1}^{n_{sh}-1} - nc_n\sin n\theta + ns_n(r) \cos n\theta
\end{align}
These quantities can then be used to compute the Jacobian of the flux coordinate system $(r,\theta,\varphi)$ (Eq.~(94) in \cite{Candy:PPCF2009}):
\begin{equation}
\mathcal{J}_{r\theta\varphi N} = \frac{1}{R_0^2}\mathcal{J}_{r\theta\varphi}=-R_N\left(\frac{\partial R_N}{\partial \bar{r}_N}\frac{\partial Z_N}{\partial \theta} -\frac{\partial R_N}{\partial \theta}\frac{\partial Z_N}{\partial \bar{r}_N}\right)
\end{equation}
Noting $l_N=l/R_0$, where $l$ is the arc length along the poloidal contour at $r=r_0$, with $l$ increasing in the direction of $\theta$ and $l=0$ at $\theta=0$, we have (Eq.~(93) in \cite{Candy:PPCF2009}):
\begin{equation}
\frac{\partial l_N}{\partial \theta} = \sqrt{\left[ \frac{\partial R_N}{\partial \theta}\right]^2 + \left[\frac{\partial Z_N}{\partial \theta} \right]^2}
\end{equation}
From Eq.~(95) of \cite{Candy:PPCF2009} we then get: 
\begin{equation}
|\nabla r| = \frac{R_N}{\mathcal{J}_{r\theta\varphi N}}\frac{\partial l_N}{\partial \theta}
\end{equation}
Combining the definition of $B_\textrm{unit}$ and Eq.~(97) of \cite{Candy:PPCF2009}, remembering that $RB_t=s_bR_0B_{ref}$, we obtain the radial derivative of the poloidal flux: 
\begin{equation}
\frac{\partial \Psi_N}{\partial \bar{r}_N}= -\frac{s_b}{q}\int_0^{2\pi} \frac{\partial l_N}{\partial \theta}\frac{\textrm{d}\theta}{R_N|\nabla r|}= -\frac{s_b}{q}\int_0^{2\pi}\frac{\mathcal{J}_{r\theta\varphi N}}{R_N^2}\,\textrm{d}\theta
\end{equation}
The normalised toroidal magnetic field is given by:
\begin{equation}
B_{t N} = \frac{B_t}{B_{ref}} = \frac{s_b}{R_N}
\end{equation}
For the poloidal magnetic field, $B_p$, we get:
\begin{equation}
B_{p N} = \frac{B_p}{B_{ref}} = \frac{|\nabla r|}{2\pi R_N}\frac{\partial \Psi_N}{\partial \bar{r}_N}
\end{equation}
The radial derivative of the plasma volume $V$ enclosed by a flux surface is given by:
\begin{equation}
V'_N = \frac{1}{R_0^2}\frac{\partial V}{\partial r} = 2\pi\int_0^{2\pi} \mathcal{J}_{r\theta\varphi N} \,\textrm{d}\theta
\end{equation}
which can be used to compute the flux surface average of a quantity $A$
\begin{equation}
\left< A \right>=\frac{2\pi}{V'_N}\int_0^{2\pi} A \mathcal{J}_{r\theta\varphi N} \,\textrm{d}\theta\,\textrm{d}\varphi
\end{equation}
The computation of the perpendicular wavevector is a bit more involved and requires to define intermediate quantities given in Eqs~(40, 41, 96, 105, 106, 107, 108, 109, 110, 111) of \cite{Candy:PPCF2009}:
\begin{align}
\cos{u} &= -\frac{\partial Z_N}{\partial \theta}\frac{\partial \theta}{\partial l_N}\\
r_{cN} &= -\left[\frac{\partial l_N}{\partial \theta}\right]^3\left[\frac{\partial R_N}{\partial \theta}\frac{\partial^2 Z_N}{\partial \theta^2}-\frac{\partial Z_N}{\partial \theta}\frac{\partial^2 R_N}{\partial \theta^2}\right]^{-1}\\
\frac{1}{r_N}E_{1N}(\theta)&=2\int_0^\theta \frac{\mathcal{J}_{r\theta\phi N}}{R_N^2}\frac{B_{tN}}{B_{pN}}\left[\frac{1}{r_{cN}}-\frac{1}{R_N}\cos{u}\right]\,\textrm{d}\theta\\
E_{2N}(\theta)&=\int_0^\theta  \frac{\mathcal{J}_{r\theta\phi N}}{R_N^2}\frac{B_N^2}{B_{pN}^2}\,\textrm{d}\theta\\
\beta^*_NE_{3N}(\theta)&=\frac{1}{2}\int_0^\theta \frac{1}{R_N}\frac{\partial l_N}{\partial \theta} \frac{B_{tN}}{B_{pN}^3}p'_N \,\textrm{d}\theta\\
f^*&=\frac{1}{E_{2N}(2\pi)}\left[ 2\pi \frac{q\hat{s}}{{r_N}}-\frac{1}{r_N}E_{1N}(2\pi)+\beta^*_NE_{3N}(2\pi)\right]\\
G&=\frac{r_N}{R_N}\frac{B_N}{B_{pN}}\\
\Theta &= \frac{R_NB_{pN}}{B_N}|\nabla r|\left[ \frac{1}{r_N}E_{1N}+f^*E_{2N}-\beta^*_NE_{3N}\right]
\end{align}
With the help of these quantities and Eq.~(113) of \cite{Candy:PPCF2009}, the perpendicular wavevector can be computed as:
\begin{equation}
k_\perp^2 = k_{r}^2g^{rr*}+ 2k_{r} k_{\theta*} g^{r\theta*} + k_{\theta}^2g^{\theta\theta*}
\end{equation}
where $k_{r}$ and $k_{\theta}$ are defined in Eq.~(\ref{eq:kxky}) and the following metric tensors have been introduced:
\begin{align}
g^{rr*}&=\frac{|\nabla r|^2}{|\nabla r|_{\theta=0}^2} \\
g^{r\theta*}&=\frac{|\nabla r|}{|\nabla r|_{\theta=0}}\frac{G\Theta}{|G_{\theta=0}|}\\
g^{\theta\theta*}&=\frac{G^2}{G^2_{\theta=0}}(1+\Theta^2)
\end{align}
To obtain the perpendicular wavevector expression above, we have assumed the IMAS binormal coordinate to be $y=-\alpha$, where $\alpha$ is the binormal coordinate used in \cite{Candy:PPCF2009} and verifying:
\begin{equation}
\mathbf{B}=-\frac{1}{2\pi}\nabla \alpha \times \nabla \Psi
\end{equation} 

\subsection{Computing radial fluxes from the normalised moments}
This section describes how the normalised radial fluxes are computed from the normalised fields and moments of the distribution function.
The moments involved in the fluxes calculation and introduced in Sec.~\ref{sec:fluxes_rotating}, are related to the IMAS normalised moments defined in Sec.~\label{sec:moments_norm} as follows:
\begin{align}
\hat{\mathcal{M}}^{J_0}_s(1) & = \hat{\mathcal{M}}^{J_0}_{sN}(1)\cdot n_{s0}\rho_*\hat{\mathcal{A}}_f\textrm{e}^{-i\alpha} \nonumber \\
\hat{\mathcal{M}}^{J_0}_s(v_\parallel) & = \hat{\mathcal{M}}^{J_0}_{sN}(Z_sv_\parallel)\cdot \frac{1}{Z_s}n_{s0}v_{thref}\rho_*\hat{\mathcal{A}}_f\textrm{e}^{-i\alpha} \nonumber \\
\hat{\mathcal{M}}^{J_1}_s(\frac{\mu_s}{Z_se}) & = \hat{\mathcal{M}}^{J_1}_{sN}(\frac{1}{3}m_sv_\perp^2)\cdot\frac{3}{2}\frac{1}{Z_seB} n_{s0}T_{e0}\rho_*\hat{\mathcal{A}}_f\textrm{e}^{-i\alpha} \nonumber \\
\hat{\mathcal{M}}^{J_0}_s(\epsilon) & = \left[\frac{3}{2}\left[\hat{\mathcal{M}}^{J_0}_{sN}(\frac{1}{3}m_sv_\parallel^2)+\hat{\mathcal{M}}^{J_0}_{sN}(\frac{1}{3}m_sv_\perp^2)\right] +\hat{\mathcal{M}}^{J_0}_{sN}(1) \mathcal{E}_{sN} \right]\cdot n_{s0}T_{e0}\rho_*\hat{\mathcal{A}}_f\textrm{e}^{-i\alpha}  \nonumber \\
\hat{\mathcal{M}}^{J_0}_s(v_\parallel\epsilon) & = \left[\hat{\mathcal{M}}^{J_0}_{sN}(\frac{1}{2}m_sv_\parallel v^2)+ 
\hat{\mathcal{M}}^{J_0}_{sN}(Z_sv_\parallel)\frac{\mathcal{E}_{sN}}{Z_s}\right]\cdot  n_{s0}v_{thref}T_{e0}\rho_*\hat{\mathcal{A}}_f\textrm{e}^{-i\alpha}  \nonumber \\
\hat{\mathcal{M}}^{J_1}_s(\frac{\mu_s}{Z_s e}\epsilon) & = \left[\hat{\mathcal{M}}^{J_1}_{sN}(\frac{1}{2}m_sv_\perp^2 v^2)\frac{1}{2}m_sv_{thref}^2+ 
\hat{\mathcal{M}}^{J_1}_{sN}(\frac{1}{3}m_sv_\perp^2)\frac{3}{2}\mathcal{E}_{sN}\right]\cdot  \frac{1}{Z_seB}n_{s0}T_{e0}\rho_*\hat{\mathcal{A}}_f\textrm{e}^{-i\alpha}  \nonumber \\
\hat{\mathcal{M}}^{J_0}_s(v_\parallel^2) & = \hat{\mathcal{M}}^{J_0}_{sN}(\frac{1}{3}m_sv_\parallel^2)\cdot \frac{3}{m_s}n_{s0}T_{e0}\rho_*\hat{\mathcal{A}}_f\textrm{e}^{-i\alpha} \nonumber \\
\hat{\mathcal{M}}^{J_1}_s(\frac{\mu_s}{Z_se}v_\parallel) & = \hat{\mathcal{M}}^{J_1}_{sN}(\frac{1}{2}m_sv_\parallel v_\perp^2)\cdot \frac{1}{Z_seB}n_{s0}v_{thref}T_{e0}\rho_*\hat{\mathcal{A}}_f\textrm{e}^{-i\alpha} \nonumber \\
\hat{\mathcal{M}}^{J_1}_s(\mu_s) & = \hat{\mathcal{M}}^{J_1}_{sN}(\frac{1}{3}m_sv_\perp^2)\cdot\frac{3}{2}\frac{1}{B} n_{s0}T_{e0}\rho_*\hat{\mathcal{A}}_f\textrm{e}^{-i\alpha} \nonumber
\end{align}
An other important quantity for the fluxes calculation is 
\begin{equation}
k_y\frac{\mathbf{b}\times\nabla y}{B}\cdot \nabla r = \mathcal{K}_{1N} \cdot \frac{1}{\rho_*R_0B_{ref}}
\end{equation}
with 
\begin{equation}
\mathcal{K}_{1N}= -k_\theta\rho_{ref}\frac{|G_{\theta=0}|}{2\pi r_N}\frac{\partial \bar{r}_N}{\partial \Psi_N}
\end{equation}
which can be computed from quantities defined in the previous section.
For the toroidal momentum flux, one also needs:
\begin{equation}
nk_\perp^r = \mathcal{K}_{2N}\frac{1}{\rho_*\rho_{ref}}
\end{equation}
with
\begin{equation}
\mathcal{K}_{2N}=-\frac{\bar{r}_N}{|G|_{\theta=0}}k_\theta\rho_{ref}\left[k_r\rho_{ref}\frac{|\nabla r|}{|\nabla r|_{\theta=0}} + k_\theta\rho_{ref}\frac{G}{|G|_{\theta=0}}\Theta|\nabla r|\right]
\end{equation}
With these relations, the normalised fluxes per wavevector (i.e. the linear weights defined in Sec. \label{sec:linear_weights}) can be expressed as:
\begin{align}
\Gamma_{sN,\phi} &= \left< \mathcal{K}_{1N} \textrm{Im}\left[ \hat{\mathcal{M}}^{J_0}_{sN}(1) \hat{\phi}_{Nf}^\dagger\right]\right>\\
\Gamma_{sN,A_\parallel} &= -\left< \mathcal{K}_{1N} \frac{2}{Z_s}\textrm{Im}\left[ \hat{\mathcal{M}}^{J_0}_{sN}(Z_sv_\parallel) \hat{A}_{\parallel,Nf}^\dagger\right]\right>\\
\Gamma_{sN,B_\parallel} &= -\left< \mathcal{K}_{1N} \frac{3}{2}\frac{1}{Z_sB_N}\textrm{Im}\left[ \hat{\mathcal{M}}^{J_1}_{sN}(\frac{1}{3}m_sv_\perp^2) \hat{B}_{\parallel,Nf}^\dagger\right]\right>
\end{align}
\begin{align}
Q_{sN,\phi} &= \left< \mathcal{K}_{1N} \textrm{Im}\left[ \left(\frac{3}{2}\hat{\mathcal{M}}^{J_0}_{sN}(\frac{1}{3}m_sv_\parallel^2)+\frac{3}{2}\hat{\mathcal{M}}^{J_0}_{sN}(\frac{1}{3}m_sv_\perp^2)+\mathcal{E}_{sN}\hat{\mathcal{M}}^{J_0}_{sN}(1) \right) \hat{\phi}_{Nf}^\dagger\right]\right>\\
Q_{sN,A_\parallel} &= -\left< \mathcal{K}_{1N} 2\textrm{Im}\left[ \left(\hat{\mathcal{M}}^{J_0}_{sN}(\frac{1}{2}m_sv_\parallel v^2)+\frac{\mathcal{E}_{sN}}{Z_s}\hat{\mathcal{M}}^{J_0}_{sN}(Z_sv_\parallel) \right) \hat{A}_{\parallel,Nf}^\dagger\right]\right>\\
Q_{sN,B_\parallel} &= \left< \mathcal{K}_{1N} \frac{m_{sN}}{Z_sB_N}\textrm{Im}\left[ \left(\hat{\mathcal{M}}^{J_0}_{sN}(\frac{1}{2}m_sv_\perp^2 v^2)+\frac{3}{2}\mathcal{E}_{sN}\hat{\mathcal{M}}^{J_0}_{sN}(\frac{1}{3}m_sv_\perp^2) \right) \hat{B}_{\parallel,Nf}^\dagger\right]\right>
\end{align}
\begin{align}
\Pi^\parallel_{sN,\phi} &= \left< \mathcal{K}_{1N} s_bm_{sN}R_N\frac{|B_{tN}|}{B_N}\frac{1}{Z_s}\textrm{Im}\left[ \hat{\mathcal{M}}^{J_0}_{sN}(Z_sv_\parallel) \hat{\phi}_{Nf}^\dagger\right]\right>\\
\Pi^\parallel_{sN,A_\parallel} &= -\left< \mathcal{K}_{1N} 2 s_bR_N\frac{|B_{tN}|}{B_N}\textrm{Im}\left[ \frac{3}{2}\hat{\mathcal{M}}^{J_0}_{sN}(\frac{1}{3}m_sv_\parallel^2) \hat{A}_{\parallel,Nf}^\dagger \right.\right. \nonumber\\
&\left.\left.-\frac{1}{2}\frac{n_s}{n_{s0}}T_{sN}\left(\frac{Z_s}{T_{sN}}\Gamma_0\hat{\phi}_{Nf}+\frac{1}{B_N}(\Gamma_0-\Gamma_1)\hat{B}_{\parallel,Nf}\right)\hat{A_\parallel}_{Nf}^\dagger\right]\right>\\
\Pi^\parallel_{sN,B_\parallel} &= \left< \mathcal{K}_{1N} s_bm_{sN}R_N\frac{|B_{tN}|}{B_N^2}\frac{1}{Z_s}\textrm{Im}\left[ \hat{\mathcal{M}}^{J_1}_{sN}(\frac{1}{2}m_sv_\parallel v_\perp^2) \hat{B}_{\parallel,Nf}^\dagger\right]\right>
\end{align}
\begin{align}
\Pi^\perp_{sN,\phi} &= \left< \mathcal{K}_{2N} \frac{m_{sN}}{2B_N^2}\left[ \frac{3}{2}\frac{1}{Z_s} \textrm{Re}\left[ \hat{\mathcal{M}}^{J_1}_{sN}(\frac{1}{3}m_sv_\perp^2) \hat{\phi}_{Nf}^\dagger\right] \right.\right. \nonumber\\
&\left. \left.+ \frac{n_s}{n_{s0}}(\Gamma_0-\Gamma_1)|\hat{\phi}_{Nf}|^2 +\frac{n_s}{n_{s0}}\frac{T_{sN}}{2Z_sB_N}(\Gamma_0-\Gamma_1)\textrm{Re}\left[\hat{B}_{\parallel,Nf}\hat{\phi}_{Nf}^\dagger\right]\right]\right>\\
\Pi^\perp_{sN,A_\parallel} &= - \left< \mathcal{K}_{2N} \frac{m_{sN}}{B_N^2}\frac{1}{Z_s} \textrm{Re}\left[ \hat{\mathcal{M}}^{J_1}_{sN}(\frac{1}{2}m_sv_\parallel v_\perp^2) \hat{A}_{\parallel,Nf}^\dagger\right]\right>\\
\Pi^\perp_{sN,B_\parallel} &= \left< \mathcal{K}_{2N} \frac{1}{(k_\perp\rho_{ref})^2}\left[ \frac{3}{2}\frac{1}{B_N}\textrm{Re}\left[ \left(\hat{\mathcal{M}}^{J_1}_{sN}(\frac{1}{3}m_s v_\perp^2) -  \hat{\mathcal{M}}^{J_0}_{sN}(\frac{1}{3}m_s v_\perp^2) \right)\hat{B}_{\parallel,Nf}^\dagger\right] \nonumber \right.\right.\\
& + \frac{n_s}{n_{s0}} \frac{Z_s}{B_N} \left[(2b_s-1)\Gamma_0-3b_s\Gamma_1\right] \textrm{Re}\left[\hat{\phi}_{Nf}\hat{B}_{\parallel,Nf}^\dagger\right]\nonumber \\
& \left. \left.+ \frac{n_s}{n_{s0}} \frac{T_{sN}}{B_N^2} \left[\frac{1}{2}(\Gamma_0-\Gamma_1) + (4-2b_s)\left[(4b_s-3)\Gamma_0+(1-4b_s)\Gamma_1\right]\right]|\hat{B}_{\parallel,Nf}|^2
\right]\right>
\end{align}

\section{Generating GK IDSs from \texttt{core\_profile} and \texttt{equilibrium}}
Implemented using IMASpy (add link and indicate where to find the routines)

Generate a new IDS per shot, time slice and radial position. Fill only the input part and leave the tables \texttt{code}, \texttt{model} empty.


\bibliographystyle{unsrt}
\bibliography{GK_DD_doc}

\end{document}
