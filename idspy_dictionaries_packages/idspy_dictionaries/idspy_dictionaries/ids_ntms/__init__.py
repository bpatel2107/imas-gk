# __version__= "034000.2.0"
# __version_imas_dd__= "03.40.00"
# __imas_dd_git_commit__= "845f1b30816f86a3cd4d53714dc56cdd307fdca1"
# 
from idspy_dictionaries.ids_ntms.idspy_ntms import (
    BTorVacuum1,
    Code,
    Identifier,
    IdsProperties,
    IdsProvenance,
    IdsProvenanceNode,
    Library,
    NtmTimeSlice,
    NtmTimeSliceMode,
    NtmTimeSliceModeDetailedEvolution,
    NtmTimeSliceModeDetailedEvolutionDeltaw,
    NtmTimeSliceModeDetailedEvolutionTorque,
    NtmTimeSliceModeEvolutionDeltaw,
    NtmTimeSliceModeEvolutionTorque,
    NtmTimeSliceModeOnset,
    Ntms,
)

__all__ = [
    "BTorVacuum1",
    "Code",
    "Identifier",
    "IdsProperties",
    "IdsProvenance",
    "IdsProvenanceNode",
    "Library",
    "NtmTimeSlice",
    "NtmTimeSliceMode",
    "NtmTimeSliceModeDetailedEvolution",
    "NtmTimeSliceModeDetailedEvolutionDeltaw",
    "NtmTimeSliceModeDetailedEvolutionTorque",
    "NtmTimeSliceModeEvolutionDeltaw",
    "NtmTimeSliceModeEvolutionTorque",
    "NtmTimeSliceModeOnset",
    "Ntms",
]
