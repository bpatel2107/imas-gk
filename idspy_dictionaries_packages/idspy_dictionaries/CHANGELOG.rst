IDSPy_Dictionaries Changelog
============================

* v 340.2.0
 - IMAS Data Dictionary v 3.40
 - ids_gyrokinetics removed and replaced by ids_gyrokinetics_local
 - added package and data dictionary versions in ids attributes
 - same package for python 3.9 and 3.10+


* v 339.1.3
 - IMAS Data Dictionary v 3.39
 - corrected a bug in the expected type linked to numpy arrays
 - updated package version

 * v 3381.1.2
  - missing provenance data member
  - datatype for the time data member

 * v 33801.1.1
  - typo in changelog
  - correction of readme and minimum python requirement.

 * v 3_38_1.0.1
  - new versioning system implemented. Based on v 0.4.0
  - IMAS Version 3.38.1

 * v 0.4.0
  
  - IMAS Data Dictionary v 3.38.1
  - Add IDS_Provenance member for IDS_Properties
  - add global variables __version__ and __version_data_dictionary__  
  - default values for numpy.arrays is now empty list []